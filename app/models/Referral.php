<?php
class Referral extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM referral WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM referral WHERE id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM referral WHERE deleted = 0 AND order_delivered = 0 ;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM referral WHERE user_id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}

	public function writeData($user_id, $email)
	{
		$sql =
			"INSERT INTO `referral`
			(
				`created_by`, 
				`user_id`, 
				`friend_email`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . $user_id . "', 
				'" . $email . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `referral` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}