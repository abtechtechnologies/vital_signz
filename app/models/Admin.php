<?php
class Admin extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function writeUser($id, $first_name, $last_name, $email, $phone, $login, $user_type_id = 2, $address = '', $city = '', $zip = '', $id_file = '', $rec_file = '', $membership_level = '', $balance = '', $referral_id = '', $referral_name = '', $profile_image = '')
	{
		do {
			$this->load->model('user');
			
			if (trim($email) == '') {
				$record = array();
			} else {
				$record = $this->user->getUserByEmail($email);
			}
			
			$record2 = $this->user->getUserByLogin($login);
			
			if ($id > 0) {
				if (count($record) > 0) {
					if ($record['id'] != $id) {
						$status = 'Can not use this email. Please choose another.';
						break;
					}
				}
				
				if ($login != '') {
					if (count($record2) > 0) {
						if ($record['id'] != $id) {
							$status = 'Can not use this login name. Please choose another.';
							break;
						}
					}
				}
				
				$sql =
				"UPDATE `user` SET
        				modified_by = " . $_SESSION['user_id'] . ",
        				first_name = '" . str_replace("'", "\'", $first_name) . "',
        				last_name = '" . str_replace("'", "\'", $last_name) . "',
        				email = '" . str_replace("'", "\'", $email) . "',
        				phone = '" . str_replace("'", "\'", $phone) . "',
        				user_type_id = '" . str_replace("'", "\'", $user_type_id) . "',
        				address = '" . str_replace("'", "\'", $address) . "',
        				city = '" . str_replace("'", "\'", $city) . "',
        				zip = '" . str_replace("'", "\'", $zip) . "',
        				id_file_name = '" . str_replace("'", "\'", $id_file) . "',
        				rec_file_name = '" . str_replace("'", "\'", $rec_file) . "',
        				membership = '" . str_replace("'", "\'", $membership_level) . "',
        				balance = '" . str_replace("'", "\'", $balance) . "',
        				referral_id = '" . str_replace("'", "\'", $referral_id) . "',
        				referral_name = '" . str_replace("'", "\'", $referral_name) . "',
						profile_image = '" . str_replace("'", "\'", $profile_image) . "',
        				login = '" . str_replace("'", "\'", $login) . "'
        			WHERE
        				id = " . str_replace("'", "\'", $id) . ";";
			} else {
				
				if (count($record) > 0) {
					$status = 'Can not use this email. Please choose another.';
					break;
				}
				
				if ($login != '') {
					if (count($record2) > 0) {
						$status = 'Can not use this login. Please choose another.';
						break;
					}
				}
				
				$sql =
				"INSERT INTO `user` (
    				`created_by`,
    				`first_name`,
    				`last_name`,
    				`email`,
    				`phone`,
    				`login`,
    				`address`,
    				`city`,
    				`zip`,
    				`id_file_name`,
    				`rec_file_name`,
    				`membership`,
    				`balance`,
    				`referral_id`,
    				`referral_name`,
					`profile_image`,
    				`user_type_id`
    			) VALUES (
    				'1',
    				'" . str_replace("'", "\'", $first_name) . "',
    				'" . str_replace("'", "\'", $last_name) . "',
    				'" . str_replace("'", "\'", $email) . "',
    				'" . str_replace("'", "\'", $phone) . "',
    				'" . str_replace("'", "\'", $login) . "',
    				'" . str_replace("'", "\'", $address) . "',
    				'" . str_replace("'", "\'", $city) . "',
    				'" . str_replace("'", "\'", $zip) . "',
    				'" . str_replace("'", "\'", $id_file) . "',
    				'" . str_replace("'", "\'", $rec_file) . "',
    				'" . str_replace("'", "\'", $membership_level) . "',
    				'" . str_replace("'", "\'", $balance) . "',
    				'" . str_replace("'", "\'", $referral_id) . "',
    				'" . str_replace("'", "\'", $referral_name) . "',
					'" . str_replace("'", "\'", $profile_image) . "',
    				'" . str_replace("'", "\'", $user_type_id) . "');";
			}
			
			return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		} while (false);
		
		return $status;
	}
	
	public function writeCompanyInfo($name, $email, $phone, $address, $phone_show, $logo, $icon, $link_image)
	{
		$sql =
		"UPDATE `company_info` SET
			modified_by = " . $_SESSION['user_id'] . ",
			name = '" . str_replace("'", "\'", $name) . "',
			email = '" . str_replace("'", "\'", $email) . "',
			phone = '" . str_replace("'", "\'", $phone) . "',
			phone_show = '" . str_replace("'", "\'", $phone_show) . "',
			logo_light = '" . str_replace("'", "\'", $logo) . "',
			icon = '" . str_replace("'", "\'", $icon) . "',
			link_image = '" . str_replace("'", "\'", $link_image) . "',
			address = '" . str_replace("'", "\'", $address) . "'
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function randomString($length = 12) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function writePassword($password, $id = 0)
	{
		if ($id == 0) {
			//Get users in reverse order to grab most recently modified user
			$this->load->model('user');
			$page['users'] = $this->user->getUsers(true);
			
			foreach ($page['users'] as $id => $data) {
				$user_id = $id;
				break;
			}
		} else {
			$user_id = $id;
		}
		
		
		// A higher "cost" is more secure but consumes more processing power
		$cost = 10;
		
		// Create a random salt
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		
		// Prefix information about the hash so PHP knows how to verify it later.
		// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
		
		// Hash the password with the salt
		$hash = crypt($password, $salt);
		
		$sql =
		"UPDATE `user` SET
			password = '" . str_replace("'", "\'", $hash) . "'
		WHERE
			id = " . str_replace("'", "\'", $user_id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
}