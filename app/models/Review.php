<?php
class Review extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM review WHERE id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `review`
			(
				`created_by`, 
				`user_id`, 
                `product_id`, 
                `rating`, 
                `description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}