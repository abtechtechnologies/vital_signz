<?php
class Zuser extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zuser WHERE Z_PK = " . $user_id . " ORDER BY Z_PK DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($user_id)
	{
		$sql = "SELECT * FROM zuser WHERE Z_PK = " . $user_id . " ORDER BY Z_PK DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{
		$sql = "SELECT * FROM zuser";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['Z_PK']] = $row;
		}
		
		return $return;
	}
	
	public function getCompany($company_id)
	{
		$sql = "SELECT * FROM zuser";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['Z_PK']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($id, $Z_PK, $ZFIRSTNAME, $ZLASTNAME, $ZEMAIL, $ZPASSWORD, $age = 0, $sex = '', $height = 0, $weight = 0)
	{
		$height = $height . '"';

		if ($age == '') {
			$age = 0;
		}
		
		if ($height == '') {
			$height = 0;
		}
		
		if ($weight == '') {
			$weight = 0;
		}
		
		if ($Z_PK > 0) {
			$sql =
			"UPDATE `zuser` SET
				ZFIRSTNAME = '" . str_replace("'", "\'", trim($ZFIRSTNAME)) . "',
				ZLASTNAME = '" . str_replace("'", "\'", trim($ZLASTNAME)) . "',
				ZEMAIL = '" . str_replace("'", "\'", trim($ZEMAIL)) . "',
				age = " . str_replace("'", "\'", trim($age)) . ",
				sex = '" . str_replace("'", "\'", trim($sex)) . "',
				weight = " . str_replace("'", "\'", trim($weight)) . ",
				height = '" . str_replace("'", "\'", trim($height)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $Z_PK) . ";";
		} else {
			$sql =
			"INSERT INTO `zuser`
			(
				`Z_PK`,
				`ZFIRSTNAME`,
				`ZLASTNAME`,
				`ZEMAIL`,
				`ZPASSWORD`
			) VALUES (
				'" . str_replace("'", "\'", trim($Z_PK)) . "',
				'" . str_replace("'", "\'", trim($ZFIRSTNAME)) . "',
				'" . str_replace("'", "\'", trim($ZLASTNAME)) . "',
				'" . str_replace("'", "\'", trim($ZEMAIL)) . "',
				'" . str_replace("'", "\'", trim($ZPASSWORD)) . "'
			);";
		}

		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function writeSex($id, $sex)
	{
		$sql = 'UPDATE `zuser` SET sex = \'' . $sex . '\' WHERE Z_PK = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function writeAge($id, $age)
	{
		$sql = 'UPDATE `zuser` SET age = ' . $age . ' WHERE Z_PK = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}

}