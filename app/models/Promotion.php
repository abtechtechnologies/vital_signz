<?php
class Promotion extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM promotion WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecentID()
	{
		$sql = 'SELECT * FROM promotion WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM promotion WHERE id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM promotion WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function generateCode($promotion_type_id, $user_id)
	{
		$code = '';
		
		$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
		
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
		
		$sql =
		"INSERT INTO `promotion`
		(
			`created_by`,
			`promotion_type_id`,
			`user_id`,
			`code`
		) VALUES (
			'" . $_SESSION['user_id'] . "',
			" . $promotion_type_id . ",
			" . $user_id . ",
			'" . $rand . "'
		);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function getRecordsByPromotionTypeAdmin($promotion_type_id)
	{
		$sql = 'SELECT * FROM promotion WHERE promotion_type_id = ' . $promotion_type_id . ' ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM promotion WHERE 1 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `promotion`
			(
				`created_by`, 
				`user_id`, 
				`product_id`, 
				`rating`, 
				`description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `promotion` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}