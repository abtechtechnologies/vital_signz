<?php
class Zheartrate extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->db = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zheartrate WHERE Z_PK = ' . $id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords()
	{
		$sql = "SELECT * FROM zheartrate";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zheartrate WHERE zuserid = " . $user_id . " ORDER BY date DESC";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zheartrate;";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['Z_PK']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zheartrate WHERE zuserid IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {

			$return['records'][] = $row;
		}
		
		return $return;
	}

	
	public function getUserRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zheartrate WHERE ZUSERID = " . $user_id . " ORDER BY Z_PK DESC;";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = str_replace('-', '/', $row['date']);

			if ($date1 == $date2) {
				
				$row_parts = explode(' ', $row_date);
				$row_date = $row_parts[0];

				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);

				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['Z_PK']] = $row;
				}
			}
		}
		
		return $return;		
	}
	
	public function getRecordsByUser()
	{
		$sql = "SELECT * FROM zheartrate ORDER BY zdate DESC";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zheartrate WHERE 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentzheartrateId()
	{
		$sql = 'SELECT * FROM zheartrate WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getzheartratesByzheartrateType($zheartrate_type_id)
	{
		$sql = 'SELECT * FROM zheartrate WHERE deleted = 0 AND parent_id = 0 AND zheartrate_type_id = ' . $zheartrate_type_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getNextId()
	{
		$sql = "SELECT * FROM zheartrate ORDER BY Z_PK DESC LIMIT 1";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['Z_PK'];
		}
		
		$return++;
		
		return $return;
	}
	
	public function getzheartratesByParentId($zheartrate_id)
	{
		$sql = 'SELECT * FROM zheartrate WHERE deleted = 0 AND parent_id = ' . $zheartrate_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzheartratesByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['zheartrate_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM zheartrate WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzheartrateSizes()
	{
		$sql = 'SELECT * FROM zheartrate WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $Z_ENT, $Z_OPT, $ZDATE, $ZRATE, $ZCOMMENT, $ZUSERID)
	{

		if ($id > 0) {
			$sql =
			"UPDATE `zheartrate` SET
				Z_ENT = '" . str_replace("'", "\'", trim($Z_ENT)) . "',
				Z_OPT = '" . str_replace("'", "\'", trim($Z_OPT)) . "',
				ZDATE = '" . str_replace("'", "\'", trim($ZDATE)) . "',
				ZRATE = '" . str_replace("'", "\'", trim($ZRATE)) . "',
				ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				ZUSERID = '" . str_replace("'", "\'", trim($ZUSERID)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zheartrate`
			(
				`Z_ENT`,
				`Z_OPT`,
				`ZDATE`,
				`date`,
				`ZRATE`,
				`ZCOMMENT`,
				`ZUSERID`
			) VALUES (
				'" . str_replace("'", "\'", trim($Z_ENT)) . "',
				'" . str_replace("'", "\'", trim($Z_OPT)) . "',
				'" . str_replace("'", "\'", trim($ZDATE)) . "',
				'" . str_replace("'", "\'", trim($ZDATE)) . "',
				'" . str_replace("'", "\'", trim($ZRATE)) . "',
				'" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zheartrate` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function writeDate($id)
	{
		$this->load->model('zheartrate');
		$record = $this->zheartrate->getRecord($id);
		
		$date = date('Y-m-d H:i:s', $record['ZDATE'] + 978307200);
		
		$sql = 'UPDATE `zheartrate` SET created_date = "' . $date . '" WHERE Z_PK = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}