<?php
class Page extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getPages()
	{
		$sql = "
			SELECT * FROM
				page
			WHERE
				deleted = 0 ";
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			//Find position of next '/'
			$dash_position = strpos($row['page_link'], '/');
				
			$subdomain = substr($row['page_link'], $dash_position);
				
			if ($subdomain == '/index') {
				$row['page_link'] = substr($row['page_link'], 0, $dash_position);
			}
				
			$row['page_link'] = '/' . $row['page_link'];
				
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getPageContent($page_name)
	{
		$sql = "
			SELECT * FROM
				page
			WHERE
				deleted = 0 AND
				name = '" . $page_name . "'";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			//Find position of next '/'
			$dash_position = strpos($row['page_link'], '/');
			
			$subdomain = substr($row['page_link'], $dash_position);
			
			if ($subdomain == '/index') {
				$row['page_link'] = substr($row['page_link'], 0, $dash_position);
			}
			
			$row['page_link'] = '/' . $row['page_link'];
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getPageNames($start_empty = false)
	{
		$sql = "
			SELECT 
				DISTINCT(name)
			FROM
				page
			WHERE
				deleted = 0
			ORDER BY
				name";
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
		
		if ($start_empty == true) {
			$return[0] = '';
		}
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['name'];
		}
	
		return $return;
	}
	
	public function getRecord($page_id)
	{
		$sql = "
			SELECT * FROM
				page
			WHERE
				deleted = 0 AND
				id = '" . $page_id . "'";
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = '';
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function writeData($data)
	{
		$sql = 
			"UPDATE `page` SET 
				modified_by = " . $_SESSION['user_id'] . ", 
				section_header = '" . str_replace("'", "\'", $data['section_header']) . "', 
				section_text = '" . str_replace("'", "\'", $data['section_text']) . "',
				page_link = '" . str_replace("'", "\'", $data['page_link']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment_file_name']) . "'
			WHERE 
				id = " . str_replace("'", "\'", $data['page_id']) . ";";
		
		//$status = $this->DB->insert('post', $data);

// 		$user = $this->DB->get('post');
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
}