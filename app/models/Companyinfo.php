<?php
class Companyinfo extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
	}
	
	public function getRecord($client_id = -1)
	{
		$sql = 'SELECT * FROM company_info WHERE id = 1 AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		if (isset($data['content_id'])) {
			$sql =
			"UPDATE `content` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", $data['title']) . "',
				content_text = '" . str_replace("'", "\'", $data['content_text']) . "',
				target_date = '" . str_replace("'", "\'", $data['target_date']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['content_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `content`
			(
				`created_by`,
				`title`,
				`content_text`,
				`attachment_file_name`,
				`target_date`,
				`content_type_id`
			) VALUES ('
				" . $_SESSION['user_id'] . "', '
				" . str_replace("'", "\'", trim($data['title'])) . "', '
				" . str_replace("'", "\'", $data['content_text']) . "', '
				" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', '
				" . str_replace("'", "\'", $data['target_date']) . "', '
				" . str_replace("'", "\'", $data['content_type_id']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function changePrimaryHolder($user_id)
	{
		$sql =
		"UPDATE `company_info` SET
			primary_inventory_user_id = " . $user_id . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function switchAllowNegative($source)
	{
		$sql =
		"UPDATE `company_info` SET
			allow_negative = " . $source . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
}