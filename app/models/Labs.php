<?php
class Labs extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid = " . $user_id . " ORDER BY ZDATE DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZNAME']])) {
				$return[$row['ZNAME']] = array();
			}
			
			$return[$row['ZNAME']]['records'][] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecordsByName($user_string, $med_name)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid IN (" . $user_string . ") AND ZNAME = '" . $med_name . "' ORDER BY date;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getUserRecordsByName($user_id, $name, $index = false)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid = " . $user_id . " AND zname = '" . $name . "' ORDER BY ZDATE DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($index == true) {
				$return[] = $row;
			} else {
				$return[$row['Z_PK']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getUserRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid = " . $user_id . " ORDER BY Z_PK DESC;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = date('m-d-Y', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				
				$string_parts = explode('-', $date1);
				$string = $string_parts[1] . '-' . $string_parts[2] . '-' . $string_parts[0];
				
				//1 Date
				if ($string == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['Z_PK']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getUserRecordsByDateRangeAndName($user_id, $date1, $date2, $lab_name)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid = " . $user_id . " AND ZNAME = '" . $lab_name . "' ORDER BY Z_PK DESC;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = date('m-d-Y', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				
				$string_parts = explode('-', $date1);
				$string = $string_parts[1] . '-' . $string_parts[2] . '-' . $string_parts[0];
				
				//1 Date
				if ($string == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['Z_PK']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecords()
	{
		$sql = 'SELECT * FROM zlabdata;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[$row['Z_PK']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `review`
			(
				`created_by`, 
				`user_id`, 
                `product_id`, 
                `rating`, 
                `description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function writeDate($id)
	{
		$this->load->model('zlabdata');
		$record = $this->zlabdata->getRecord($id);
		
		$date = date('Y-m-d H:i:s', $record['ZDATE'] + 978307200);
		
		$sql = 'UPDATE `zlabdata` SET date = "' . $date . '" WHERE Z_PK = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}

}