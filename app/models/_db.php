<?php

class _db extends CI_Model {
	
	public function sql($conn, $table, $user_id)
	{
		$sql = "SELECT * FROM " . $table . " WHERE zuserid = " . $user_id;
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			if ($table == 'znutrientgoals') {
				$return[$row['ZCOMMON']] = $row;
			} else {
				$return[] = $row;
			}
			
		}
		
		return $return;
	}
}
