<?php
class Zsteps extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zsteps WHERE user_id IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[] = $row;
		}
		
		return $return;
	}

	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zsteps WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($conn, $limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zsteps LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zsteps";
		}
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getNextId()
	{
		$sql = "SELECT * FROM zsteps ORDER BY Z_PK DESC LIMIT 1";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row['Z_PK'] + 1;
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($conn, $user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zsteps;";
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zsteps WHERE user_id = " . $user_id;

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($conn)
	{
		$sql = "SELECT * FROM zsteps";
		
		$result = $conn->query($sql);
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zsteps WHERE 1;';
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecordsByDateRange($company_string, $date1, $date2)
	{
		$sql = "SELECT * FROM zsteps WHERE user_id IN (" . $company_string . ") ORDER BY date DESC;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($date1 == $date2) {
				
				$date_pieces = explode(' ', $row['date']);
				$row_date = $date_pieces[0];
				
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime(str_replace('-', '/', $row['date']));
				
				//FOR DAYLIGHT SAVINGS
				//$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['Z_PK']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function writeData($id, $Z_PK, $date, $user_id, $steps)
	{

		if ($id > 0) {
			$sql =
			"UPDATE `zsteps` SET
				date = '" . str_replace("'", "\'", trim($date)) . "',
				user_id = '" . str_replace("'", "\'", trim($user_id)) . "',
				steps = '" . str_replace("'", "\'", trim($steps)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $Z_PK) . ";";
		} else {
			$sql =
			"INSERT INTO `zsteps`(`Z_PK`,`date`,`user_id`,`steps`) VALUES ('" . str_replace("'", "\'", trim($Z_PK)) . "','" . $date. "','" . str_replace("'", "\'", trim($user_id)) . "','" . str_replace("'", "\'", trim($steps)) . "');";
		}
		
		$return = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zsteps` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}