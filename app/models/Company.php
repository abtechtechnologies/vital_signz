<?php
class Company extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM company WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}

	public function getRecords()
	{
		$sql = 'SELECT * FROM company WHERE deleted = 0 ORDER BY id DESC;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($data)
	{
		if ($data['company_id'] > 0) {
			$sql =
			"UPDATE `company` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", $data['title']) . "',
				content_text = '" . str_replace("'", "\'", $data['content_text']) . "',
				target_date_start = '" . str_replace("'", "\'", $data['target_date_start']) . "',
				thumb_file_name = '" . str_replace("'", "\'", $data['attachment_file_name']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment_file_name']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['company_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `company`
			(
				`created_by`, 
				`title`, 
				`content_text`, 
				`attachment_file_name`, 
				`thumb_file_name`, 
				`target_date_start`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($data['title'])) . "', 
				'" . str_replace("'", "\'", $data['content_text']) . "', 
				'" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', 
				'" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', 
				'" . str_replace("'", "\'", $data['target_date_start']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `company` SET deleted = 1 WHERE id = ' . $id;

		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function writeMotd($id, $motd)
	{
		$sql = 'UPDATE `company` SET motd = \'' . $motd . '\' WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}