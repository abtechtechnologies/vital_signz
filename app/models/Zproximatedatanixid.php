<?php
class Zproximatedatanixid extends CI_Model {

	public function __construct()
	{
		
	}
	
	public function getUserRecords($conn, $user_id)
	{
		$sql = "SELECT * FROM zproximatedatanixid WHERE zuserid = " . $user_id . " ORDER BY zdate DESC";
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$return[] = $row;			
		}
				
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `review`
			(
				`created_by`, 
				`user_id`, 
                `product_id`, 
                `rating`, 
                `description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}