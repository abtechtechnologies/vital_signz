<?php
class Zalert extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zalert WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($user_id)
	{
		$sql = 'SELECT * FROM zalert WHERE deleted = 0 ORDER BY id DESC;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecords($company_id)
	{
		$sql = 'SELECT * FROM zalert WHERE company_id = ' . $company_id . ' AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecords($include_children = false)
	{			
		if ($include_children == true) {
			$sql = 'SELECT * FROM zalert WHERE deleted = 0 ORDER BY id;';
		} else {
			$sql = 'SELECT * FROM zalert WHERE deleted = 0 AND parent_id = 0 ORDER BY id;';
		}

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($zalert_id, $priority, $alert, $company_id)
	{
		if ($zalert_id > 0) {
			$sql =
			"UPDATE `zalert` SET
				modified_by = " . $_SESSION['user_id'] . ",
				priority = '" . str_replace("'", "\'", $priority) . "',
				company_id = '" . str_replace("'", "\'", $company_id) . "',
				alert = '" . str_replace("'", "\'", $alert) . "'
			WHERE
				id = " . str_replace("'", "\'", $zalert_id) . ";";
		} else {
			$sql =
			"INSERT INTO `zalert`
			(
				`created_by`,  
				`priority`,
				`company_id`,
				`alert`
			) VALUES (
			'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", $priority) . "', 
				'" . str_replace("'", "\'", $company_id) . "', 
				'" . str_replace("'", "\'", $alert) . "'
			);";
		}
		
		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zalert` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}