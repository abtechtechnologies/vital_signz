<?php
class Zfooddata extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zfooddata WHERE user_id = " . $user_id . " ORDER BY id DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecordsByDateRange($company_string, $date1, $date2)
	{
		$sql = "SELECT * FROM zfooddata WHERE user_id IN (" . $company_string . ") ORDER BY id DESC;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($date1 == $date2) {
				
				$date_pieces = explode(' ', $row['date']);
				$row_date = $date_pieces[0];
				
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime(str_replace('-', '/', $row['date']));
				
				//FOR DAYLIGHT SAVINGS
				//$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zfooddata;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$time = strtotime($row['created_date']);
			
			$row_date = date('Y-m-d', $time);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getRecords($limit = '')
	{
		if ($limit != '') {
			$sql = "SELECT * FROM zfooddata LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zfooddata";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = "SELECT * FROM zfooddata WHERE id = " . id . ";";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zfooddata WHERE user_id IN (" . $user_string . ") ORDER BY date";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}

	public function writeData($id, $user_id, $quantity, $food_name, $serving_qty, $serving_unit, $serving_weight_grams, $calories, $cholesterol, $dietary_fiber, $potassium, $protein, $saturated_fat, $sodium, $sugars, $total_carbohydrate, $total_fat, $meal_type, $date)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zfooddata` SET
			food_name = '" . str_replace("'", "\'", trim($food_name)) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zfooddata`(`created_by`,`created_date`,`date`,`deleted`,`modified_by`,`modified_date`, `user_id`, `quantity`, `food_name`, `serving_qty`, `serving_unit`, `serving_weight_grams`, `calories`, `cholesterol`, `dietary_fiber`, `potassium`, `protein`, `saturated_fat`, `sodium`, `sugars`, `total_carbohydrate`, `total_fat`, `meal_type`) VALUES (1,NOW(),'" . $date . "',0,1,'2018-01-01 11:11:11'," . $user_id . "," . $quantity . ",'" . $food_name . "'," . $serving_qty . ",'" . $serving_unit . "'," . $serving_weight_grams . "," . $calories . "," . $cholesterol . "," . $dietary_fiber . "," . $potassium . "," . $protein . "," . $saturated_fat . "," . $sodium . "," . $sugars . "," . $total_carbohydrate . "," . $total_fat . ",'" . $meal_type . "');";
		}
		
		$return = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function writeDate($id, $date)
	{

		$sql = 'UPDATE `zfooddata` SET date = "' . $date . '" WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}

}