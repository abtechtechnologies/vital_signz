<?php
class Maillink extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($key)
	{
		$sql = "SELECT * FROM mail_link WHERE link_key = '" . $key . "' AND deleted = 0;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($user_id, $key)
	{
		$sql =
			"INSERT INTO `mail_link`
			(
				`created_by`, 
				`user_id`,
				`link_key`
			) VALUES (
				'1', 
				'" . str_replace("'", "\'", trim($user_id)) . "', 
				'" . str_replace("'", "\'", trim($key)) . "'
			);";

		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `mail_link` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function unlock($user_id, $key)
	{
		$sql = "
			SELECT * FROM 
				mail_link 
			WHERE 
				link_key = '" . $key . "' AND 
				user_id = " . $user_id . " AND 
				deleted = 0;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		if (count($return) > 0) {
			$status = 1;
		} else {
			$status = 0;
		}
		
		return $status;
	}
}