<?php
class Zbloodpressure extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE id = ' . $id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zbloodpressure WHERE zuserid = " . $user_id . " ORDER BY date DESC";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {			
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zbloodpressure WHERE zuserid IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecords($limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zbloodpressure ORDER BY zdate DESC LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zbloodpressure ORDER BY zdate DESC";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($conn)
	{
		$sql = "SELECT * FROM zbloodpressure ORDER BY zdate DESC";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE 1;';
		
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentzbloodpressureId()
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getzbloodpressuresByzbloodpressureType($zbloodpressure_type_id)
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE deleted = 0 AND parent_id = 0 AND zbloodpressure_type_id = ' . $zbloodpressure_type_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzbloodpressuresByParentId($zbloodpressure_id)
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE deleted = 0 AND parent_id = ' . $zbloodpressure_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzbloodpressuresByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['zbloodpressure_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzbloodpressureSizes()
	{
		$sql = 'SELECT * FROM zbloodpressure WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $date, $ZDIASTOLIC, $ZSYSTOLIC, $ZUSERID)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zbloodpressure` SET
				modified_date = NOW(),
				ZDIASTOLIC = '" . str_replace("'", "\'", trim($ZDIASTOLIC)) . "',
				ZSYSTOLIC = '" . str_replace("'", "\'", trim($ZSYSTOLIC)) . "',
				ZUSERID = '" . str_replace("'", "\'", trim($ZUSERID)) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zbloodpressure`
			(
				`created_date`,
				`date`,
				`ZDIASTOLIC`,
				`ZSYSTOLIC`,
				`ZUSERID`
			) VALUES (
				NOW(),
				'" . str_replace("'", "\'", trim($date)) . "',
				'" . str_replace("'", "\'", trim($ZDIASTOLIC)) . "',
				'" . str_replace("'", "\'", trim($ZSYSTOLIC)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}

		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function getRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zbloodpressure WHERE ZUSERID = " . $user_id . " ORDER BY id DESC;";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {

			if ($date1 == $date2) {
				
				$date_pieces = explode(' ', $row['date']);
				$row_date = $date_pieces[0];

				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime(str_replace('-', '/', $row['date']));
				
				//FOR DAYLIGHT SAVINGS
				//$row_time = $row_time - 3600;

				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getCompanyRecordsByDateRange($company_string, $date1, $date2)
	{
		$sql = "SELECT * FROM zbloodpressure WHERE ZUSERID IN (" . $company_string . ") ORDER BY id DESC;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($date1 == $date2) {
				
				$date_pieces = explode(' ', $row['date']);
				$row_date = $date_pieces[0];
				
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime(str_replace('-', '/', $row['date']));
				
				//FOR DAYLIGHT SAVINGS
				//$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zbloodpressure` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function writeDate($id)
	{
		$this->load->model('zbloodpressure');
		$record = $this->zbloodpressure->getRecord($id);
		
		$date = date('Y-m-d H:i:s', $record['ZDATE'] + 978307200);
		
		$sql = 'UPDATE `zbloodpressure` SET date = "' . $date . '" WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}