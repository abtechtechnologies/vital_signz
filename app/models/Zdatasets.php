<?php
class Zdatasets extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->db = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = "SELECT * FROM zdatasets WHERE Z_PK = " . $id;
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getNameMatch($name)
	{
		$name = strtoupper($name);
		
		$sql = 'SELECT * FROM zdatasets WHERE ZDESC = "' . $name . '";';

		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords($limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zdatasets ORDER BY Z_PK DESC LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zdatasets";
		}
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByName()
	{

		$sql = "SELECT * FROM zdatasets";
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['ZDESC']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zdatasets WHERE 1;';
		
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentzdatasetsId()
	{
		$sql = 'SELECT * FROM zdatasets WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getzdatasetssByzdatasetsType($zdatasets_type_id)
	{
		$sql = 'SELECT * FROM zdatasets WHERE deleted = 0 AND parent_id = 0 AND zdatasets_type_id = ' . $zdatasets_type_id . ';';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzdatasetssByParentId($zdatasets_id)
	{
		$sql = 'SELECT * FROM zdatasets WHERE deleted = 0 AND parent_id = ' . $zdatasets_id . ';';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzdatasetssByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['zdatasets_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM zdatasets WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzdatasetsSizes()
	{
		$sql = 'SELECT * FROM zdatasets WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace('/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $Z_PK, $Z_ENT, $Z_OPT, $ZRANGEHIGH, $ZRANGELOW, $ZCOMMENT, $ZDESC, $ZNAME, $ZTAGS, $ZUNITS, $ZUSERID)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zdatasets` SET
				ZRANGEHIGH = '" . str_replace("'", "\'", trim($ZRANGEHIGH)) . "',
				ZRANGELOW = '" . str_replace("'", "\'", trim($ZRANGELOW)) . "',
				ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				ZDESC = '" . str_replace("'", "\'", trim($ZDESC)) . "',
				ZNAME = '" . str_replace("'", "\'", trim($ZNAME)) . "',
				ZTAGS = '" . str_replace("'", "\'", trim($ZTAGS)) . "',
				ZUNITS = '" . str_replace("'", "\'", $ZUNITS) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zdatasets`
			(
				`Z_PK`,
				`Z_ENT`,
				`Z_OPT`,
				`ZRANGEHIGH`,
				`ZRANGELOW`,
				`ZCOMMENT`,
				`ZDESC`,
				`ZNAME`,
				`ZTAGS`,
				`ZUNITS`,
				`ZUSERID`
			) VALUES (
				'" . str_replace("'", "\'", trim($Z_PK)) . "',
				'" . str_replace("'", "\'", trim($Z_ENT)) . "',
				'" . str_replace("'", "\'", trim($Z_OPT)) . "',
				'" . str_replace("'", "\'", trim($ZRANGEHIGH)) . "',
				'" . str_replace("'", "\'", trim($ZRANGELOW)) . "',
				'" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				'" . str_replace("'", "\'", trim($ZDESC)) . "',
				'" . str_replace("'", "\'", trim($ZNAME)) . "',
				'" . str_replace("'", "\'", trim($ZTAGS)) . "',
				'" . str_replace("'", "\'", trim($ZUNITS)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}
		
		$result = $conn->query($sql);
		
		if ($result == '') {
			print '<pre>';
			print_r($conn);
			print '</pre>';
			exit;
		}
	
		return $result;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zdatasets` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace('/\s+/', ' ', $sql));

		return $status;
	}
}