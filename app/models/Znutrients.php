<?php
class Znutrients extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->db = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM znutrients WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords()
	{
		$sql = "SELECT * FROM znutrients";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getListRecords()
	{
		$sql = "SELECT * FROM znutrients WHERE list = 1 ORDER BY name ASC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM znutrients;";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM znutrients WHERE zuserid = " . $user_id;

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser()
	{
		$sql = "SELECT * FROM znutrients";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM znutrients WHERE 1;';
		
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentznutrientsId()
	{
		$sql = 'SELECT * FROM znutrients WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getznutrientssByznutrientsType($znutrients_type_id)
	{
		$sql = 'SELECT * FROM znutrients WHERE deleted = 0 AND parent_id = 0 AND znutrients_type_id = ' . $znutrients_type_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientssByParentId($znutrients_id)
	{
		$sql = 'SELECT * FROM znutrients WHERE deleted = 0 AND parent_id = ' . $znutrients_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientssByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['znutrients_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM znutrients WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientsSizes()
	{
		$sql = 'SELECT * FROM znutrients WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $usda_tag, $name, $unit)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `znutrients` SET
				ZDATE = '" . str_replace("'", "\'", trim($ZDATE)) . "',
				ZVALUE = '" . str_replace("'", "\'", trim($ZVALUE)) . "',
				ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				ZNAME = '" . str_replace("'", "\'", trim($ZNAME)) . "',
				ZUSERID = '" . str_replace("'", "\'", trim($ZUSERID)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $Z_PK) . ";";
		} else {
			$sql =
			"INSERT INTO `znutrients`
			(
				`id`,
				`usda_tag`,
				`name`,
				`unit`
			) VALUES (
				'" . str_replace("'", "\'", trim($id)) . "',
				'" . str_replace("'", "\'", trim($usda_tag)) . "',
				'" . str_replace("'", "\'", trim($name)) . "',
				'" . str_replace("'", "\'", trim($unit)) . "'
			);";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `znutrients` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}