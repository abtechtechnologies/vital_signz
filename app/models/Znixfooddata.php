<?php
class Znixfooddata extends CI_Model {

	public function __construct()
	{
		
	}
	
	public function getUserRecords($conn, $user_id)
	{
		$sql = "SELECT * FROM znixfooddata WHERE zuserid = " . $user_id . " ORDER BY zdate DESC";
		
		$result = $conn->query($sql);

		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$return[] = $row;			
		}
				
		return $return;
	}
	
	public function getRecordsByDateRange($conn, $user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM znixfooddata;";
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getRecords($limit = '')
	{
		if ($limit != '') {
			$sql = "SELECT * FROM znixfooddata LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM znixfooddata";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `review`
			(
				`created_by`, 
				`user_id`, 
                `product_id`, 
                `rating`, 
                `description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `review` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}