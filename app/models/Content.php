<?php
class Content extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM content WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getContent($id)
	{				
		$sql = 'SELECT * FROM content WHERE deleted = 0 AND id = ' . $id . ' ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
				
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getAllContent()
	{
		$sql = 'SELECT * FROM content WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getContentByContentType($content_type_id = 0)
	{
		//Sort by target date if content type is an EVENT
		if ($content_type_id == 3) {
			$db = $this->DB->query('SELECT * FROM content WHERE content_type_id IN (3,6) AND deleted = 0 ORDER BY target_date');
		} else {
			$db = $this->DB->query('SELECT * FROM content WHERE content_type_id = ' . $content_type_id . ' AND deleted = 0 ORDER BY id DESC');
		}
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($data)
	{
		if (isset($data['content_id'])) {
			$sql =
			"UPDATE `content` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", $data['title']) . "',
				content_text = '" . str_replace("'", "\'", $data['content_text']) . "',
				target_date_start = '" . str_replace("'", "\'", $data['target_date']) . "',
				thumb_file_name = '" . str_replace("'", "\'", $data['attachment_file_name']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment_file_name']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['content_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `content`
			(
				`created_by`, 
				`title`, 
				`content_text`, 
				`attachment_file_name`, 
				`thumb_file_name`, 
				`target_date_start`, 
				`content_type_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($data['title'])) . "', 
				'" . str_replace("'", "\'", $data['content_text']) . "', 
				'" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', 
				'" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', 
				'" . str_replace("'", "\'", $data['target_date']) . "', 
				'" . str_replace("'", "\'", $data['content_type_id']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `content` SET deleted = 1 WHERE id = ' . $id;

		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}