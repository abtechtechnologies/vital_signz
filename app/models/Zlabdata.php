<?php
class Zlabdata extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid = " . $user_id . " ORDER BY Z_PK DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecords($limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zlabdata ORDER BY Z_PK DESC LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zlabdata";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = "SELECT * FROM zlabdata WHERE Z_PK = " . $id;
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM review WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM review WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM review WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecordsByName($user_string, $med_name)
	{
		$sql = "SELECT * FROM zlabdata WHERE zuserid IN (" . $user_string . ") AND ZNAME = '" . $med_name . "' ORDER BY date;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[] = $row;
		}
		
		return $return;
	}

	public function writeData($id, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zlabdata` SET
				modified_date = NOW(),
				date = '" . str_replace("'", "\'", trim($ZDATE)) . "',
				ZVALUE = '" . str_replace("'", "\'", trim($ZVALUE)) . "',
				ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zlabdata`
			(
				`created_date`,
				`date`,
				`ZVALUE`,
				`ZCOMMENT`,
				`ZNAME`,
				`ZUSERID`
			) VALUES (
				NOW(),
				'" . str_replace("'", "\'", trim($ZDATE)) . "',
				'" . str_replace("'", "\'", trim($ZVALUE)) . "',
				'" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				'" . str_replace("'", "\'", trim($ZNAME)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}

		$return = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'DELETE FROM `zlabdata` WHERE Z_PK = ' . $id;
	
		$return = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		return $return;
	}
	
	public function writeDate($id)
	{
		$this->load->model('zlabdata');
		$record = $this->zlabdata->getRecord($id);
		
		$date = date('Y-m-d H:i:s', $record['ZDATE'] + 978307200);
		
		$sql = 'UPDATE `zlabdata` SET date = "' . $date . '" WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}

}