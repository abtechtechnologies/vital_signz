<?php
class Zmedicationdata extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->db = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE Z_PK = ' . $id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecords($user_string)
	{
		$sql = "SELECT * FROM zmedicationdata WHERE zuserid IN (" . $user_string . ")";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZGENERIC']])) {
				$return[$row['ZGENERIC']] = array();
			}
			
			$return[$row['ZGENERIC']]['records'][] = $row;
		}
		
		return $return;
	}
	
	public function getCompanyRecordsByName($user_string, $med_name)
	{
		$sql = "SELECT * FROM zmedicationdata WHERE zuserid IN (" . $user_string . ") AND ZGENERIC = '" . $med_name . "' ORDER BY date;";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {

			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM zmedicationdata WHERE ZUSERID = " . $user_id . " ORDER BY ZDATE DESC";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['Z_PK']] = $row;
		}
		
		return $return;
	}
	
	public function getUserRecordsByName($user_id, $name)
	{
		$sql = "SELECT * FROM zmedicationdata WHERE zuserid = " . $user_id . " AND zgeneric = '" . $name . "' ORDER BY ZDATE DESC";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByMed()
	{
		$sql = "SELECT * FROM zmedicationdata";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZGENERIC']])) {
				$return[$row['ZGENERIC']] = array();
			}
			
			$return[$row['ZGENERIC']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecords($limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zmedicationdata LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zmedicationdata";
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['Z_PK']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentzmedicationdataId()
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getzmedicationdatasByzmedicationdataType($zmedicationdata_type_id)
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE deleted = 0 AND parent_id = 0 AND zmedicationdata_type_id = ' . $zmedicationdata_type_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzmedicationdatasByParentId($zmedicationdata_id)
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE deleted = 0 AND parent_id = ' . $zmedicationdata_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzmedicationdatasByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['zmedicationdata_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getzmedicationdataSizes()
	{
		$sql = 'SELECT * FROM zmedicationdata WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getNextId()
	{
		$sql = "SELECT * FROM zmedicationdata ORDER BY Z_PK DESC LIMIT 1";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['Z_PK'];
		}
		
		$return++;
		
		return $return;
	}
	
	public function writeData($id, $Z_PK, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZVAL1, $ZBRAND, $ZCOMMENT, $ZDOSAGEINFO, $ZFDAID, $ZGENERIC, $ZMANUFACTURER, $ZSTR1, $ZUNITS, $ZUSERID)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zmedicationdata` SET
				ZDATE = '" . str_replace("'", "\'", trim($ZDATE)) . "',
				ZVALUE = '" . str_replace("'", "\'", trim($ZVALUE)) . "',
				ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				ZNAME = '" . str_replace("'", "\'", trim($ZNAME)) . "',
				ZUSERID = '" . str_replace("'", "\'", trim($ZUSERID)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zmedicationdata`
			(
				`Z_PK`,				
				`ZDATE`,
				`ZAMOUNT`,
				`ZNDOSES`,
				`ZVAL1`,
				`ZBRAND`,
				`ZCOMMENT`,
				`ZDOSAGEINFO`,
				`ZFDAID`,
				`ZGENERIC`,
				`ZMANUFACTURER`,
				`ZSTR1`,
				`ZUNITS`,
				`ZUSERID`
			) VALUES (
				'" . str_replace("'", "\'", trim($Z_PK)) . "',				
				'" . str_replace("'", "\'", trim($ZDATE)) . "',
				'" . str_replace("'", "\'", trim($ZAMOUNT)) . "',
				'" . str_replace("'", "\'", trim($ZNDOSES)) . "',
				'" . str_replace("'", "\'", trim($ZVAL1)) . "',
				'" . str_replace("'", "\'", trim($ZBRAND)) . "',
				'" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
				'" . str_replace("'", "\'", trim($ZDOSAGEINFO)) . "',
				'" . str_replace("'", "\'", trim($ZFDAID)) . "',
				'" . str_replace("'", "\'", trim($ZGENERIC)) . "',
				'" . str_replace("'", "\'", trim($ZMANUFACTURER)) . "',
				'" . str_replace("'", "\'", trim($ZSTR1)) . "',
				'" . str_replace("'", "\'", trim($ZUNITS)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function updateData($id, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZCOMMENT)
	{
		$sql =
		"UPDATE `zmedicationdata` SET
			ZDATE = '" . str_replace("'", "\'", trim($ZDATE)) . "',
			ZAMOUNT = '" . str_replace("'", "\'", trim($ZAMOUNT)) . "',
			ZCOMMENT = '" . str_replace("'", "\'", trim($ZCOMMENT)) . "',
			ZNDOSES = '" . str_replace("'", "\'", trim($ZNDOSES)) . "'
		WHERE
			Z_PK = " . str_replace("'", "\'", $id) . ";";
		
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zmedicationdata` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function writeDate($id)
	{
		$this->load->model('zmedicationdata');
		$record = $this->zmedicationdata->getRecord($id);
		
		$date = date('Y-m-d H:i:s', $record['ZDATE'] + 978307200);
		
		$sql = 'UPDATE `zmedicationdata` SET date = "' . $date . '" WHERE Z_PK = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}