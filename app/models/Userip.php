<?php
class Userip extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM user_ip WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords($user_id)
	{				
		$sql = 'SELECT * FROM user_ip WHERE deleted = 0 AND user_id = ' . $user_id . ' ORDER BY id;';

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['interest_id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin($user_id)
	{
		$sql = 'SELECT * FROM user_ip WHERE user_id = ' . $user_id . ' ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['interest_id']] = $row;
		}
	
		return $return;
	}
	
	public function getUserByIP($ip)
	{
	    $sql = 'SELECT * FROM user_ip WHERE ip = \'' . $ip . '\' AND deleted = 0';
	
	    $db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return = $row;
	    }
	
	    return $return;
	}
	
	public function getUserByActiveIP($ip)
	{
		$sql = 'SELECT * FROM user_ip WHERE ip = \'' . $ip . '\' AND deleted = 0 AND active = 1';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getAdminIP($ip)
	{
		$sql = 'SELECT * FROM user_ip WHERE ip = \'' . $ip . '\' AND deleted = 0 AND admin = 1';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $user_id, $ip, $active)
	{	
		if ($id > 0) {
			$sql =
			"UPDATE `user_ip` SET
				modified_by = " . $_SESSION['user_id'] . ",
				ip = '" . trim(str_replace("'", "\'", $ip)) . "',
				active = '" . trim(str_replace("'", "\'", $active)) . "'
			WHERE
				id = " . trim(str_replace("'", "\'", $id)) . ";";
		} else {
			$sql =
			"INSERT INTO `user_ip`
			(
				`created_by`,
				`ip`,
				`user_id`,
				`active`
			) VALUES (
				'1',
				'" . str_replace("'", "\'", trim($ip)) . "',
				'" . str_replace("'", "\'", trim($user_id)) . "',
				'" . str_replace("'", "\'", trim($active)) . "'
			);";
		}

		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `user_ip` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}