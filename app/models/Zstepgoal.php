<?php
class Zstepgoal extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM zstepgoal WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getUserRecord($user_id)
	{
		$sql = 'SELECT * FROM zstepgoal WHERE user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($conn, $limit = 0)
	{
		if ($limit > 0) {
			$sql = "SELECT * FROM zstepgoal LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM zstepgoal";
		}
		
		$result = $conn->query($sql);

		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getNextId()
	{
		$sql = "SELECT * FROM zstepgoal ORDER BY id DESC LIMIT 1";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($conn, $user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM zstepgoal;";
		
		$result = $conn->query($sql);
		
		if (mysqli_num_rows($result) == 0)
		{
			die();
		}
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getUserRecords($conn, $user_id)
	{
		$sql = "SELECT * FROM zstepgoal WHERE user_id = " . $user_id;

		$result = $conn->query($sql);

		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($conn)
	{
		$sql = "SELECT * FROM zstepgoal";
		
		$result = $conn->query($sql);
		
		$return = array();
		
		while($row = $result->fetch_assoc())
		{
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM zstepgoal WHERE 1;';
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $Z_PK, $user_id, $goal)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `zstepgoal` SET
				goal = '" . str_replace("'", "\'", trim($goal)) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `zstepgoal`(`id`,`created_by`,`modified_by`,`modified_date`,`user_id`,`goal`) VALUES ('" . str_replace("'", "\'", trim($Z_PK)) . "',1,1,'2018-01-01 11:11:11','" . str_replace("'", "\'", trim($user_id)) . "','" . str_replace("'", "\'", trim($goal)) . "');";
		}
		
		$return = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `zstepgoal` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}