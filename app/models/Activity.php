<?php
class Activity extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM activity WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($user_id)
	{
		$sql = 'SELECT * FROM activity WHERE deleted = 0 ORDER BY id DESC;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecords($include_children = false)
	{			
		if ($include_children == true) {
			$sql = 'SELECT * FROM activity WHERE deleted = 0 ORDER BY id;';
		} else {
			$sql = 'SELECT * FROM activity WHERE deleted = 0 AND parent_id = 0 ORDER BY id;';
		}

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($activity_id, $user_id, $action)
	{
		if ($activity_id > 0) {
			$sql =
			"UPDATE `activity` SET
				modified_by = " . $_SESSION['user_id'] . ",
				user_id = '" . str_replace("'", "\'", $user_id) . "',
				action = '" . str_replace("'", "\'", $action) . "'
			WHERE
				id = " . str_replace("'", "\'", $activity_id) . ";";
		} else {
			$sql =
			"INSERT INTO `activity`
			(
				`created_by`,  
				`user_id`,
				`action`
			) VALUES (
				'" . str_replace("'", "\'", $user_id) . "', 
				'" . str_replace("'", "\'", $user_id) . "', 
				'" . str_replace("'", "\'", $action) . "'
			);";
		}
		
		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `activity` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}