<?php
class Znutrientgoals extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->db = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($conn)
	{
		$sql = "SELECT * FROM znutrientgoals";

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($conn, $user_id, $date1, $date2)
	{
		$sql = "SELECT * FROM znutrientgoals;";
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = date('Y-m-d', $row['ZDATE'] + 978307200);
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['Z_PK']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
		
	}
	
	public function getUserRecords($user_id)
	{
		$sql = "SELECT * FROM znutrientgoals WHERE zuserid = " . $user_id;

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['ZCOMMON']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser($conn)
	{
		$sql = "SELECT * FROM znutrientgoals";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['ZUSERID']])) {
				$return[$row['ZUSERID']] = array();
			}
			
			$return[$row['ZUSERID']][] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE 1;';
		
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentznutrientgoalsId()
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getznutrientgoalssByznutrientgoalsType($znutrientgoals_type_id)
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE deleted = 0 AND parent_id = 0 AND znutrientgoals_type_id = ' . $znutrientgoals_type_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientgoalssByParentId($znutrientgoals_id)
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE deleted = 0 AND parent_id = ' . $znutrientgoals_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientgoalssByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['znutrientgoals_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getznutrientgoalsSizes()
	{
		$sql = 'SELECT * FROM znutrientgoals WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $Z_PK, $Z_ENT, $Z_OPT, $ZVALUE, $ZCOMMON, $ZNAME, $ZUNIT, $ZUSERID)
	{
		if ($Z_PK > 0) {
			$sql =
			"UPDATE `znutrientgoals` SET
				ZVALUE = '" . str_replace("'", "\'", trim($ZVALUE)) . "'
			WHERE
				Z_PK = " . str_replace("'", "\'", $Z_PK) . ";";
		} else {
			$sql =
			"INSERT INTO `znutrientgoals`
			(
				`Z_PK`,
				`Z_ENT`,
				`Z_OPT`,
				`ZVALUE`,
				`ZCOMMON`,
				`ZNAME`,
				`ZUNIT`,
				`ZUSERID`
			) VALUES (
				'" . str_replace("'", "\'", trim($Z_PK)) . "',
				'" . str_replace("'", "\'", trim($Z_ENT)) . "',
				'" . str_replace("'", "\'", trim($Z_OPT)) . "',
				'" . str_replace("'", "\'", trim($ZVALUE)) . "',
				'" . str_replace("'", "\'", trim($ZCOMMON)) . "',
				'" . str_replace("'", "\'", trim($ZNAME)) . "',
				'" . str_replace("'", "\'", trim($ZUNIT)) . "',
				'" . str_replace("'", "\'", trim($ZUSERID)) . "'
			);";
		}

		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `znutrientgoals` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}