<?php

class _loader extends CI_Model {
	
	public function load($page_data)
	{
		$this->load->model('companyinfo');
		$page_data['loader']['company'] = $this->companyinfo->getRecord();
		
		$page_data['loader']['uri'] = $_SERVER['REQUEST_URI'];

		//Strip first '/' in URI
		$page = substr($_SERVER['REQUEST_URI'], 1);
	
		//Strip page vars
		if (strpos($page, "?") != false) {
			$page_var = substr($page, strpos($page, "?") + 1);
			$page = substr($page, 0, strpos($page, "?"));
		}
		
		$page_data['loader']['page'] = $page;

		//Check if client or main site
		if ($_SESSION['client_id'] > 1) {

			//Find position of next '/'
			$dash_position = strpos($page, '/');
			
			if ($dash_position == false) {
				$page = '';
			} else {
				$page = substr($page, $dash_position + 1);
			}
		}

		//If empty string, then you are at the home page
		if (strlen($page) == 0) {
			$page = 'home/index';
		} else {
			//Find position of next '/'
			$dash_position = strpos($page, '/');
			
			//If it doesnt exist, you are at an index page
			if ($dash_position == '') {
				$page = $page . '/index';
			}
		}
		
		$this->load->helper('url');
		
		//redirect('http://' . $page_data['loader']['company']['site'] . '/admin');
		
		$page_data['loader']['user'] = array();
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			$page_data['loader']['user_id'] = 0;
		} else {
			$page_data['loader']['user_id'] = $_SESSION['user_id'];
			
			$this->load->model('user');
			$page_data['loader']['user'] = $this->user->getRecord($page_data['loader']['user_id']);
		}
		
		if (!isset($_SESSION['admin_user_id']) || $_SESSION['admin_user_id'] == 0) {
			$page_data['loader']['admin_user_id'] = 0;
		} else {
			$page_data['loader']['admin_user_id'] = $_SESSION['admin_user_id'];
			
			$this->load->model('user');
			$page_data['loader']['admin_user'] = $this->user->getRecord($page_data['loader']['admin_user_id']);
		}
		
		$this->load->model('page');
		$page_data['loader']['page_content'] = $this->page->getPageContent($page);


		$this->load->model('metadata');
		$page_data['loader']['meta'] = $this->metadata->getRecord();
		
		$this->load->model('client');
		$clients = $this->client->getRecords();

		//Serve 404 page
		if (isset($page_data['404']) && $page_data['404'] == true) {
			$this->load->view('_header/index.phtml', $page_data);
			$this->load->view('_404/index.phtml', $page_data);
			$this->load->view('_footer/index.phtml', $page_data);
		} else {
			if ($page == 'home/test') {
				$this->load->view('../../layouts/canvas/header.phtml', $page_data);
				$this->load->view($page . '.phtml', $page_data);
				$this->load->view('../../layouts/canvas/footer.phtml', $page_data);
			} else {
				$this->load->view('_header/index.phtml', $page_data);
				$this->load->view($page . '.phtml', $page_data);
				$this->load->view('_footer/index.phtml', $page_data);
			}
			
		}
		
		
		return $page_data;
	}
}
