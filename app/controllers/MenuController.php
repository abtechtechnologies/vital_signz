<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['vendor_id'] = $this->input->get('vendor_id');
		$this->page_data['set_product_type_id'] = $this->input->get('product_type');
		
		if ($this->page_data['set_product_type_id'] == '') {
			$this->page_data['set_product_type_id'] = 0;
		}
		
		$this->load->model('inventory');
		$this->load->model('vendor');
		$this->load->model('inventory');
		$this->load->model('product');
		$this->load->model('producttype');
		$this->load->model('productsize');
	
		$this->page_data['vendors'] = $this->vendor->getRecords();

		$this->page_data['product_sizes'] = $this->productsize->getRecordsByProduct();

		$product_types = $this->producttype->getRecords(true);
		$this->page_data['product_types_alt'] = $this->producttype->getRecords(true);
		
		$this->page_data['product_types'] = array();
		$this->page_data['product_types'][0]['name'] = 'All Products';
		
		$products = $this->product->getProductsByProductTypeId();
		
		foreach ($product_types as $id => $data) {
			if (isset($products[$id])) {
				$this->page_data['product_types'][$id] = array();
				$this->page_data['product_types'][$id] = $data;
			}
		}
		
		if ($this->page_data['vendor_id'] != '') {
			$this->load->model('vendor');
			$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
			
			$this->page_data['products'] = $this->product->getProductsByVendorId($this->page_data['vendor_id']);
		} else {
			$this->page_data['products'] = $this->product->getRecords();
		}
		
		foreach ($this->page_data['products'] as $id => $data) {
			$this->page_data['products'][$id]['product_inventory'] = $this->inventory->getProductInventory($id);
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['vendor_id'] = $this->input->get('vendor_id');
		$this->page_data['set_product_type_id'] = $this->input->get('product_type');
		
		if ($this->page_data['set_product_type_id'] == '') {
			$this->page_data['set_product_type_id'] = 0;
		}
		
		$this->load->model('inventory');
		$this->load->model('vendor');
		$this->load->model('inventory');
		$this->load->model('product');
		$this->load->model('producttype');
		$this->load->model('productsize');
	
		$this->page_data['vendors'] = $this->vendor->getRecords();
		$this->page_data['product_sizes'] = $this->productsize->getRecordsByProduct();

		$product_types = $this->producttype->getRecords(true);
		$this->page_data['product_types_alt'] = $this->producttype->getRecords(true);
		
		$this->page_data['product_types'] = array();
		$this->page_data['product_types'][0]['name'] = 'All Products';
		
		$products = $this->product->getProductsByProductTypeId();
		
		foreach ($product_types as $id => $data) {
			if (isset($products[$id])) {
				$this->page_data['product_types'][$id] = array();
				$this->page_data['product_types'][$id] = $data;
			}
		}
		
		if ($this->page_data['vendor_id'] != '') {
			$this->load->model('vendor');
			$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
			
			$this->page_data['products'] = $this->product->getProductsByVendorId($this->page_data['vendor_id']);
		} else {
			$this->page_data['products'] = $this->product->getRecords();
		}
		
		foreach ($this->page_data['products'] as $id => $data) {
			$this->page_data['products'][$id]['product_inventory'] = $this->inventory->getProductInventory($id);
		}
				
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
}