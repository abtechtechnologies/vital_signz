<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sitemap()
	{
		$this->load->view('home/urllist.txt');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->page_data['signup'] = $this->input->get('signup');

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function loginAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();

		$this->load->model('zuser');
		$users = $this->zuser->getRecords();

		$found = false;
		
		foreach ($users as $id => $data) {
			if (strtolower($data['ZEMAIL']) == strtolower($this->input->post('email'))) {
				
				if (hash_equals($data['ZPASSWORD'], crypt($this->input->post('password'), $data['ZPASSWORD'])) || $data['ZPASSWORD'] == $this->input->post('password')) {
					$found = true;
					$user_id = $data['Z_PK'];
					break;
				}
			} else {
				$user_id = 0;
			}
		}
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();

		if ($found == true) {
		
			$_SESSION['user_id'] = $user_id;
			
			$user = $this->zuser->getRecord($user_id);
			
			if ($user['command'] == 1) {
				$_SESSION['company_id'] = $user['company_id'];
			}

			$this->load->model('activity');
			$status = $this->activity->writeData(0, $user_id, 'Logged in');
			
			$status = 1;
		} else {
			$status = 'Those credentials are not found in our system.';
		}
		
		print $status;
		exit;
	}
	
	public function logoutAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('activity');
		$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Manually Logged Out');
		
		$_SESSION['user_id'] = 0;
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$this->load->helper('url');
		redirect('http://' . $company_info['site'] . '/web/login');
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('producttype');
		$this->page_data['product_types'] = $this->producttype->getRecords(true);
		
		$this->load->model('product');
		$this->page_data['sub_products'] = $this->product->getProductSizes();
		
		foreach ($this->page_data['product_types'] as $id => $data) {
			$products = $this->product->getProductsByProductType($id);
			
			foreach ($products as $id2 => $data2) {
				$this->page_data['products'][$id][$id2] = $data2;
			}
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function location()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/location';
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function elements()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/elements';
	
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function saleTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/sale-test';
		
		 require 'lib/paytrace/vendor/autoload.php';
		
		$client = new GuzzleHttp\Client();
		
		// DRY
		$auth_server = 'https://api.paytrace.com';
		
		// send a request to the authentication server
		// note: normally, you'd store the username/password in a more secure fashion!
		$res = $client->post($auth_server . "/oauth/token", [
			'body' => [
				'grant_type' => 'password', 
				'username' => 'JoeAhart', 
				'password' => 'Tranzq123!'
			]
		]);
		
		// grab the results (as JSON)
		$json = $res->json();
		
		// get the access token
		$token = $json['access_token'];
		
		// create a fake sale transaction
		$sale_data = array(
			"amount" => 19.99, 
			"credit_card" => array(
				"number" => "4111111111111111",
				"expiration_month" => "12",
				"expiration_year" => "2020"
			),
			"billing_address" => array(
				"name" => "Steve Smith", 
				"street_address" => "8320 E. West St.",
				"city" => "Spokane",
				"state" => "WA", 
				"zip" => "85284"
			)
		);
		
		try {
			// post the transaction to hermes
			$res = $client->post($auth_server . "/v1/transactions/sale/keyed", [
				'headers' => [  'Authorization' => "Bearer " . $token],
				'json' => $sale_data
			]);
		} catch (Exception $e) {
			//@TODO
				print_r('!!! - ' . $e->getMessage());
				exit;
		}
		
		$response = $res->json();
		
		// dump the json results
		$result = var_export($res->json());
			
		//@TODO
			print '<pre>';
			print_r($result);
			print '</pre>';
			exit;
	}

}