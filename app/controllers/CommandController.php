<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommandController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('zuser');
		$user = $this->zuser->getRecord($_SESSION['user_id']);
		
		$this->load->model('zalert');
		$this->page_data['alerts'] = $this->zalert->getCompanyRecords($_SESSION['company_id']);

		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;
		
		foreach ($company_users as $id => $data) {
			if ($data['age'] > 0) {
				$age_count++;
				$total_age += $data['age'];
			}
			
			if ($data['sex'] == 'Male') {
				$male_count++;
			} else if ($data['sex'] == 'Female') {
				$female_count++;
			} else if ($data['sex'] == 'Trans') {
				$trans_count++;
			} else if ($data['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$data['age'] && (int)$data['age'] < 30) {
				$age_group1++;
			} else if (30 <= $data['age'] && (int)$data['age'] < 40) {
				$age_group2++;
			} else if (40 <= $data['age'] && (int)$data['age'] < 50) {
				$age_group3++;
			} else if (50 <= $data['age'] && (int)$data['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		$this->page_data['male'] = round(($male_count / count($company_users)) * 100, 2);
		$this->page_data['female'] = round(($female_count / count($company_users)) * 100, 2);
		$this->page_data['trans'] = round(($trans_count / count($company_users)) * 100, 2);
		$this->page_data['other'] = round(($other_count / count($company_users)) * 100, 2);
		$this->page_data['decline'] = round(($decline_count / count($company_users)) * 100, 2);
		
		$this->page_data['age_group1'] = round(($age_group1 / count($company_users)) * 100, 2);
		$this->page_data['age_group2'] = round(($age_group2 / count($company_users)) * 100, 2);
		$this->page_data['age_group3'] = round(($age_group3 / count($company_users)) * 100, 2);
		$this->page_data['age_group4'] = round(($age_group4 / count($company_users)) * 100, 2);
		$this->page_data['age_group5'] = round(($age_group5 / count($company_users)) * 100, 2);

		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		$this->page_data['age_count'] = $age_count;
		
		$this->page_data['signup'] = $this->input->get('signup');
				
		$this->page_data['company_id'] = $_SESSION['company_id'];
		
		$this->load->model('company');
		$this->page_data['company'] = $this->company->getRecord($this->page_data['company_id']);

		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function vitals () {
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}

		$this->load->model('zbloodpressure');
		$blood = $this->zbloodpressure->getCompanyRecords($company_string);
		
		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zbloodpressure');
		
		$this->page_data['records_on_graph'] = 0;
		$total_records = array();
		
		//Data for graph
		for ($i = 0; $i < count($dates); $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' month'));
			} else if ($this->page_data['filter'] == 5) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' months'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' months'));
			} else if ($this->page_data['filter'] == 1) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' hours'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' hours'));
			} else {
				$first = date('Y-m-d', strtotime( '-' . $i . ' day'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->zbloodpressure->getCompanyRecordsByDateRange($company_string, $first, $last);
			
			$day_sum_dia = 0;
			$day_sum_sys = 0;
			
			foreach ($records as $id => $data) {
				$day_sum_dia += $data['ZDIASTOLIC'];
				$day_sum_sys += $data['ZSYSTOLIC'];
				
				$this->page_data['records_on_graph']++;
				$total_records[] = $data;
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_dia'] = 0;
				$this->page_data['totals'][$last]['avg_sys'] = 0;
			} else {
				$day_avg_dia = $day_sum_dia / count($records);
				$day_avg_sys = $day_sum_sys / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['avg_dia'] = $day_avg_dia;
				$this->page_data['totals'][$last]['avg_sys'] = $day_avg_sys;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);
				
		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;

		foreach ($total_records as $id => $data) {
			$user = $this->zuser->getRecord($data['ZUSERID']);

			if ($user['age'] > 0) {
				$age_count++;
				$total_age += $user['age'];
			}
			
			if ($user['sex'] == 'Male') {
				$male_count++;
			} else if ($user['sex'] == 'Female') {
				$female_count++;
			} else if ($user['sex'] == 'Trans') {
				$trans_count++;
			} else if ($user['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$user['age'] && (int)$user['age'] < 30) {
				$age_group1++;
			} else if (30 <= $user['age'] && (int)$user['age'] < 40) {
				$age_group2++;
			} else if (40 <= $user['age'] && (int)$user['age'] < 50) {
				$age_group3++;
			} else if (50 <= $user['age'] && (int)$user['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		$this->page_data['male'] = round(($male_count / count($total_records)) * 100, 2);
		$this->page_data['female'] = round(($female_count / count($total_records)) * 100, 2);
		$this->page_data['trans'] = round(($trans_count / count($total_records)) * 100, 2);
		$this->page_data['other'] = round(($other_count / count($total_records)) * 100, 2);
		$this->page_data['decline'] = round(($decline_count / count($total_records)) * 100, 2);
		
		$this->page_data['age_group1'] = round(($age_group1 / count($total_records)) * 100, 2);
		$this->page_data['age_group2'] = round(($age_group2 / count($total_records)) * 100, 2);
		$this->page_data['age_group3'] = round(($age_group3 / count($total_records)) * 100, 2);
		$this->page_data['age_group4'] = round(($age_group4 / count($total_records)) * 100, 2);
		$this->page_data['age_group5'] = round(($age_group5 / count($total_records)) * 100, 2);
		
		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		
		$this->page_data['age_count'] = $age_count;
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function steps () {
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}
		
		$this->load->model('zsteps');
		$steps = $this->zsteps->getCompanyRecords($company_string);

		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zsteps');
		
		$this->page_data['records_on_graph'] = 0;
		$total_records = array();
		
		//Data for graph
		for ($i = 0; $i < count($dates); $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' month'));
			} else if ($this->page_data['filter'] == 5) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' months'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' months'));
			} else if ($this->page_data['filter'] == 1) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' hours'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' hours'));
			} else {
				$first = date('Y-m-d', strtotime( '-' . $i . ' day'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->zsteps->getCompanyRecordsByDateRange($company_string, $first, $last);

			$step_sum = 0;
			
			foreach ($records as $id => $data) {
				$step_sum += $data['steps'];
				
				$this->page_data['records_on_graph']++;
				$total_records[] = $data;
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['step_avg'] = 0;
			} else {
				$step_avg = $step_sum / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['step_avg'] = $step_avg;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);

		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;
		
		foreach ($total_records as $id => $data) {
			$user = $this->zuser->getRecord($data['user_id']);
			
			if ($user['age'] > 0) {
				$age_count++;
				$total_age += $user['age'];
			}
			
			if ($user['sex'] == 'Male') {
				$male_count++;
			} else if ($user['sex'] == 'Female') {
				$female_count++;
			} else if ($user['sex'] == 'Trans') {
				$trans_count++;
			} else if ($user['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$user['age'] && (int)$user['age'] < 30) {
				$age_group1++;
			} else if (30 <= $user['age'] && (int)$user['age'] < 40) {
				$age_group2++;
			} else if (40 <= $user['age'] && (int)$user['age'] < 50) {
				$age_group3++;
			} else if (50 <= $user['age'] && (int)$user['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		$this->page_data['male'] = round(($male_count / count($total_records)) * 100, 2);
		$this->page_data['female'] = round(($female_count / count($total_records)) * 100, 2);
		$this->page_data['trans'] = round(($trans_count / count($total_records)) * 100, 2);
		$this->page_data['other'] = round(($other_count / count($total_records)) * 100, 2);
		$this->page_data['decline'] = round(($decline_count / count($total_records)) * 100, 2);
		
		$this->page_data['age_group1'] = round(($age_group1 / count($total_records)) * 100, 2);
		$this->page_data['age_group2'] = round(($age_group2 / count($total_records)) * 100, 2);
		$this->page_data['age_group3'] = round(($age_group3 / count($total_records)) * 100, 2);
		$this->page_data['age_group4'] = round(($age_group4 / count($total_records)) * 100, 2);
		$this->page_data['age_group5'] = round(($age_group5 / count($total_records)) * 100, 2);
		
		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		
		$this->page_data['age_count'] = $age_count;
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function diet () {
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}

		$this->load->model('zfooddata');
		$food = $this->zfooddata->getCompanyRecords($company_string);

		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zfooddata');

		$this->page_data['records_on_graph'] = 0;
		$total_records = array();
		
		//Data for graph
		for ($i = 0; $i < count($dates); $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' month'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' month'));
			} else if ($this->page_data['filter'] == 5) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' months'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' months'));
			} else if ($this->page_data['filter'] == 1) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' hours'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' hours'));
			} else {
				$first = date('Y-m-d', strtotime( '-' . $i . ' day'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->zfooddata->getCompanyRecordsByDateRange($company_string, $first, $last);
			
			$calorie_sum = 0;
			$cholesterol_sum = 0;
			$dietary_fiber_sum = 0;
			$potassium_sum = 0;
			$protein_sum = 0;
			$saturated_fat_sum = 0;
			$sodium_sum = 0;
			$sugars_sum = 0;
			$total_carbohydrate_sum = 0;
			$total_fat_sum = 0;
			
			foreach ($records as $id => $data) {

				$calorie_sum += $data['calories'];
				$cholesterol_sum += $data['cholesterol'];
				$dietary_fiber_sum += $data['dietary_fiber'];
				$potassium_sum += $data['potassium'];
				$protein_sum += $data['protein'];
				$saturated_fat_sum += $data['saturated_fat'];
				$sodium_sum += $data['sodium'];
				$sugars_sum += $data['sugars'];
				$total_carbohydrate_sum += $data['total_carbohydrate'];
				$total_fat_sum += $data['total_fat'];
				
				$this->page_data['records_on_graph']++;
				$total_records[] = $data;
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_calories'] = 0;
				$this->page_data['totals'][$last]['avg_cholesterol'] = 0;
				$this->page_data['totals'][$last]['avg_dietary_fiber'] = 0;
				$this->page_data['totals'][$last]['avg_potassium'] = 0;
				$this->page_data['totals'][$last]['avg_protein'] = 0;
				$this->page_data['totals'][$last]['avg_saturated_fat'] = 0;
				$this->page_data['totals'][$last]['avg_sodium'] = 0;
				$this->page_data['totals'][$last]['avg_sugars'] = 0;
				$this->page_data['totals'][$last]['avg_total_carbohydrate'] = 0;
				$this->page_data['totals'][$last]['avg_total_fat'] = 0;
			} else {
				$this->page_data['totals'][$last]['records'] = $records;
				
				//***START NUTRIENTS***
				$avg_calories = round($calorie_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_calories'] = $avg_calories;
				
				$avg_cholesterol = round($cholesterol_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_cholesterol'] = $avg_cholesterol;
				
				$avg_dietary_fiber = round($dietary_fiber_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_dietary_fiber'] = $avg_dietary_fiber;
				
				$avg_potassium = round($potassium_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_potassium'] = $avg_potassium;
				
				$avg_protein = round($protein_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_protein'] = $avg_protein;
				
				$avg_saturated_fat = round($saturated_fat_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_saturated_fat'] = $avg_saturated_fat;
				
				$avg_sodium = round($sodium_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_sodium'] = $avg_sodium;
				
				$avg_sugars = round($sugars_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_sugars'] = $avg_sugars;
				
				$avg_total_carbohydrate = round($total_carbohydrate_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_total_carbohydrate'] = $avg_total_carbohydrate;
				
				$avg_total_fat = round($total_fat_sum / count($records), 1);
				$this->page_data['totals'][$last]['avg_total_fat'] = $avg_total_fat;
			}
			
		}

		$this->page_data['dates'] = array_reverse($this->page_data['totals']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);

		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;

		foreach ($total_records as $id => $data) {
			$user = $this->zuser->getRecord($data['user_id']);
			
			if ($user['age'] > 0) {
				$age_count++;
				$total_age += $user['age'];
			}
			
			if ($user['sex'] == 'Male') {
				$male_count++;
			} else if ($user['sex'] == 'Female') {
				$female_count++;
			} else if ($user['sex'] == 'Trans') {
				$trans_count++;
			} else if ($user['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$user['age'] && (int)$user['age'] < 30) {
				$age_group1++;
			} else if (30 <= $user['age'] && (int)$user['age'] < 40) {
				$age_group2++;
			} else if (40 <= $user['age'] && (int)$user['age'] < 50) {
				$age_group3++;
			} else if (50 <= $user['age'] && (int)$user['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		if (count($total_records) == 0) {
			$this->page_data['no_records'] = true;
		} else {
			$this->page_data['no_records'] = false;
			
			$this->page_data['male'] = round(($male_count / count($total_records)) * 100, 2);
			$this->page_data['female'] = round(($female_count / count($total_records)) * 100, 2);
			$this->page_data['trans'] = round(($trans_count / count($total_records)) * 100, 2);
			$this->page_data['other'] = round(($other_count / count($total_records)) * 100, 2);
			$this->page_data['decline'] = round(($decline_count / count($total_records)) * 100, 2);
			
			$this->page_data['age_group1'] = round(($age_group1 / count($total_records)) * 100, 2);
			$this->page_data['age_group2'] = round(($age_group2 / count($total_records)) * 100, 2);
			$this->page_data['age_group3'] = round(($age_group3 / count($total_records)) * 100, 2);
			$this->page_data['age_group4'] = round(($age_group4 / count($total_records)) * 100, 2);
			$this->page_data['age_group5'] = round(($age_group5 / count($total_records)) * 100, 2);
		}
		
		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		$this->page_data['age_count'] = $age_count;
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function meds () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();	
		
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}
		
		$this->load->model('zmedicationdata');
		$this->page_data['meds'] = $this->zmedicationdata->getCompanyRecords($company_string);

		$this->page_data['filter'] = $this->input->get('filter');
				
		$dates = array();
		
		if ($this->page_data['filter'] == 1) {
			$filter_count = 24;
		} elseif ($this->page_data['filter'] == '' || $this->page_data['filter'] == 2) {
			$filter_count = 7;
		} elseif ($this->page_data['filter'] == 3) {
			$filter_count = 30;
		} elseif ($this->page_data['filter'] == 4) {
			$filter_count = 12;
		} else {
			
		}
		

		foreach ($this->page_data['meds'] as $med_name => $med_data) {
			
			$this->page_data['meds'][$med_name]['count'] = count($med_data['records']);
			$this->page_data['meds'][$med_name]['amount'] = 0;

			foreach ($med_data['records'] as $i => $record) {
				$this->page_data['meds'][$med_name]['units'] = $record['ZUNITS'];
				
				$this->page_data['meds'][$med_name]['amount'] += $record['ZAMOUNT'];
			}
			
			$this->page_data['meds'][$med_name]['amount_avg'] = round($this->page_data['meds'][$med_name]['amount'] / count($med_data['records']), 1);
		}
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function tests () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}
		
		$this->load->model('labs');
		$this->page_data['labs'] = $this->labs->getCompanyRecords($company_string);

		$this->load->model('zdatasets');
		
		$this->page_data['u_labs'] = array();
		
		foreach ($this->page_data['labs'] as $id => $data) {
			foreach ($data['records'] as $i => $record) {

				$match = $this->zdatasets->getNameMatch($record['ZNAME']);
				
				if (count($match) > 0) {
					$this->page_data['labs'][$id]['records'][$i]['test'] = $match;
					$this->page_data['u_labs'][$record['ZNAME']] = $record['ZNAME'];
				} else {
					
				}
			}
		}

		$this->page_data['filter'] = $this->input->get('filter');

		foreach ($this->page_data['labs'] as $test_name => $test_data) {
			

			$this->page_data['labs'][$test_name]['count'] = count($test_data['records']);
			$this->page_data['labs'][$test_name]['amount'] = 0;
			
			foreach ($test_data['records'] as $i => $record) {
				
				if (!isset($record['test'])) {
					$this->page_data['labs'][$test_name]['units'] = '';
				} else {
					$this->page_data['labs'][$test_name]['units'] = $record['test']['ZUNITS'];
				}
				
				$this->page_data['labs'][$test_name]['amount'] += $record['ZVALUE'];
			}
			
			$this->page_data['labs'][$test_name]['amount_avg'] = round($this->page_data['labs'][$test_name]['amount'] / count($test_data['records']), 1);
		}
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function viewMed () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$this->page_data['med_name'] = $this->input->get('name');
		$med_name = $this->page_data['med_name'];
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}
		
		$this->load->model('zmedicationdata');
		$this->page_data['meds'] = $this->zmedicationdata->getCompanyRecordsByName($company_string, $this->page_data['med_name']);
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zmedicationdata', $med_name);

		$this->page_data['records_on_graph'] = 0;
		$total_records = array();

		foreach ($this->page_data['meds'] as $id => $data) {
			
			if ($this->page_data['filter'] == 1) {
				//**************HOURS FOR A SINGLE DAY****************
				$this->page_data['meds'][$med_name][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
				$this->page_data['meds'][$med_name][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
				
				$count = 0;
				
				foreach ($dates as $date => $date_record) {
					
					$parts = explode('/', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date_string = $parts[0] . '/' . $parts[1] . '/' . $parts2[0] . ' ' . $parts2[1];
					$new_date = strtotime($new_date_string);
					$med_time = strtotime(str_replace('-', '/', $data['date']));
					
					if ($med_time <= $new_date && $med_time >= strtotime($new_date_string . ' - 1hour')) {
						$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						$this->page_data['records_on_graph']++;
						$total_records[] = $data;
					} else {
						
					}
					
					$count++;
				}
				//**************END HOURS FOR A SINGLE DAY****************
			} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
				//**************MONTHS FOR A YEAR****************
				$count = 0;

				foreach ($dates as $date => $date_record) {
					
					$parts = explode('/', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date_string = $parts[0] . '/' . $parts[1] . '/' . $parts2[0] . ' ' . $parts2[1];
					$new_date = strtotime($new_date_string);
					$med_time = strtotime(str_replace('-', '/', $data['date']));
					
					if (count($dates) > 4) {
						$next_date = date('Y/m/d H:i:s', strtotime($new_date_string . ' - 1week'));
					} else {
						$next_date = date('Y/m/d H:i:s', strtotime($new_date_string . ' - 1month'));
					}

					if ($med_time <= $new_date && $med_time >= strtotime($next_date)) {
						$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						$this->page_data['records_on_graph']++;
						$total_records[] = $data;
					} else {
						
					}
					
					$count++;
				}
				//**************END MONTHS FOR A YEAR****************
			} else {
				$parts = explode(' ', str_replace('-', '/', $data['date']));
				
				$record_date = $parts[0];

				if (isset($dates[$record_date])) {
					$dates[$record_date]['tests'][$data['Z_PK']] = $data;
					
					$this->page_data['records_on_graph']++;
					$total_records[] = $data;
				} 
			}
		}
		
		foreach ($dates as $id => $date_data) {
			if (!isset($dates[$id]['tests'])) {
				$dates[$id]['tests'] = array();
			}
		}

		foreach ($dates as $id => $date_data) {
			$total = 0;
			
			if (count($date_data) > 0) {
				foreach ($date_data['tests'] as $med_id => $data) {
					$total += $data['ZAMOUNT'];
				}
				
				if (count($date_data['tests']) == 0) {
					$dates[$id]['avg'] = 0;
				} else {
					$dates[$id]['avg'] = $total / count($date_data['tests']);
				}
			} else {
				$dates[$id]['avg'] = 0;
			}
			
		}

		$this->page_data['dates'] = array_reverse($dates);
		
		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;
		
		foreach ($total_records as $id => $data) {
			$user = $this->zuser->getRecord($data['ZUSERID']);
			
			if ($user['age'] > 0) {
				$age_count++;
				$total_age += $user['age'];
			}
			
			if ($user['sex'] == 'Male') {
				$male_count++;
			} else if ($user['sex'] == 'Female') {
				$female_count++;
			} else if ($user['sex'] == 'Trans') {
				$trans_count++;
			} else if ($user['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$user['age'] && (int)$user['age'] < 30) {
				$age_group1++;
			} else if (30 <= $user['age'] && (int)$user['age'] < 40) {
				$age_group2++;
			} else if (40 <= $user['age'] && (int)$user['age'] < 50) {
				$age_group3++;
			} else if (50 <= $user['age'] && (int)$user['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		if (count($total_records) == 0) {
			$this->page_data['no_records'] = true;
		} else {
			$this->page_data['no_records'] = false;
			
			$this->page_data['male'] = round(($male_count / count($total_records)) * 100, 2);
			$this->page_data['female'] = round(($female_count / count($total_records)) * 100, 2);
			$this->page_data['trans'] = round(($trans_count / count($total_records)) * 100, 2);
			$this->page_data['other'] = round(($other_count / count($total_records)) * 100, 2);
			$this->page_data['decline'] = round(($decline_count / count($total_records)) * 100, 2);
			
			$this->page_data['age_group1'] = round(($age_group1 / count($total_records)) * 100, 2);
			$this->page_data['age_group2'] = round(($age_group2 / count($total_records)) * 100, 2);
			$this->page_data['age_group3'] = round(($age_group3 / count($total_records)) * 100, 2);
			$this->page_data['age_group4'] = round(($age_group4 / count($total_records)) * 100, 2);
			$this->page_data['age_group5'] = round(($age_group5 / count($total_records)) * 100, 2);
		}
		
		
		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		
		$this->page_data['age_count'] = $age_count;

		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function viewTest () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zuser');
		$company_users = $this->zuser->getCompany($_SESSION['company_id']);
		
		$this->page_data['test_name'] = $this->input->get('name');
		$test_name = $this->page_data['test_name'];
		
		$company_string = '';
		
		$count = 1;
		foreach ($company_users as $id => $data) {
			if ($count < count($company_users)) {
				$company_string .= $id . ',';
			} else {
				$company_string .= $id;
			}
			
			$count++;
		}
		
		$this->load->model('zdatasets');
		$this->page_data['dataset'] = $this->zdatasets->getNameMatch($this->page_data['test_name']);
		
		$this->load->model('labs');
		$this->page_data['labs'] = $this->labs->getCompanyRecordsByName($company_string, $this->page_data['test_name']);

		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zlabdata', $test_name);
		
		$this->page_data['records_on_graph'] = 0;
		$total_records = array();
		
		foreach ($this->page_data['labs'] as $id => $data) {
			
			if ($this->page_data['filter'] == 1) {
				//**************HOURS FOR A SINGLE DAY****************
				$this->page_data['labs'][$test_name][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
				$this->page_data['labs'][$test_name][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
				
				$count = 0;
				
				foreach ($dates as $date => $date_record) {
					
					$parts = explode('/', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date_string = $parts[0] . '/' . $parts[1] . '/' . $parts2[0] . ' ' . $parts2[1];
					$new_date = strtotime($new_date_string);
					$med_time = strtotime(str_replace('-', '/', $data['date']));
					
					if ($med_time <= $new_date && $med_time >= strtotime($new_date_string . ' - 1hour')) {
						$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
						$this->page_data['records_on_graph']++;
						$total_records[] = $data;
					} else {
						
					}
					
					$count++;
				}
				//**************END HOURS FOR A SINGLE DAY****************
			} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
				//**************MONTHS FOR A YEAR****************
				$count = 0;
				
				foreach ($dates as $date => $date_record) {
					
					$parts = explode('/', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date_string = $parts[0] . '/' . $parts[1] . '/' . $parts2[0] . ' ' . $parts2[1];
					$new_date = strtotime($new_date_string);
					$med_time = strtotime(str_replace('-', '/', $data['date']));
					
					if (count($dates) > 4) {
						$next_date = date('Y/m/d H:i:s', strtotime($new_date_string . ' - 1month'));
					} else {
						$next_date = date('Y/m/d H:i:s', strtotime($new_date_string . ' - 1week'));
					}

					
					if ($med_time <= $new_date && $med_time >= strtotime($next_date)) {
						$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
						$this->page_data['records_on_graph']++;
						$total_records[] = $data;
					} else {
						
					}
					
					$count++;
				}
				//**************END MONTHS FOR A YEAR****************
			} else {
				$parts = explode(' ', str_replace('-', '/', $data['date']));
				
				$record_date = $parts[0];
				
				if (isset($dates[$record_date])) {
					$dates[$record_date]['tests'][$data['Z_PK']] = $data;
					
					$this->page_data['records_on_graph']++;
					$total_records[] = $data;
				}
			}
		}

		foreach ($dates as $id => $date_data) {
			if (!isset($dates[$id]['tests'])) {
				$dates[$id]['tests'] = array();
			}
		}
		
		foreach ($dates as $id => $date_data) {
			$total = 0;
			
			if (count($date_data) > 0) {
				foreach ($date_data['tests'] as $med_id => $data) {
		
					$total += $data['ZVALUE'];
				}
				
				if (count($date_data['tests']) == 0) {
					$dates[$id]['avg'] = 0;
				} else {
					$dates[$id]['avg'] = $total / count($date_data['tests']);
				}
			} else {
				$dates[$id]['avg'] = 0;
			}
			
		}
		
		$this->page_data['dates'] = array_reverse($dates);
		
		$total_age = 0;
		$age_count = 0;
		
		$male_count = 0;
		$female_count = 0;
		$trans_count = 0;
		$other_count = 0;
		$decline_count = 0;
		
		$age_group1 = 0;
		$age_group2 = 0;
		$age_group3 = 0;
		$age_group4 = 0;
		$age_group5 = 0;
		
		foreach ($total_records as $id => $data) {
			$user = $this->zuser->getRecord($data['ZUSERID']);
			
			if ($user['age'] > 0) {
				$age_count++;
				$total_age += $user['age'];
			}
			
			if ($user['sex'] == 'Male') {
				$male_count++;
			} else if ($user['sex'] == 'Female') {
				$female_count++;
			} else if ($user['sex'] == 'Trans') {
				$trans_count++;
			} else if ($user['sex'] == 'Other') {
				$other_count++;
			} else {
				$decline_count++;
			}
			
			if (0 < (int)$user['age'] && (int)$user['age'] < 30) {
				$age_group1++;
			} else if (30 <= $user['age'] && (int)$user['age'] < 40) {
				$age_group2++;
			} else if (40 <= $user['age'] && (int)$user['age'] < 50) {
				$age_group3++;
			} else if (50 <= $user['age'] && (int)$user['age'] < 60) {
				$age_group4++;
			} else {
				$age_group5++;
			}
		}
		
		if (count($total_records) == 0) {
			$this->page_data['no_records'] = true;
		} else {
			$this->page_data['no_records'] = false;
			
			$this->page_data['male'] = round(($male_count / count($total_records)) * 100, 2);
			$this->page_data['female'] = round(($female_count / count($total_records)) * 100, 2);
			$this->page_data['trans'] = round(($trans_count / count($total_records)) * 100, 2);
			$this->page_data['other'] = round(($other_count / count($total_records)) * 100, 2);
			$this->page_data['decline'] = round(($decline_count / count($total_records)) * 100, 2);
			
			$this->page_data['age_group1'] = round(($age_group1 / count($total_records)) * 100, 2);
			$this->page_data['age_group2'] = round(($age_group2 / count($total_records)) * 100, 2);
			$this->page_data['age_group3'] = round(($age_group3 / count($total_records)) * 100, 2);
			$this->page_data['age_group4'] = round(($age_group4 / count($total_records)) * 100, 2);
			$this->page_data['age_group5'] = round(($age_group5 / count($total_records)) * 100, 2);
		}
		
		
		if ($age_count == 0) {
			$this->page_data['avg_age'] = 0;
		} else {
			$this->page_data['avg_age'] = $total_age / $age_count;
		}
		
		
		$this->page_data['age_count'] = $age_count;
		
		//Init functions and page load
		$this->load->model('_loader3');
		$this->_loader3->load($this->page_data);
	}
	
	public function writeMotdAction () {
		$this->load->model('company');
		$status = $this->company->writeMotd($_POST['company_id'], $_POST['motd']);
		
		print $status;
		exit; 
	}
	
	public function writeAlertAction () {
		
		if (!isset($_SESSION['company_id']) || $_SESSION['company_id'] == 0) {
			print 'Something has gone wrong';
			exit;
		} else {
			$this->load->model('zalert');
			$status = $this->zalert->writeData(0, $_POST['priority'], $_POST['alert'], $_SESSION['company_id']);
			
			print $status;
			exit;
		}
		
	}
}