<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {
	
	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
	
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function login()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		
		
		if ($_SESSION['client_id'] > 1) {
			$this->load->model('client');
			$page['client'] = $this->client->getRecord($_SESSION['client_id']);
		}
		
		$this->load->model('companyinfo');
		$page['company_info'] = $this->companyinfo->getRecord();
		
		$this->load->view('/admin/login.phtml', $page);
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		$records = $this->inventory->getHouseRecords();
		
		print '<pre>';
		print_r($records);
		print '</pre>';
		exit;
		$key = '64d570652b59cbf9c7a89fce5f0b0b1a';
		
		$data_string = '
		{
			"destination":
			{
				"address":
				{
					"unparsed":"6532 Mallard St, San Diego, CA, USA"
				},
					"notes":"this is a test"
			},
			"recipients":
			[{
				"name":"Joe Ahart",
				"phone":"6304000009",
				"notes":"**CLICK HERE TO CALL OR TEXT Joe** NOTES: do somthing at the door or whatever"
			}],
			"notes":"' .
			
			'some product (somesize) x 3",' .
			
			'"autoAssign":
			{
				"mode":"distance"
			}
		}';
		
		// create curl resource
		$ch = curl_init();
		
		// set url
		curl_setopt($ch, CURLOPT_URL, "https://onfleet.com/api/v2/tasks");
		
		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Authorization: Basic ' . base64_encode($key . ':' . ''),
						'Access-Control-Allow-Origin: http://wwweed.org',
						'Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS',
						'Access-Control-Max-Age: 1000',
						'Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With'
		));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		
		// $output contains the output string
		$output = curl_exec($ch);
		
		// close curl resource to free up system resources
		curl_close($ch);
		
		$status = json_decode($output, JSON_PRETTY_PRINT);
		
		if (isset($status['message'])) {
			$error = array(
							'message' => $status['message']
			);
		} else {
			$error = 0;
		}
		
		$return = array(
						'status' => $status,
						'error' => $error
		);
		
		print '<pre>';
		print_r($return);
		print '</pre>';
		exit;
	}
	
	public function loginAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load(true);
		
		$this->load->model('user');
		$users = $this->user->getRecords();
		
		$found = false;
		
		foreach ($users as $id => $data) {
			if (strtolower($data['login']) == strtolower($this->input->post('login'))) {
				
				if (hash_equals($data['password'], crypt($this->input->post('password'), $data['password'])) || $data['password'] == $this->input->post('password')) {
					$found = true;
					$user_id = $data['id'];
					break;
				}
			} else {
				$user_id = 0;
			}
		}
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		if ($found == true) {
			
			if ($_SESSION['client_id'] > 1) {
				$this->load->model('client');
				$client = $this->client->getRecord($_SESSION['client_id']);
				
				$session_user_var = $client['db_name'] . 'user_id';
				$session_admin_user_var = $client['db_name'] . 'admin_user_id';
				
				$_SESSION[$session_user_var] = $user_id;
				$_SESSION[$session_admin_user_var] = $user_id;
				
				$_SESSION['user_id'] = 0;
				$_SESSION['admin_user_id'] = 0;
			} else {
				$_SESSION['user_id'] = $user_id;
				$_SESSION['admin_user_id'] = $user_id;
			}
			
			$status = 1;
		} else {
			$status = 'Those credentials are not found in our system.';
		}
		
		print $status;
		exit;
	}
	
	public function account()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'account';
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editUser()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-user';
		
		$page['user_id'] = $this->input->get('user_id');
		
		
		if ($page['user_id'] == '') {
			$page['user_id'] = 0;
		}
		
		if ($page['user_id'] > 0) {
			$this->load->model('user');
			$page['user'] = $this->user->getRecordAdmin($page['user_id']);
			
			$page['user_type_id'] = $page['user']['user_type_id'];
			
			$this->load->model('sale');
			$page['sales'] = $this->sale->getRecordsByUserId($page['user_id']);
			
			$this->load->model('product');
			$this->load->model('productold');
			$page['products'] = $this->product->getRecordsAdmin();
			$page['product_archive'] = $this->productold->getRecordsAdmin();
			
			$this->load->model('productsize');
			$page['product_sizes'] = $this->productsize->getRecordsAdmin();
			
			// 			foreach ($page['sales'] as $id => $data) {
			// 				//OLD DB
			// 				if ($data['transaction_id'] < 390) {
			// 					$product = $this->productold->getRecordAdmin($data['product_id']);
			// 					if ($product['parent_id'] != '') {
			// 						$page['sales'][$id]['product'] = $page['product_archive'][$product['parent_id']]['name'] . " - " . $product['name'];
			// 						$page['sales'][$id]['price'] = $product['price'];
			// 					}
			// 				}
			// 			}
			
			
		} else {
			$page['user_type_id'] = $this->input->get('user_type_id');
			
			if ($page['user_type_id'] == '') {
				$page['user_type_id'] = 0;
			}
			
			$page['user'] = array(
							'deleted' => 0,
							'user_type_id' => $page['user_type_id']
			);
		}
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$this->load->model('transaction');
		$page['transactions'] = $this->transaction->getRecordsByUserId($page['user_id']);
		
		$page['total_sales'] = 0;
		
		foreach ($page['transactions'] as $id => $data) {
			$page['transactions'][$id]['product_sizes'] = $this->product->getProductSizesByTransactionId($id);
			
			if ($data['alt_cost'] > 0) {
				$page['total_sales'] += $data['alt_cost'];
			} else {
				$page['total_sales'] += $data['cost'];
			}
		}
		
		$page['membership_options'] = array(
						0 => 'None',
						1 => 'Green',
						2 => 'Gold',
						3 => 'Black'
		);
		
		if ($page['user_type_id'] == 0) {
			redirect('http://' . $company_info['site'] . '/admin?error=1');
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function checkUserInventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		$records = $this->inventory->getUserRecords($this->input->post('user_id'));
		
		$found = false;
		
		foreach ($records as $id => $data) {
			if ($data['quantity'] > 0) {
				$found = true;
			}
		}
		
		print $found;
	}
	
	public function deleteUser()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		$this->load->model('product');
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$records = $this->inventory->getUserRecords($this->input->post('user_id'));
		
		//1 => Move stock to primary holder
		if ($this->input->post('source') == 1) {
			foreach ($records as $inventory_id => $data) {
				if ($data['quantity'] > 0) {
					
					//Get primary holder of this product
					$product = $this->product->getRecord($data['product_id']);
					
					if ($product['inventory_holder_id'] > 0) {
						$holder_id = $product['inventory_holder_id'];
					} else {
						$holder_id = $company['primary_inventory_user_id'];
					}
					
					//Get primary holders inventory ID for this product
					$holder_match = $this->inventory->getMatch($holder_id, $data['product_id']);
					
					$new_quantity = $holder_match['quantity'] + $data['quantity'];
					
					//Move old stock to primary holder
					$status = $this->inventory->writeData($holder_match['id'], $new_quantity);
					
					//Zero old stock, just for safety. NOT SURE IF WANTED
					$status = $this->inventory->writeData($inventory_id, 0);
				}
				
				//Delete inventory record for this user_id/product_id combo
				$status = $this->inventory->delete($inventory_id);
			}
		} else {
			foreach ($records as $inventory_id => $data) {
				//Zero old stock, just for safety. NOT SURE IF WANTED
				$status = $this->inventory->writeData($inventory_id, 0);
				
				//Delete inventory record for this user_id/product_id combo
				$status = $this->inventory->delete($inventory_id);
			}
		}
		
		$this->load->model('user');
		$status = $this->user->delete($this->input->post('user_id'));
		
		print $status;
	}
	
	/*********************** PRODUCT TYPE *************************/
	public function editProductType()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-product-type';
		
		$page['product_type_id'] = $this->input->get('product_type_id');
		
		if ($page['product_type_id'] == '') {
			$page['product_type_id'] = 0;
		}
		
		if ($page['product_type_id'] > 0) {
			$this->load->model('producttype');
			$page['product_type'] = $this->producttype->getRecord($page['product_type_id']);
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function deleteProductType()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('producttype');
		$status = $this->producttype->delete($this->input->post('product_type_id'));
		
		print $status;
	}
	
	public function viewProductType()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-product-type';
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		
		$date = getdate();
		$page['current_date'] = $date['year'] . '-' . $date['mon'] . '-' . $date['mday'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeProductType()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'product_type_id' => $this->input->post('product_type_id'),
						'name' => $this->input->post('name'),
						'attachment' => $this->input->post('attachment'),
		);
		
		$this->load->model('producttype');
		$status = $this->producttype->writeData($data);
		
		print $status;
	}
	
	public function editMenuItem()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['menu_item_id'] = $this->input->get('menu_item_id');
		
		if ($page['menu_item_id'] == '') {
			$page['menu_item_id'] = 0;
		}
		
		$page['update_db'] = false;
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecordsList();
		
		$this->load->model('menuitemtype');
		$page['menu_item_types'] = $this->menuitemtype->getRecords();
		
		$this->load->model('inventory');
		$page['inventory'] = $this->inventory->getRecords();
		
		if ($page['menu_item_id'] > 0) {
			
			$this->load->model('menuitem');
			$page['menu_item'] = $this->menuitem->getRecord($page['menu_item_id']);
			
			$this->load->model('menuitempart');
			$page['menu_item_parts'] = $this->menuitempart->getRecordsByMenuItemId($page['menu_item_id']);
			
		} else {
			$page['menu_item_parts'] = array();
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writeMenuItem()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('menuitem');
		$this->load->model('menuitempart');
		$this->load->model('companyinfo');
		
		$menu_item_id = $this->input->post('menu_item_id');
		$menu_item_type_id = $this->input->post('menu_item_type_id');
		$name = $this->input->post('name');
		$price = $this->input->post('price');
		$attachment = $this->input->post('attachment');
		$description = $this->input->post('description');
		$active = $this->input->post('active');
		
		$status = $this->menuitem->writeData($menu_item_id, $menu_item_type_id, $name, $price, $attachment, $description, $active);
		
		$menu_item_parts = $this->menuitempart->getRecordsByMenuItemId($menu_item_id);
		$menu_item = $this->menuitem->getRecent();
		
		$parts = $this->input->post('parts');
		unset($parts[0]);
		
		foreach ($parts as $i => $values) {
			
			if ($i > count($menu_item_parts)) {
				
				$status = $this->menuitempart->writeData(0, $menu_item['id'], $values['product_id'], $values['units']);
			} else {
				$j = $i - 1;
				
				$record = array_slice($menu_item_parts, $j, 1);
				
				$status = $this->menuitempart->writeData($record[0]['id'], $menu_item['id'], $values['product_id'], $values['units']);
			}
		}
		
		if ($status == 1) {
			$this->load->model('_utility');
			
			$company = $this->companyinfo->getRecord();
			
			$this->_utility->resizeImage($attachment, $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
		}
		
		print $status;
	}
	
	public function viewMenuItem()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('menuitem');
		$page['menu_items'] = $this->menuitem->getRecords();
		
		foreach ($page['menu_items'] as $id => $data) {
			$year = substr($data['created_date'], 0, 4);
			$page['menu_items'][$id]['created_date'] = substr($data['created_date'], 5, 5) . '-' . $year;
		}
		
		$this->load->model('menuitemtype');
		$page['menu_item_types'] = $this->menuitemtype->getRecords(true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function editProductPart()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-product';
		
		$page['product_id'] = $this->input->get('product_id');
		
		if ($page['product_id'] == '') {
			$page['product_id'] = 0;
		}
		
		$page['update_db'] = false;
		
		if ($page['product_id'] > 0) {
			$this->load->model('product');
			$page['product'] = $this->product->getRecord($page['product_id']);
			
			$this->load->model('productsize');
			$page['product_sizes'] = $this->productsize->getRecordsByProductId($page['product_id']);
			
			$page['sub_products'] = $this->product->getProductsByParentId($page['product_id']);
			
			$this->load->model('productimage');
			$page['product_images'] = $this->productimage->getProductImages($page['product_id']);
			
			if (count($page['product_images']) == 0) {
				$status = $this->productimage->updateDB($page['product_id']);
				
				$page['update_db'] = true;
				
				$page['product_images'] = $this->productimage->getProductImages($page['product_id']);
			}
			
			if (count($page['sub_products']) == 1) {
				$page['sub_products'][-1] = false;
				$page['sub_products'][-2] = false;
			}
			
			if (count($page['sub_products']) == 2) {
				$page['sub_products'][-1] = false;
			}
			
			$this->load->model('inventory');
			$page['inventory'] = $this->inventory->getHouseInventory($page['product_id']);
		} else {
			$page['product_sizes'] = array();
		}
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		$this->load->model('user');
		$page['available_holders'] = $this->user->getAvailableHolders(true, false, true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writeProductPart()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		
		$data = array(
						'product_id' => $this->input->post('product_id'),
						'product_type_id' => $this->input->post('product_type_id'),
						'name' => $this->input->post('name'),
						'attachment' => $this->input->post('attachment'),
						'vendor_id' => $this->input->post('vendor_id'),
						'unit_name' => $this->input->post('unit_name'),
						'description' => $this->input->post('description'),
						'unit_price_wholesale' => $this->input->post('unit_price_wholesale'),
						'initial_inventory' => $this->input->post('initial_inventory'),
						'part' => 1,
						'active' => 1,
						'inventory_holder_id' => $this->input->post('primary_holder_id')
		);
		
		$status = $this->product->writeData($data);
		
		if ($status == 1) {
			$this->load->model('_utility');
			
			$this->_utility->resizeImage($data['attachment'], $_SERVER['DOCUMENT_ROOT']);
		}
		
		if ($this->input->post('product_id') == 0) {
			$product_id = $this->product->getRecentId();
		} else {
			$product_id = $this->input->post('product_id');
		}
		
		$product = $this->product->getRecord($product_id);
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getHouseRecord($product_id);
		
		if (count($inventory) > 0) {
			$inventory_id = $inventory['id'];
		} else {
			$inventory_id = 0;
		}
		
		if ($this->input->post('primary_holder_id') > 0) {
			$holder_user_id = $this->input->post('primary_holder_id');
		} else {
			$holder_user_id = $company['primary_inventory_user_id'];
		}
		
		$status = $this->inventory->writeData($inventory_id, $this->input->post('initial_inventory'), $product_id , $holder_user_id);
		
		$this->load->model('user');
		$users = $this->user->getAvailableHolders(true);
		
		$status = 1;
		
		foreach ($users as $user_id => $record) {
			
			$products = $this->product->getRecords();
			
			foreach ($products as $product_id => $data) {
				$inventory_record = $this->inventory->getMatch($user_id, $product_id);
				
				
				if (count($inventory_record) == 0) {
					
					$status = $this->inventory->writeData(0, 0, $product_id, $user_id);
				}
			}
			
		}
		
		print $status;
	}
	
	public function viewProductPart()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$page['products'] = $this->product->getParts();
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		foreach ($page['products'] as $id => $data) {
			$year = substr($data['created_date'], 0, 4);
			$page['products'][$id]['created_date'] = substr($data['created_date'], 5, 5) . '-' . $year;
		}
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	/*********************** PRODUCT *************************/
	public function editProduct()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$this->load->model('productpart');
		
		$page['product_id'] = $this->input->get('product_id');
		
		if ($page['product_id'] == '') {
			$page['product_id'] = 0;
		}
		
		$page['update_db'] = false;
		
		if ($page['product_id'] > 0) {
			
			$this->load->model('menuitempart');
			$page['menu_item_parts'] = $this->menuitempart->getRecordsByMenuItemId($page['product_id']);
			
			$page['product'] = $this->product->getRecord($page['product_id']);
			
			$this->load->model('productsize');
			$page['product_sizes'] = $this->productsize->getRecordsByProductId($page['product_id']);
			
			$page['sub_products'] = $this->product->getProductsByParentId($page['product_id']);
			
			$this->load->model('productimage');
			$page['product_images'] = $this->productimage->getProductImages($page['product_id']);
			
			if (count($page['product_images']) == 0) {
				$status = $this->productimage->updateDB($page['product_id']);
				
				$page['update_db'] = true;
				
				$page['product_images'] = $this->productimage->getProductImages($page['product_id']);
			}
			
			if (count($page['sub_products']) == 1) {
				$page['sub_products'][-1] = false;
				$page['sub_products'][-2] = false;
			}
			
			if (count($page['sub_products']) == 2) {
				$page['sub_products'][-1] = false;
			}
			
			$this->load->model('inventory');
			$page['inventory'] = $this->inventory->getHouseInventory($page['product_id']);
			
			$page['product_parts'] = $this->productpart->getProductPartsByProductId($page['product_id']);
		} else {
			$page['product_sizes'] = array();
			$page['menu_item_parts'] = array();
			
			$page['product_parts'] = array();
		}
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		//1 => Product Part
		//unset($page['product_types'][1]);
		
		$this->load->model('user');
		$page['available_holders'] = $this->user->getAvailableHolders(true, false, true);
		
		$page['all_product_parts'] = $this->product->getParts();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function deleteProduct()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$status = $this->product->delete($this->input->post('product_id'));
		
		print $status;
	}
	
	public function removeProductFromTransaction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('sale');
		$this->load->model('productsize');
		$this->load->model('inventory');
		
		$sales = $this->sale->getSalesByTransactionId($this->input->post('transaction_id'));
		
		foreach ($sales as $id => $data) {
			if ($this->input->post('product_id') == $data['product_id']) {
				$size = $this->productsize->getRecord($data['product_size_id']);
				$units = $size['units'];
				
				$house = $this->inventory->getHouseRecord($data['product_id']);
				$new_house = $house['quantity'] + $units;
				
				$status = $this->inventory->writeData($house['id'], $new_house, $data['product_id'], 4);
				
				if ($status == 1) {
					$status = $this->sale->delete($id);
				}
			}
		}
		
		print $status;
	}
	
	public function deleteProductSize()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$i = $this->input->post('i');
		$i--;
		
		$this->load->model('productsize');
		$product_sizes = $this->productsize->getRecordsByProductId($this->input->post('product_id'));
		
		$record = array_slice($product_sizes, $i, 1);
		
		$status = $this->productsize->delete($record[0]['id']);
		
		print $status;
	}
	
	public function viewProduct()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-product';
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecordsBackend();
		
		$this->load->model('productpart');
		$parts = $this->productpart->getRecords();
		
		$page['product_parts'] = array();
		
		foreach ($parts as $id => $data) {
			$product = $this->product->getRecord($data['menu_item_id']);
			
			if (count($product) > 0 && $product != '') {
				
				$product_part = $this->product->getRecord($data['product_id']);
				
				if (!isset($page['product_part_totals'][$data['menu_item_id']])) {
					$page['product_part_totals'][$data['menu_item_id']] = $product_part['unit_price_wholesale'];
				} else {
					$page['product_part_totals'][$data['menu_item_id']] += $product_part['unit_price_wholesale'];
				}
			}
			
		}
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		foreach ($page['products'] as $id => $data) {
			$year = substr($data['created_date'], 0, 4);
			$page['products'][$id]['created_date'] = substr($data['created_date'], 5, 5) . '-' . $year;
		}
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		$page['product_children'] = $this->product->getRecords(true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeProduct()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		
		$data = array(
						'product_id' => $this->input->post('product_id'),
						'product_type_id' => $this->input->post('product_type_id'),
						'name' => $this->input->post('name'),
						'attachment' => $this->input->post('attachment1'),
						'vendor_id' => $this->input->post('vendor_id'),
						'unit_name' => $this->input->post('unit_name'),
						'description' => $this->input->post('description'),
						'unit_price_wholesale' => $this->input->post('unit_price_wholesale'),
						'initial_inventory' => $this->input->post('initial_inventory'),
						'inventory_holder_id' => $this->input->post('primary_holder_id'),
						'combo' => $this->input->post('combo_switch')
		);
		
		$status = $this->product->writeData($data);
		
		if ($status == 1) {
			$this->load->model('_utility');
			
			$this->_utility->resizeImage($data['attachment'], $_SERVER['DOCUMENT_ROOT']);
		}
		
		$this->load->model('productpart');
		$this->load->model('productsize');
		$product_sizes = $this->productsize->getRecordsByProductId($data['product_id']);
		
		if ($this->input->post('product_id') == 0) {
			$product = $this->product->getRecent();
		} else {
			$product = $this->product->getRecord($this->input->post('product_id'));
		}
		
		$new_sizes = $this->input->post('new_sizes');
		unset($new_sizes[0]);
		
		foreach ($new_sizes as $i => $values) {
			
			if ($i > count($product_sizes)) {
				$status = $this->productsize->write(0, $product['id'], $values['name'], $values['inventory'], $values['price']);
			} else {
				$j = $i - 1;
				
				$record = array_slice($product_sizes, $j, 1);
				
				$status = $this->productsize->write($record[0]['id'], $product['id'], $values['name'], $values['inventory'], $values['price']);
			}
		}
		
		if ($this->input->post('combo_switch') == 1) {
			$product_parts = $this->productpart->getProductPartsByProductId($data['product_id']);
			
			$parts = $this->input->post('parts');
			unset($parts[0]);
			
			foreach ($parts as $i => $values) {
				
				if ($i > count($product_parts)) {
					
					$status = $this->productpart->writeData(0, $product['id'], $values['product_id'], $values['units']);
				} else {
					$j = $i - 1;
					
					$record = array_slice($product_parts, $j, 1);
					
					$status = $this->productpart->writeData($record[0]['id'], $product['id'], $values['product_id'], $values['units']);
				}
			}
		} else {
			
		}
		
		
		
		
		if ($this->input->post('product_id') == 0) {
			$product_id = $this->product->getRecentId();
		} else {
			$product_id = $this->input->post('product_id');
		}
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getHouseRecord($product_id);
		
		if (count($inventory) > 0) {
			$inventory_id = $inventory['id'];
		} else {
			$inventory_id = 0;
		}
		
		$product = $this->product->getRecord($product_id);
		
		if ($this->input->post('primary_holder_id') > 0) {
			$holder_user_id = $this->input->post('primary_holder_id');
		} else {
			$holder_user_id = $company['primary_inventory_user_id'];
		}
		
		$status = $this->inventory->writeData($inventory_id, $this->input->post('initial_inventory'), $product_id , $holder_user_id);
		
		if ($status == 1) {
			
			$this->load->model('productimage');
			
			for ($i = 1; $i < 6; $i++) {
				$status = $this->productimage->writeSingleData($product_id, $this->input->post('attachment' . $i), $i);
			}
		}
		
		$this->load->model('user');
		$users = $this->user->getAvailableHolders(true);
		
		$status = 1;
		
		foreach ($users as $user_id => $record) {
			
			$products = $this->product->getRecords();
			
			foreach ($products as $product_id => $data) {
				$inventory_record = $this->inventory->getMatch($user_id, $product_id);
				
				
				if (count($inventory_record) == 0) {
					
					$status = $this->inventory->writeData(0, 0, $product_id, $user_id);
				}
			}
			
		}
		
		print $status;
	}
	
	public function activateProduct()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'product_id' => $this->input->post('product_id'),
						'active' => $this->input->post('flag')
		);
		
		$this->load->model('product');
		$status = $this->product->activate($data);
		
		print $status;
	}
	
	public function writeProductSize()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'product_id' => $this->input->post('product_id'),
						'name' => $this->input->post('name'),
						'attachment' => $this->input->post('attachment1'),
						'price' => 0,
						'has_child' => 0,
						'parent' => 0,
						'active' => $this->input->post('active'),
						'vendor_id' => $this->input->post('vendor_id'),
						'initial_inventory' => $this->input->post('initial_inventory'),
						'unit_name' => $this->input->post('unit_name'),
						'product_type_id' => $this->input->post('product_type_id'),
						'unit_price_wholesale' => $this->input->post('unit_price_wholesale'),
						'description' => $this->input->post('description')
		);
		
		if ($this->input->post('product_id') == 0) {
			$this->load->model('product');
			$status = $this->product->writeData($data);
			
			if ($status == 1) {
				
				$product_id = $this->product->getRecentId();
				
				$this->load->model('productimage');
				
				for ($i = 1; $i < 6; $i++) {
					$status = $this->productimage->writeSingleData($product_id, $this->input->post('attachment' . $i), $i);
				}
				
				if ($status == 1) {
					$product = $this->product->getRecord($product_id);
					
					$this->load->model('companyinfo');
					$company = $this->companyinfo->getRecord();
					
					if ($product['inventory_holder_id'] > 0) {
						$holder_user_id = $product['inventory_holder_id'];
					} else {
						$holder_user_id = $company['primary_inventory_user_id'];
					}
					
					$this->load->model('inventory');
					
					$record = $this->inventory->getRecordByProductId($product_id);
					
					if (count($record) > 0) {
						$inventory_id = $record['id'];
					} else {
						$inventory_id = 0;
					}
					
					$this->load->model('user');
					$available_holders = $this->user->getAvailableHolders(true);
					
					$status = $this->inventory->writeData($inventory_id, $this->input->post('initial_inventory'), $product_id, $holder_user_id);
					
					foreach ($available_holders as $id => $data) {
						if ($id != $holder_user_id) {
							$status = $this->inventory->writeData($inventory_id, 0, $product_id, $id);
						}
					}
					
					if ($status == 1) {
						$this->load->model('_utility');
						
						if ($this->input->post('attachment1') != '') {
							$this->_utility->resizeImage($this->input->post('attachment1'), $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
						}
						
						if ($this->input->post('attachment2') != '') {
							$this->_utility->resizeImage($this->input->post('attachment2'), $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
						}
						
						if ($this->input->post('attachment3') != '') {
							$this->_utility->resizeImage($this->input->post('attachment3'), $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
						}
						
						if ($this->input->post('attachment4') != '') {
							$this->_utility->resizeImage($this->input->post('attachment4'), $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
						}
						
						if ($this->input->post('attachment5') != '') {
							$this->_utility->resizeImage($this->input->post('attachment5'), $_SERVER['DOCUMENT_ROOT'], $company['db_name']);
						}
					}
				}
			}
		} else {
			$product_id = $this->input->post('product_id');
		}
		
		$this->load->model('productsize');
		$status = $this->productsize->writeNewSize($product_id);
		
		if ($status == 1) {
			$status = $this->productsize->getRecentId();
			print $status;
			exit;
		} else {
			print $status;
			exit;
		}
		
	}
	
	public function newSaleOLD()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('productsize');
		$this->load->model('companyinfo');
		
		$page['page'] = 'new-sale';
		$page['current_url'] = $_SERVER['REQUEST_URI'];
		$vars = $_GET;
		
		date_default_timezone_set("America/Los_Angeles");
		
		$page['current_time'] = date('H:i', time());
		
		$page['set_product_type_id'] = $this->input->get('product_type_id');
		
		if ($page['set_product_type_id'] == '') {
			$page['set_product_type_id'] = 0;
		}
		
		$page['set_user_id'] = $this->input->get('set_user_id');
		
		if ($page['set_user_id'] == '') {
			$page['set_user_id'] = 0;
		}
		
		$page['set_due'] = $this->input->get('set_due');
		
		if ($page['set_due'] == '') {
			$page['set_due'] = 0;
		}
		
		$page['set_holder'] = $this->input->get('set_holder');
		
		if ($page['set_holder'] == '') {
			$page['set_holder'] = 0;
		}
		
		if ($page['set_holder'] == 0) {
			$company = $this->companyinfo->getRecord();
			
			$page['set_holder'] = $company['primary_inventory_user_id'];
		}
		
		$page['set_cashcard'] = $this->input->get('set_cashcard');
		
		if ($page['set_cashcard'] == '') {
			$page['set_cashcard'] = 0;
		}
		
		$page['set_created_date'] = $this->input->get('set_created_date');
		
		if ($page['set_created_date'] == '') {
			$page['set_created_date'] = 0;
		}
		
		$this->load->model('product');
		
		if ($page['set_product_type_id'] > 1) {
			$page['products'] = $this->product->getProductsByProductType($page['set_product_type_id']);
		} else {
			$page['products'] = $this->product->getRecords();
		}
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		$page['all_products'] = $this->product->getRecords(true);
		$page['product_sizes'] = $this->product->getProductSizes();
		
		$page['formatted_products'] = array();
		
		foreach ($page['products'] as $id => $data) {
			if (!isset($page['formatted_products'][$data['product_type_id']])) {
				$page['formatted_products'][$data['product_type_id']] = array();
			}
			
			$page['formatted_products'][$data['product_type_id']][$id] = $data;
			
			$page['products'][$id]['sizes'] = $this->productsize->getRecordsByProductId($id);
		}
		
		$page['product_sizes'] = $this->productsize->getRecordsByProduct();
		$page['all_product_sizes'] = $this->productsize->getRecords();;
		
		$this->load->model('producttype');
		$product_types = $this->producttype->getRecords(true);
		
		$page['product_types'] = array();
		$page['product_types'][0]['name'] = 'All Products';
		
		$products = $this->product->getProductsByProductTypeId();
		
		foreach ($product_types as $id => $data) {
			if (isset($products[$id])) {
				$page['product_types'][$id] = array();
				$page['product_types'][$id] = $data;
			}
		}
		
		$this->load->model('inventory');
		$page['all_inventory'] = $this->inventory->getRecords();
		$page['inventory'] = $this->inventory->getRecordsByUser();
		$page['product_inventory'] = $this->inventory->getRecordsByProduct();
		
		$this->load->model('user');
		$page['users'] = $this->user->getUsers();
		$page['user_list'] = array();
		$page['user_list'][0]['user_type_id'] = 1;
		$page['user_list'][0]['first_name'] = '- Select User -';
		$page['user_list'][0]['last_name'] = '';
		
		foreach ($page['users'] as $id => $data) {
			$page['user_list'][$id] = $data;
		}
		
		$page['users'] = $this->user->getRecordsAdmin();
		
		$page['users_first'] = $this->user->getClients();
		$page['users_last'] = $this->user->getClients(true);
		
		$page['formatted_users_first_name'] = array();
		$page['formatted_users_last_name'] = array();
		
		foreach ($page['users_first'] as $id => $data) {
			if ($data['deleted'] == 0) {
				$first_letter = ucfirst(substr($data['first_name'], 0, 1));
				
				if (!isset($page['formatted_users_first_name'][$first_letter])) {
					$page['formatted_users_first_name'][$first_letter] = array();
				}
				$page['formatted_users_first_name'][$first_letter][$id] = $data;
			}
		}
		
		foreach ($page['users_last'] as $id => $data) {
			if ($data['deleted'] == 0) {
				$first_letter_alt = ucfirst(substr($data['last_name'], 0, 1));
				
				if (!isset($page['formatted_users_last_name'][$first_letter_alt])) {
					$page['formatted_users_last_name'][$first_letter_alt] = array();
				}
				
				$page['formatted_users_last_name'][$first_letter_alt][$id] = $data;
			}
		}
		
		$this->load->model('cart');
		$carts = $this->cart->getRecords();
		$page['carts'] = array();
		$count = 0;
		
		foreach ($carts as $id => $data) {
			
			//Make sure the size isnt deleted , shouldnt ever happen
			if (isset($page['all_product_sizes'][$data['product_size_id']])) {
				//Make sure the parent isnt deleted
				if (isset($page['all_products'][$data['product_id']])) {
					
					
					
					
					if (!isset($page['carts'][$data['user_id']]['cart_string'])) {
						$page['carts'][$data['user_id']]['cart_string'] = '';
					}
					
					if (isset($page['carts'][$data['user_id']]['total'])) {
						$page['carts'][$data['user_id']]['total'] += $page['all_product_sizes'][$data['product_size_id']]['price'] * $data['quantity'];
					} else {
						$page['carts'][$data['user_id']]['total'] = $page['all_product_sizes'][$data['product_size_id']]['price'] * $data['quantity'];
					}
					
					$page['carts'][$data['user_id']]['name'] =
					$page['users'][$data['user_id']]['first_name'] . ' ' .
					$page['users'][$data['user_id']]['last_name'];
					
					if (count($data) <= $count) {
						$page['carts'][$data['user_id']]['cart_string'] .= $page['all_products'][$data['product_id']]['name'] . ' (' . $page['all_product_sizes'][$data['product_size_id']]['name'] . ' x' . $data['quantity'] . ') - ';
					} else {
						$page['carts'][$data['user_id']]['cart_string'] .= $page['all_products'][$data['product_id']]['name'] . ' (' . $page['all_product_sizes'][$data['product_size_id']]['name'] . ' x' . $data['quantity'] . ') / ';
					}
					
					$count++;
					
					$page['carts'][$data['user_id']][$id] = $data;
					
					$page['carts'][$data['user_id']][$id]['units_adj'] = $page['all_product_sizes'][$data['product_size_id']]['units'] * $data['quantity'];
					
					if (isset($page['product_inventory'][$data['product_id']][4])) {
						$page['carts'][$data['user_id']][$id]['house_inv'] = $page['product_inventory'][$data['product_id']][4]['quantity'];
						$page['carts'][$data['user_id']][$id]['unit_name'] = $page['product_inventory'][$data['product_id']][4]['unit_name'];
					} else {
						$page['carts'][$data['user_id']][$id]['house_inv'] = 0;
						$page['carts'][$data['user_id']][$id]['unit_name'] = 0;
					}
					
				}
			}
		}
		
		$this->load->model('vendor');
		$page['growers'] = $this->vendor->getRecords();
		
		if ($page['set_user_id'] > 0) {
			
			$page['set_total'] = 0;
			
			if (isset($page['carts'][$page['set_user_id']])) {
				foreach ($page['carts'][$page['set_user_id']] as $sale_id => $data) {
					if (is_numeric($sale_id)) {
						$record = $this->productsize->getRecord($data['product_size_id']);
						
						$page['set_total'] += $record['price'] * $data['quantity'];
					}
				}
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function newSale()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$users = $this->user->getAvailableHolders(true);
		
		$this->load->model('product');
		$this->load->model('inventory');
		
		$status = 1;
		
		foreach ($users as $user_id => $record) {
			
			$products = $this->product->getRecords();
			
			foreach ($products as $product_id => $data) {
				$inventory_record = $this->inventory->getMatch($user_id, $product_id);
				
				
				if (count($inventory_record) == 0) {
					
					$status = $this->inventory->writeData(0, 0, $product_id, $user_id);
				}
			}
		}
		
		$this->load->model('productsize');
		$this->load->model('companyinfo');
		$this->load->model('producttype');
		$this->load->model('vendor');
		
		$page['set_user_id'] = $this->input->get('user_id');
		
		$company_info = $this->companyinfo->getRecord();
		
		$page['current_url'] = $_SERVER['REQUEST_URI'];
		$vars = $_GET;
		
		date_default_timezone_set("America/Los_Angeles");
		
		$page['current_time'] = date('H:i', time());
		
		
		$page['vendors'] = $this->vendor->getRecords();
		$page['products'] = $this->product->getRecords(true);
		$page['product_sizes'] = $this->productsize->getRecords();
		
		foreach ($page['products'] as $id => $data) {
			$page['products'][$id]['sizes'] = $this->productsize->getRecordsByProductId($id);
		}
		
		$product_types = $this->producttype->getRecords(true);
		
		$page['product_types'] = array();
		$page['product_types'][0]['name'] = 'All Products';
		
		$products = $this->product->getProductsByProductTypeId();
		
		foreach ($product_types as $id => $data) {
			if (isset($products[$id])) {
				$page['product_types'][$id] = array();
				$page['product_types'][$id] = $data;
			}
		}
		
		$page['inventory'] = $this->inventory->getRecords();
		$page['user_inventory'] = $this->inventory->getRecordsByUser();
		$page['product_inventory'] = $this->inventory->getRecordsByProduct();
		
		$this->load->model('user');
		
		$page['users'] = $this->user->getRecordsAdmin();
		$page['holders'] = $this->user->getAvailableHolders(true);
		
		$page['sales_people'] = $page['holders'];
		$page['sales_people'][0]['id'] = 0;
		$page['sales_people'][0]['first_name'] = '- No Set Sales Person -';
		$page['sales_people'][0]['last_name'] = '';
		
		//Unset HOUSE as sales person. HOUSE cant make sales
		unset($page['sales_people'][2]);
		
		$page['sales_people'] = array_reverse($page['sales_people'], true);
		
		$this->load->model('cart');
		$carts = $this->cart->getRecords();
		$page['carts'] = array();
		$count = 0;
		
		$this->load->model('membership');
		$page['memberships'] = $this->membership->getRecords();
		
		foreach ($carts as $id => $data) {
			if (!isset($page['users'][$data['user_id']])) {
				
			} else {
				//Make sure the size isnt deleted , shouldnt ever happen
				if (isset($page['product_sizes'][$data['product_size_id']])) {
					//Make sure the parent isnt deleted
					if (isset($page['products'][$data['product_id']])) {
						
						if (!isset($page['carts'][$data['user_id']]['cart_string'])) {
							$page['carts'][$data['user_id']]['cart_string'] = '';
						}
						
						$page['carts'][$data['user_id']]['name'] = $page['users'][$data['user_id']]['first_name'] . ' ' . $page['users'][$data['user_id']]['last_name'];
						
						if (count($data) <= $count) {
							$page['carts'][$data['user_id']]['cart_string'] .= $page['products'][$data['product_id']]['name'] . ' (' . $page['product_sizes'][$data['product_size_id']]['name'] . ' x' . $data['quantity'] . ') - ';
						} else {
							$page['carts'][$data['user_id']]['cart_string'] .= $page['products'][$data['product_id']]['name'] . ' (' . $page['product_sizes'][$data['product_size_id']]['name'] . ' x' . $data['quantity'] . ') / ';
						}
						
						$page['carts'][$data['user_id']]['membership_id'] = $page['users'][$data['user_id']]['membership'];
						$page['carts'][$data['user_id']]['membership_paid'] = $page['users'][$data['user_id']]['membership_paid'];
						$page['carts'][$data['user_id']]['balance'] = $page['users'][$data['user_id']]['balance'];
						
						if ($page['carts'][$data['user_id']]['membership_id'] > 0 && $page['users'][$data['user_id']]['membership_paid'] == 0) {
							$page['carts'][$data['user_id']]['membership_due'] = $page['memberships'][$page['carts'][$data['user_id']]['membership_id']]['cost'];
						} else {
							$page['carts'][$data['user_id']]['membership_due'] = 0;
						}
						
						if (isset($page['carts'][$data['user_id']]['total'])) {
							$page['carts'][$data['user_id']]['total'] += $page['product_sizes'][$data['product_size_id']]['price'] * $data['quantity'];
						} else {
							$page['carts'][$data['user_id']]['total'] = $page['product_sizes'][$data['product_size_id']]['price'] * $data['quantity'];
						}
						
						if ($page['users'][$data['user_id']]['balance'] > $page['carts'][$data['user_id']]['total']) {
							//User has more money in account than sale, sub total is 0
							//Membership can be cash only, dont compare it to balance at any point
							$page['carts'][$data['user_id']]['total_adj'] = $page['carts'][$data['user_id']]['membership_due'];
							$page['carts'][$data['user_id']]['new_balance'] = $page['users'][$data['user_id']]['balance'] - $page['carts'][$data['user_id']]['total'];
						} else {
							//Not enough in balance to cover sale, THIS IS USERS WHO DONT HAVE A MEMBERSHIP AS WELL
							if ($page['carts'][$data['user_id']]['membership_due'] > 0) {
								//Membership is active but unpaid
								if ($page['memberships'][$page['carts'][$data['user_id']]['membership_id']]['worth'] > $page['carts'][$data['user_id']]['total'] - $page['users'][$data['user_id']]['balance']) {
									//Worth of unpaid membership is more than the total
									$page['carts'][$data['user_id']]['total_adj'] = $page['carts'][$data['user_id']]['membership_due'];
									$page['carts'][$data['user_id']]['new_balance'] = $page['memberships'][$page['carts'][$data['user_id']]['membership_id']]['worth'] - $page['carts'][$data['user_id']]['total'] + $page['users'][$data['user_id']]['balance'];
								} else {
									$page['carts'][$data['user_id']]['total_adj'] = $page['carts'][$data['user_id']]['total'] - $page['users'][$data['user_id']]['balance'] - $page['memberships'][$page['carts'][$data['user_id']]['membership_id']]['worth'];
									$page['carts'][$data['user_id']]['new_balance'] = 0;
								}
							} else {
								$page['carts'][$data['user_id']]['total_adj'] = $page['carts'][$data['user_id']]['total'] - $page['users'][$data['user_id']]['balance'];
								$page['carts'][$data['user_id']]['new_balance'] = 0;
							}
						}
						
						$count++;
						
						if (!isset($page['carts'][$data['user_id']]['cart_records'])) {
							$page['carts'][$data['user_id']]['cart_records'] = array();
						}
						
						$page['carts'][$data['user_id']]['cart_records'][$id] = $data;
						
						$page['carts'][$data['user_id']]['cart_records'][$id]['unit_cost'] = $page['product_sizes'][$data['product_size_id']]['price'];
						$page['carts'][$data['user_id']]['cart_records'][$id]['cost'] = $page['product_sizes'][$data['product_size_id']]['price'] * $data['quantity'];
						
						$page['carts'][$data['user_id']]['cart_records'][$id]['units_adj'] = $page['product_sizes'][$data['product_size_id']]['units'] * $data['quantity'];
						
						$product_record = $this->product->getRecord($data['product_id']);
						
						if ($product_record['inventory_holder_id'] > 0) {
							$holder_id = $product_record['inventory_holder_id'];
						} else {
							$holder_id = $company_info['primary_inventory_user_id'];
						}
						
						if (isset($page['product_inventory'][$data['product_id']][$holder_id])) {
							$page['carts'][$data['user_id']]['cart_records'][$id]['house_inv'] = $page['product_inventory'][$data['product_id']][$holder_id]['quantity'];
							$page['carts'][$data['user_id']]['cart_records'][$id]['unit_name'] = $page['product_inventory'][$data['product_id']][$holder_id]['unit_name'];
						} else {
							//This should never happen!!!!
							$page['carts'][$data['user_id']]['cart_records'][$id]['house_inv'] = 0;
							$page['carts'][$data['user_id']]['cart_records'][$id]['unit_name'] = 0;
						}
						
					}
				}
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function newSaleAction () {
		
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->database();
		$this->db->trans_start();
		
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $page_data['company_info']['site']);
		}
		
		$this->load->model('sale');
		$this->load->model('user');
		$this->load->model('cart');
		$this->load->model('product');
		$this->load->model('transaction');
		$this->load->model('inventory');
		
		$status = 1;
		
		//Record Transaction
		if ($status == 1) {
			$status = $this->transaction->writeData($_POST['cash_due'], $_SESSION['user_id']);
			
			$transaction_id = $this->transaction->getMostRecent();
		}
		
		foreach ($_POST['cart'] as $cart_id => $data) {
			
			if ($status == 1) {
				$sale_array = array(
								'sale_id' => 0,
								'transaction_id' => $transaction_id,
								'user_id' => $_POST['user_id'],
								'product_id' => $data['product']['id']
				);
				
				$status = $this->sale->writeData($sale_array);
				
				//Remove stock from inventory
				if ($status == 1) {
					$status = $this->inventory->removeInventory($data['parent']['id'], $data['product']['units_total']);
				}
			} else {
				break;
			}
		}
		
		//Remove balance from account
		if ($status == 1) {
			$status = $this->user->updateBalance($_POST['user_id'], $_POST['subtotal']);
		}
		
		//Notify drivers
		$drivers = $this->user->getDrivers();
		
		$customer = $this->user->getRecord($_POST['user_id']);
		
		foreach ($drivers as $id => $data) {
			if ($status == 1) {
				$this->load->library('email');
				$this->email->set_mailtype("html");
				
				$this->email->from('info@wwweed.org');
				
				if ($page_data['company_info']['dev_mode'] == 1) {
					$this->email->to('apogeewebdesign@gmail.com');
				} else {
					$this->email->to($data['email']);
				}
				
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
				
				$this->email->subject('Order Available For Delivery From World Wide Weed!');
				
				//Piece together string for telling what was ordered
				$products_string = '';
				$count = 1;
				
				foreach ($_POST['cart'] as $cart_id => $data2) {
					$child = $this->product->getRecord($data2['product']['id']);
					$parent = $this->product->getRecord($data2['parent']['id']);
					
					if ($count == count($_POST['cart'])) {
						$products_string .= $child['name'] . ' ' . $parent['name'];
					} else {
						$products_string .= $child['name'] . ' ' . $parent['name'] . ',  ';
					}
					
					$count++;
				}
				
				$data = array(
								'name' => $data['first_name'],
								'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
								'delivery_address' => $customer['address'] . ',' . $customer['city'] . ',' . $customer['zip'],
								'site' => $page_data['company_info']['site'],
								'products' => $products_string
				);
				
				$body = $this->load->view('email/delivery-available.phtml', $data, TRUE);
				$this->email->message($body);
				
				$this->email->send();
				
				$status = $this->email->print_debugger();
				
				if (trim(strip_tags($status)) == 0) {
					$status = 1;
				}
				
				$this->email->clear(true);
			}
		}
		
		//Empty cart
		if ($status == 1) {
			$status = $this->cart->emptyCart($_POST['user_id']);
		}
		
		//Empty temp cart
		if ($status == 1) {
			$_SESSION['temp_cart'] = array();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$status = 'DB transaction error';
		}
		
		//Send email to drivers
		
		print $status;
	}
	
	public function page()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'page';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		$this->load->model('page');
		$page['piled_pages'] = $this->page->getPageNames();
		
		$page['colors'] = array(
						'#55CE63',
						'#7460EE',
						'#F62D51',
						'#01C0C8',
						'#f7a151'
		);
		
		$page['pages'] = array();
		
		foreach ($page['piled_pages'] as $id => $data) {
			$dash_position = strpos($data, '/');
			
			if (!isset($page['pages'][substr($data, 0, $dash_position)])) {
				$page['pages'][substr($data, 0, $dash_position)] = array();
			}
			
			$page['pages'][substr($data, 0, $dash_position)][$id] = $data;
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editProgram()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data['page'] = 'admin/edit-program';
		
		$data['program_id'] = $this->input->get('program_id');
		
		$this->load->model('programtype');
		$data['program_types'] = $this->programtype->getProgramTypes();
		
		$this->load->model('program');
		$data['program'] = $this->program->getProgramData($data['program_id']);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewProgram()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data['page'] = 'admin/view-program';
		
		$this->load->model('program');
		$data['programs'] = $this->program->getPrograms();
		
		foreach ($data['programs'] as $id => $record) {
			$data['programs'][$id]['description'] = substr(strip_tags($record['description']), 0, 140) . '...';
		}
		
		$this->load->model('programtype');
		$data['program_types'] = $this->programtype->getProgramTypes(true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function createTag()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/create-tag';
		$page['tag_id'] = $this->input->get('tag_id', 0);
		
		$this->load->model('tag');
		$page['tags'] = $this->tag->getTags();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editTag()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/edit-tag';
		
		$page['tag_id'] = $this->input->get('tag_id');
		
		$this->load->model('tag');
		$page['tags'] = $this->tag->getTags();
		$page['tag_name'] = $this->tag->getTagName($page['tag_id']);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewTag()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-tag';
		
		$this->load->model('tag');
		$page['tags'] = $this->tag->getTags();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editPage()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-page';
		
		$this->load->model('page');
		$page['section'] = $this->page->getRecord($this->input->get('page_id'));
		$page['pages'] = $this->page->getPageNames(true);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewPage()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'view-page';
		
		$this->load->model('page');
		$page['page_content'] = $this->page->getPageContent($this->input->get('page'));
		$page['page_name'] = $this->input->get('page');
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writePage()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'page_id' => $this->input->post('page_id'),
						'section_header' => $this->input->post('section_header'),
						'attachment_file_name' => $this->input->post('attachment'),
						'page_link' => $this->input->post('page_link'),
						'section_text' => $this->input->post('section_text')
		);
		
		$this->load->model('page');
		$status = $this->page->writeData($data);
		
		print $status;
	}
	
	public function writeContent()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		if ($this->input->post('content_id') > 0) {
			$data = array(
							'content_id' => $this->input->post('content_id'),
							'title' => $this->input->post('title', ''),
							'attachment_file_name' => $this->input->post('attachment', ''),
							'target_date' => $this->input->post('target_date', ''),
							'content_type_id' => $this->input->post('content_type_id', '0'),
							'content_text' => $this->input->post('content_text', ''),
			);
		} else {
			$data = array(
							'title' => $this->input->post('title'),
							'attachment_file_name' => $this->input->post('attachment'),
							'target_date' => $this->input->post('target_date'),
							'thumb_file_name' => '',
							'content_type_id' => $this->input->post('content_type_id'),
							'content_text' => $this->input->post('content_text'),
			);
		}
		
		$this->load->model('content');
		$status = $this->content->writeData($data);
		
		print $status;
	}
	
	public function createUser()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'create-user';
		
		$page['user_type_id'] = $this->input->get('user_type_id');
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function createDriver()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'create-driver';
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function createAdmin()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'create-driver';
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writeTag()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('tag');
		$tags = $this->tag->getTags();
		
		//Check to see if tag already exists
		foreach ($tags as $id => $data) {
			if ($this->input->get('name') == $data['name']) {
				print 'Tag already exists';
				exit;
			}
		}
		
		if ($this->input->get('tag_id') > 0) {
			$data = array(
							'tag_id' => $this->input->get('tag_id'),
							'name' => $this->input->get('name')
			);
		} else {
			$data = array(
							'name' => $this->input->get('name')
			);
		}
		
		$status = $this->tag->writeData($data);
		
		print $status;
	}
	
	public function editSaleAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('transaction');
		
		$date = str_replace('/', '-', substr($this->input->post('date'), 6)) . '-' . str_replace('/', '-', substr($this->input->post('date'), 0, 5));
		$date = $date . ' ' . $this->input->post('time') . ':00';
		
		$data = array(
						'transaction_id' => $this->input->post('transaction_id'),
						'time' => $date,
						'notes' => $this->input->post('notes'),
						'alt_cost' => $this->input->post('alt_cost')
		);
		
		$status = $this->transaction->updateData($data);
		
		print $status;
	}
	
	public function writeUser()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$user_id = $this->input->post('id');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$login = $this->input->post('login');
		$password = $this->input->post('password');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$expiration = $this->input->post('expiration');
		$referral_id = $this->input->post('referral_id');
		$referral_name = $this->input->post('referral_name');
		$membership_level = $this->input->post('membership_level');
		$balance = $this->input->post('balance');
		$id_file = $this->input->post('id_file');
		$notes = $this->input->post('notes');
		$profile = $this->input->post('profile');
		$rec_file = $this->input->post('rec_file');
		$zip = $this->input->post('zip');
		
		$user_type_id = $this->input->post('user_type_id');
		
		$source = $this->input->post('source');
		
		$this->load->model('admin');
		$status = $this->admin->writeUser($user_id, $first_name, $last_name, $email, $phone, $login, $user_type_id, $address, $city, $zip, $id_file, $rec_file, $membership_level, $balance, $referral_id, $referral_name, $notes, $state, $expiration, $profile);
		
		if ($status == 1) {
			$status = $this->admin->writePassword($password);
		}
		
		$this->load->model('user');
		$this->load->model('inventory');
		
		//If this is a new user create an inventory record for each product
		//5 => Employee, 1 => Admin
		if ($user_type_id == 5 || $user_type_id == 1) {
			
			if ($user_id == 0) {
				$this->load->model('product');
				$products = $this->product->getRecords();
				
				$new_user_id = $this->user->getRecentId();
				
				foreach ($products as $product_id => $data) {
					$status = $this->inventory->writeData(0, 0, $product_id, $new_user_id);
				}
			}
		}
		
		if ($source == 'quick') {
			$status = $this->user->getRecentId();
		}
		
		print $status;
	}
	
	public function writeUserPassword()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$id = $this->input->post('id');
		$password = $this->input->post('password');
		
		$this->load->model('admin');
		$status = $this->admin->writePassword($password, $id);
		
		print $status;
	}
	
	public function activateUser()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$user_id = $this->input->post('user_id');
		
		$this->load->model('user');
		$status = $this->user->activate($user_id);
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		if ($status == 1) {
			$this->load->model('admin');
			$key = $this->admin->randomString();
			
			$this->load->model('maillink');
			$status = $this->maillink->writeData($user_id, $key);
			
			if ($status == 1) {
				$user = $this->user->getRecord($user_id);
				
				$this->load->library('email');
				
				$this->email->set_mailtype("html");
				
				$this->email->from('info@wwweed.org');
				
				$this->email->to(trim($user['email']));
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
				
				$this->email->subject('Wwweed.org Application Approved!');
				
				$data = array(
								'user' => $user,
								'key' => $key,
								'site' => $company_info['site']
				);
				
				$body = $this->load->view('email/application-approved.phtml', $data, TRUE);
				$this->email->message($body);
				
				$this->email->send();
				
				$status = $this->email->print_debugger();
				
				if (trim(strip_tags($status)) == 0) {
					$status = 1;
				}
				
				print strip_tags($status);
				exit;
			}
		}
		
		print $status;
	}
	
	public function writeCompanyInfo()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		
		$this->load->model('admin');
		$status = $this->admin->writeCompanyInfo($name, $email, $phone, $address);
		
		print $status;
	}
	
	public function content()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'content';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function sale()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'sale';
		
		$this->load->model('sale');
		$page['sales'] = $this->sale->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writeInventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$inventory_id = $this->input->post('inventory_id');
		$quantity = $this->input->post('quantity');
		$product_id = $this->input->post('product_id');
		
		$this->load->model('inventory');
		$status = $this->inventory->writeData($inventory_id, $quantity, $product_id);
		
		print $status;
	}
	
	public function writeUserInventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$updates = array();
		
		foreach ($_POST['products'] as $i => $data) {
			$updates[$data['inventory_id']] = $data['quantity'];
		}
		
		$this->load->model('inventory');
		
		foreach ($updates as $inventory_id => $data) {
			$status = $this->inventory->writeData($inventory_id, $data);
		}
		
		print $status;
	}
	
	public function writeNewHolder()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		
		$user_id = $this->input->post('user_id');
		$quantity = $this->input->post('quantity');
		$product_id = $this->input->post('product_id');
		$house = $this->input->post('house');
		
		$quantity = round($quantity, 1);
		
		$status = 1;
		
		if ($house == 1) {
			$new_house = 0;
			$record = $this->inventory->getHouseProduct($product_id);
			
			$house_quantity = $record['quantity'];
			$house_id = $record['id'];
			
			if ($quantity > $house_quantity) {
				$status = 'Not enough house inventory to complete that action.';
			} else {
				$new_house = $house_quantity - $quantity;
			}
		}
		
		if ($status == 1) {
			if ($house == 1) {
				$status = $this->inventory->writeNewHolderFromHouse($user_id, $quantity, $product_id, $new_house, $house_id);
			} else {
				$status = $this->inventory->writeNewHolder($user_id, $quantity, $product_id);
			}
		}
		
		print $status;
	}
	
	public function updateInventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$user_id = $this->input->post('user_id');
		$quantity = round($this->input->post('quantity'));
		$product_id = $this->input->post('product_id');
		
		$this->load->model('inventory');
		
		$match = $this->inventory->getMatch($user_id, $product_id);
		
		$status = $this->inventory->writeData($match['id'], $quantity, $product_id, $user_id);
		
		print $status;
	}
	
	public function takeHouse()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$user_id = $this->input->post('user_id');
		$quantity = round($this->input->post('quantity'), 1);
		$product_id = $this->input->post('product_id');
		
		$quantity = round($quantity, 1);
		
		$this->load->model('inventory');
		$this->load->model('product');
		$product = $this->product->getRecord($product_id);
		
		if ($product['inventory_holder_id'] > 0) {
			$holder_user_id = $product['inventory_holder_id'];
		} else {
			$holder_user_id = $company['primary_inventory_user_id'];
		}
		
		$house = $this->inventory->getHouseRecord($product_id);
		
		$status = $this->inventory->writeData($house['id'], 0, $product_id, $holder_user_id);
		
		if ($status == 1) {
			$record = $this->inventory->findMatch($user_id, $product_id);
			
			$amount = $record['quantity'] + $house['quantity'];
			
			$status = $this->inventory->writeData($record['id'], $amount, $product_id, $user_id);
		}
		
		print $status;
	}
	
	public function editInventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		$product = $this->product->getRecord($this->input->get('product_id'));
		
		if ($product['inventory_holder_id'] > 0) {
			$user_id = $product['inventory_holder_id'];
		} else {
			$user_id = $company['primary_inventory_user_id'];
		}
		
		$page['set_holder_id'] = $user_id;
		
		$page['page'] = 'edit-inventory';
		
		$this->load->model('inventory');
		$page['house'] = $this->inventory->getHouseRecord($this->input->get('product_id'));
		
		if (count($page['house']) == 0) {
			$status = $this->inventory->writeData(0, 0, $this->input->get('product_id'), $user_id);
			
			if ($status == 1) {
				$page['house'] = $this->inventory->getHouseRecord($this->input->get('product_id'));
			}
		}
		
		$page['inventory'] = $this->inventory->getRecordsByProductId($this->input->get('product_id'), true);
		
		$page['product_id'] = $this->input->get('product_id');
		
		$users = array();
		
		foreach($page['inventory'] as $id => $data) {
			$users[$data['user_id']] = $data['user_id'];
		}
		
		$this->load->model('user');
		$page['users'] = $this->user->getUsers();
		$page['available_holders'] = $this->user->getAvailableHolders(true);
		
		$page['user_list'] = array();
		$page['user_list'][0]['user_type_id'] = 1;
		$page['user_list'][0]['first_name'] = '- Select User -';
		$page['user_list'][0]['last_name'] = '';
		
		foreach ($page['users'] as $id => $data) {
			if (!in_array($id, $users)) {
				$page['user_list'][$id] = $data;
			}
		}
		
		$page['product'] = $this->product->getRecord($this->input->get('product_id'));
		
		$page['total'] = 0;
		
		foreach ($page['inventory'] as $id => $data) {
			$page['total'] += $data['quantity'];
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function inventory()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'inventory';
		
		$page['stock_filter'] = $this->input->get('stock_filter');
		
		$page['set_product_type_id'] = $this->input->get('product_type');
		
		if ($page['set_product_type_id'] == '') {
			$page['set_product_type_id'] = 0;
		}
		
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('producttype');
		$this->load->model('user');
		$this->load->model('companyinfo');
		$this->load->model('inventory');
		$this->load->model('vendor');
		
		$company = $this->companyinfo->getRecord();
		
		$page['products'] = $this->product->getRecordsAdmin(true);
		$page['product_sizes'] = $this->productsize->getRecords(true);
		$page['product_types'] = $this->producttype->getRecords(true);
		$page['users'] = $this->user->getRecords();
		$page['holders'] = $this->user->getAvailableHolders(true);
		$page['vendors'] = $this->vendor->getRecords();
		
		$inventory_quantities = array();
		
		if ($page['set_product_type_id'] > 0) {
			$inventory = $this->inventory->getRecords();
			
			foreach ($inventory as $id => $data) {
				$product = $this->product->getRecord($data['product_id']);
				
				
				//Unset filtered out records
				if ($product['product_type_id'] != $page['set_product_type_id']) {
					unset($inventory[$id]);
				} else {
					if (!isset($inventory_quantities[$data['product_id']])) {
						$inventory_quantities[$data['product_id']] = 0;
					}
					
					$inventory_quantities[$data['product_id']] += $data['quantity'];
				}
				
				
			}
		} else {
			$inventory = $this->inventory->getRecords();
			
			foreach ($inventory as $id => $data) {
				if (!isset($inventory_quantities[$data['product_id']])) {
					$inventory_quantities[$data['product_id']] = 0;
				}
				
				$inventory_quantities[$data['product_id']] += $data['quantity'];
			}
		}
		
		$page['inventory'] = array();
		
		foreach ($inventory as $id => $data) {
			
			if ($page['stock_filter'] == '') {
				//Show All Records
				if (!isset($page['inventory'][$data['product_id']])) {
					$page['inventory'][$data['product_id']]['quantity'] = $inventory_quantities[$data['product_id']];
				}
			} else {
				//Show only products with positive inventory
				if ($inventory_quantities[$data['product_id']] > 0) {
					
					if (!isset($page['inventory'][$data['product_id']])) {
						$page['inventory'][$data['product_id']]['quantity'] = $inventory_quantities[$data['product_id']];
					}
				}
			}
		}
		
		$page['retail_total'] = 0;
		$page['wholesale_total'] = 0;
		
		foreach ($page['inventory'] as $product_id => $data) {
			
			$product = $this->product->getRecord($product_id);
			
			if ($product == '' || count($product) == 0) {
				unset($page['inventory'][$product_id]);
			} else {
				$page['inventory'][$product_id]['wholesale_unit'] = $product['unit_price_wholesale'];
				
				$product_sizes = $this->productsize->getRecordsByProductId($product_id);
				$averages = array();
				$average_total = 0;
				
				foreach ($product_sizes as $id => $data) {
					if ($data['units'] == 0) {
						$val =  0;
					} else {
						$val =  round($data['price'] / $data['units'], 2);
					}
					
					
					$averages[] = $val;
					$average_total += $val;
				}
				
				if (count($averages) == 0) {
					$page['inventory'][$product_id]['average_retail'] = 0;
				} else {
					$page['inventory'][$product_id]['average_retail'] = round($average_total / count($averages), 2);
				}
			}
		}
		
		foreach ($page['inventory'] as $id => $data) {
			$page['wholesale_total'] += $data['wholesale_unit'] * $data['quantity'];
			$page['retail_total'] += $data['average_retail'] * $data['quantity'];
		}
		
		$page['wholesale_total'] = number_format($page['wholesale_total'], 2, '.', ',');
		$page['retail_total'] = number_format($page['retail_total'], 2, '.', ',');
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function removeOldHolderAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		
		if ($_POST['source'] == 1) {
			$to_be_deleted = $this->inventory->findMatch($_POST['user_id'], $_POST['product_id']);
			$house = $this->inventory->getHouseRecord($to_be_deleted['product_id']);
			
			$house_quantity = $house['quantity'] + $_POST['quantity'];
			
			//Write new HOUSE record
			$status = $this->inventory->writeData($house['id'], $house_quantity);
			
			//Delete Old
			if ($status == 1) {
				//$status = $this->inventory->delete($to_be_deleted['id']);
				$status = $this->inventory->writeData($to_be_deleted['id'], 0);
			}
		} else {
			//$status = $this->inventory->delete($_POST['inventory_id']);
		}
		
		print $status;
		exit;
	}
	
	public function reviews()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'reviews';
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getRecords();
		
		$this->load->model('review');
		$page['reviews'] = $this->review->getRecordsAlt();
		
		$page['inventory'] = array();
		
		foreach ($inventory as $id => $data) {
			if (!isset($page['inventory'][$data['product_id']])) {
				$page['inventory'][$data['product_id']] = $data;
			} else {
				$page['inventory'][$data['product_id']]['quantity'] += $data['quantity'];
			}
		}
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewReviews()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'view-reviews';
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getRecords();
		
		$this->load->model('user');
		$page['users'] = $this->user->getUsers();
		
		$this->load->model('review');
		$page['reviews'] = $this->review->getRecordsByProductId($this->input->get('product_id'));
		
		$page['inventory'] = array();
		
		foreach ($inventory as $id => $data) {
			if (!isset($page['inventory'][$data['product_id']])) {
				$page['inventory'][$data['product_id']] = $data;
			} else {
				$page['inventory'][$data['product_id']]['quantity'] += $data['quantity'];
			}
		}
		
		$this->load->model('product');
		$page['product'] = $this->product->getRecord($this->input->get('product_id'));
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function deleteReview()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('review');
		$status = $this->review->delete($this->input->post('review_id'));
		
		print $status;
	}
	
	public function sales()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$this->load->model('pagecount');
		$this->load->model('transaction');
		$this->load->model('productold');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('sale');
		
		$page['error'] = $this->input->get('error');
		$filter = $this->input->get('filter');
		$dates = explode(' - ', $this->input->get('filter'));
		
		$page['visits'] = $this->pagecount->getCount(1);
		
		$page['users'] = $this->user->getRecordsAdmin();
		
		if ($filter == 2) {
			$page['filter'] = 2;
			
			$sales = $this->sale->getRecordsByTime(168);
			$page['transactions'] = $this->transaction->getRecordsByTime(168);
		} elseif ($filter == 3) {
			$page['filter'] = 3;
			
			$sales = $this->sale->getRecordsByTime(720);
			$page['transactions'] = $this->transaction->getRecordsByTime(720);
		} elseif ($filter == 4) {
			$page['filter'] = 4;
			
			$sales = $this->sale->getRecordsByTime(8760);
			$page['transactions'] = $this->transaction->getRecordsByTime(8760);
		} elseif ($filter == 5) {
			$page['filter'] = 5;
			
			$sales = $this->sale->getRecords();
			$page['transactions'] = $this->transaction->getRecords();
		} elseif ($filter == '') {
			$page['filter'] = 1;
			
			$sales = $this->sale->getRecordsByTime(48);
			$page['transactions'] = $this->transaction->getRecordsByTime(48);
		} else {
			$page['filter'] = $filter;
			
			$sales = $this->sale->getRecordsByDateRange($dates[0], $dates[1]);
			$page['transactions'] = $this->transaction->getRecordsByDateRange($dates[0], $dates[1]);
		}
		
		$sale_total = 0;
		$paid_total = 0;
		
		//Remove memberships, redundant
		unset($page['sales'][0]);
		
		$page['pending_sales'] = array();
		
		foreach ($page['transactions'] as $transaction_id => $data) {
			
			if ($data['pending'] == 1) {
				$page['pending_sales'][$transaction_id] = $data;
				unset($page['transactions'][$transaction_id]);
				
				foreach ($sales as $sale_id => $data2) {
					if ($data2['transaction_id'] == $transaction_id) {
						unset($sales[$sale_id]);
					}
				}
				
			} else {
				$sale_total += $data['sub_total'];
				
				if ($data['alt_cost'] == 0) {
					$paid_total += $data['cost'];
				} else {
					$paid_total += $data['alt_cost'];
				}
				
				$page['transactions'][$transaction_id]['products'] = $this->product->getProductSizesByTransactionId($transaction_id);
				
				foreach ($page['transactions'][$transaction_id]['products'] as $product_size_id => $data2) {
					$page['transactions'][$transaction_id]['products'][$product_size_id]['product_size'] = $this->productsize->getRecord($product_size_id);
				}
			}
		}
		
		$page['sale_total'] = $sale_total;
		$page['paid_total'] = $paid_total;
		$page['high_sale_total'] = 0;
		$page['high_paid_total'] = 0;
		
		$page['averages'] = array();
		$page['graph_pending_sales'] = array();
		
		//Data for graph
		for ($i = 0; $i < 30; $i++) {
			//$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
			$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			$day_sales = $this->transaction->getRecordsByDateRange($last, $last);
			
			foreach ($day_sales as $transaction_id => $data) {
				if ($data['pending'] == 1) {
					$page['graph_pending_sales'][$transaction_id] = $data;
					unset($day_sales[$transaction_id]);
				}
			}
			
			$day_transactions = array();
			$day_subtotal = 0;
			$day_paid = 0;
			
			foreach ($day_sales as $id => $data) {
				$day_subtotal += $data['sub_total'];
				
				if ($data['alt_cost'] == 0) {
					$day_paid += $data['cost'];
				} else {
					$day_paid += $data['alt_cost'];
				}
			}
			
			if (count($day_sales) > 0) {
				$avg_sub = $day_subtotal / count($day_sales);
				$avg_paid = $day_paid / count($day_sales);
			} else {
				$avg_sub = 0;
				$avg_paid = 0;
			}
			
			$page['averages'][$i] =	array(
							'last' =>  $last,
							'month' => $date[1],
							'day' => $date[2],
							'sub' => $day_subtotal,
							'avg_sub' => $avg_sub,
							'paid' => $day_paid,
							'avg_paid' => $avg_paid
			);
			
			if ($day_subtotal > $page['high_sale_total']) {
				$page['high_sale_total'] = $day_subtotal;
			}
			
			if ($day_paid > $page['high_paid_total']) {
				$page['high_paid_total'] = $day_paid;
			}
		}
		
		//Dates are descending, flip array to show in chronological order
		$page['averages'] = array_reverse($page['averages']);
		
		$page['product_sales'] = $this->product->getProductSales();
		
		$page['all_products'] = $this->product->getRecordsAdmin();
		$page['all_sizes'] = $this->productsize->getRecords();
		
		$page['old_products'] = $this->productold->getRecords();
		
		$page['transaction_count'] = count($page['transactions']);
		$page['sale_count'] = count($sales);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function pendingSales()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('transaction');
		$page['transactions'] = $this->transaction->getPending();
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecordsAdmin();
		
		$this->load->model('productsize');
		$page['product_sizes'] = $this->productsize->getRecords();
		
		$this->load->model('user');
		$page['users'] = $this->user->getRecords();
		
		foreach ($page['transactions'] as $transaction_id => $data) {
			
			$page['transactions'][$transaction_id]['products'] = $this->product->getProductSizesByTransactionId($transaction_id);
			
			foreach ($page['transactions'][$transaction_id]['products'] as $product_size_id => $data2) {
				$page['transactions'][$transaction_id]['products'][$product_size_id]['product_size'] = $this->productsize->getRecord($product_size_id);
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function profitsAndLosses()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$this->load->model('pagecount');
		$this->load->model('transaction');
		$this->load->model('productold');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('sale');
		$this->load->model('creditmerchant');
		$this->load->model('expense');
		
		$page['merchants'] = $this->creditmerchant->getRecords();
		
		$page['error'] = $this->input->get('error');
		$filter = $this->input->get('filter');
		$dates = explode(' - ', $this->input->get('filter'));
		
		$page['visits'] = $this->pagecount->getCount(1);
		
		$page['users'] = $this->user->getRecordsAdmin();
		
		if ($filter == 2) {
			$page['filter'] = 2;
			
			$sales = $this->sale->getRecordsByTime(168);
			$page['transactions'] = $this->transaction->getRecordsByTime(168);
			$page['expenses'] = $this->expense->getRecordsByTime(168);
		} elseif ($filter == 3) {
			$page['filter'] = 3;
			
			$sales = $this->sale->getRecordsByTime(720);
			$page['transactions'] = $this->transaction->getRecordsByTime(720);
			$page['expenses'] = $this->expense->getRecordsByTime(720);
		} elseif ($filter == 4) {
			$page['filter'] = 4;
			
			$sales = $this->sale->getRecordsByTime(8760);
			$page['transactions'] = $this->transaction->getRecordsByTime(8760);
			$page['expenses'] = $this->expense->getRecordsByTime(8760);
		} elseif ($filter == 5) {
			$page['filter'] = 5;
			
			$sales = $this->sale->getRecords();
			$page['transactions'] = $this->transaction->getRecords();
			$page['expenses'] = $this->expense->getRecords();
		} elseif ($filter == '') {
			$page['filter'] = 1;
			
			$sales = $this->sale->getRecordsByTime(48);
			$page['transactions'] = $this->transaction->getRecordsByTime(48);
			$page['expenses'] = $this->expense->getRecordsByTime(48);
		} else {
			$page['filter'] = $filter;
			
			$sales = $this->sale->getRecordsByDateRange($dates[0], $dates[1]);
			$page['transactions'] = $this->transaction->getRecordsByDateRange($dates[0], $dates[1]);
			$page['expenses'] = $this->expense->getRecordsByDateRange($dates[0], $dates[1]);
		}
		
		
		$sale_total = 0;
		$paid_total = 0;
		$cash_total = 0;
		$card_total = 0;
		$expenses_total = 0;
		
		foreach ($page['expenses'] as $expense_id => $data) {
			$expenses_total += $data['amount'];
		}
		
		$page['transaction_count'] = count($page['transactions']);
		$page['sale_count'] = count($sales);
		
		//Remove memberships, redundant
		unset($page['sales'][0]);
		
		foreach ($page['transactions'] as $transaction_id => $data) {
			$sale_total += $data['sub_total'];
			
			if ($data['alt_cost'] == 0) {
				$paid_total += $data['cost'];
			} else {
				$paid_total += $data['alt_cost'];
			}
			
			$page['transactions'][$transaction_id]['products'] = $this->product->getProductSizesByTransactionId($transaction_id);
			
			foreach ($page['transactions'][$transaction_id]['products'] as $product_size_id => $data2) {
				$page['transactions'][$transaction_id]['products'][$product_size_id]['product_size'] = $this->productsize->getRecord($product_size_id);
			}
			
			if ($data['payment_type'] == 'card') {
				if ($data['alt_cost'] > 0) {
					$card_total = $data['alt_cost'];
				} else {
					$card_total = $data['cost'];
				}
				
				//Add card total to applicable merchant
				$page['merchants'][$data['credit_merchant_id']]['total_sales'] += $card_total;
			} else {
				if ($data['alt_cost'] > 0) {
					$cash_total += $data['alt_cost'];
				} else {
					$cash_total += $data['cost'];
				}
				
			}
		}
		
		$page['expenses_total'] = $expenses_total;
		
		$page['sale_total'] = $sale_total;
		$page['paid_total'] = $paid_total;
		
		$page['card_total'] = $card_total;
		$page['cash_total'] = $cash_total;
		
		$page['high_sale_total'] = 0;
		$page['high_paid_total'] = 0;
		
		$page['averages'] = array();
		
		//Data for graph
		for ($i = 0; $i < 30; $i++) {
			//$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
			$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			$day_sales = $this->transaction->getRecordsByDateRange($last, $last);
			
			$day_transactions = array();
			$day_subtotal = 0;
			$day_paid = 0;
			
			foreach ($day_sales as $id => $data) {
				$day_subtotal += $data['sub_total'];
				
				if ($data['alt_cost'] == 0) {
					$day_paid += $data['cost'];
				} else {
					$day_paid += $data['alt_cost'];
				}
			}
			
			if (count($day_sales) > 0) {
				$avg_sub = $day_subtotal / count($day_sales);
				$avg_paid = $day_paid / count($day_sales);
			} else {
				$avg_sub = 0;
				$avg_paid = 0;
			}
			
			$page['averages'][$i] =	array(
							'last' =>  $last,
							'month' => $date[1],
							'day' => $date[2],
							'sub' => $day_subtotal,
							'avg_sub' => $avg_sub,
							'paid' => $day_paid,
							'avg_paid' => $avg_paid
			);
			
			if ($day_subtotal > $page['high_sale_total']) {
				$page['high_sale_total'] = $day_subtotal;
			}
			
			if ($day_paid > $page['high_paid_total']) {
				$page['high_paid_total'] = $day_paid;
			}
		}
		
		//Dates are descending, flip array to show in chronological order
		$page['averages'] = array_reverse($page['averages']);
		
		$page['product_sales'] = $this->product->getProductSales();
		
		$page['all_products'] = $this->product->getRecordsAdmin();
		$page['all_sizes'] = $this->productsize->getRecords();
		
		$page['old_products'] = $this->productold->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function salesReport()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$dates = explode(' - ', $this->input->get('filter'));
		
		$page['page'] = 'sales-report';
		
		$this->load->model('user');
		$page['users'] = $this->user->getRecordsAdmin();
		
		$this->load->model('transaction');
		$page['transactions'] = $this->transaction->getRecords();
		
		$this->load->model('productold');
		$page['product_archive'] = $this->productold->getRecordsAdmin();
		
		$this->load->model('productsize');
		$page['product_sizes'] = $this->productsize->getRecords();
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecordsAdmin();
		
		$this->load->model('sale');
		
		$filter = $this->input->get('filter');
		
		if ($filter == 2) {
			$page['filter'] = 2;
			
			$sales = $this->sale->getRecordsByTime(168);
		} elseif ($filter == 3) {
			$page['filter'] = 3;
			
			$sales = $this->sale->getRecordsByTime(720);
		} elseif ($filter == 4) {
			$page['filter'] = 4;
			
			$sales = $this->sale->getRecordsByTime(8760);
		} elseif ($filter == '') {
			$page['filter'] = 1;
			
			$sales = $this->sale->getRecordsByTime(48);
		} else {
			//Result is a string, meaning its a date
			$page['filter'] = $filter;
			$sales = $this->sale->getRecordsByDateRange($dates[0], $dates[1]);
		}
		
		$page['sales'] = array();
		
		foreach ($sales as $id => $data) {
			$page['sales'][$id] = $data;
		}
		
		$page['sales_array'] = array();
		$page['sales_total'] = 0;
		$transactions = array();
		
		foreach ($page['sales'] as $id => $data) {
			if (!array_key_exists ($data['user_id'], $page['users'])) {
				$page['sales'][$id]['user_deleted'] = true;
			} else {
				$page['sales'][$id]['user_deleted'] = false;
			}
			
			$user = $this->user->getRecordAdmin($data['user_id']);
			$product = $this->product->getRecordAdmin($data['product_id']);
			$product_size = $this->productsize->getRecordAdmin($data['product_size_id']);
			
			//OLD DB
			if ($data['transaction_id'] < 390) {
				$product = $this->productold->getRecordAdmin($data['product_id']);
				
				if ($product['parent_id'] != '' && count($user) > 0) {
					
					$page['sales_array'][] = array(
									$user['first_name'],
									$user['last_name'],
									$user['email'],
									$page['product_archive'][$product['parent_id']]['name'] . "-" . $product['name'],
									$product['price'],
									$data['created_date']
					);
				}
				
				$price = $product['price'];
				
			} else {
				$page['sales_array'][] = array($user['first_name'], $user['last_name'], $user['email'], $product['name'] . "-" . $product_size['name'], $product_size['price'], $data['created_date']);
				
				$price = $product_size['price'];
			}
			
			$page['sales_total'] += $price;
			
			$transactions[$data['transaction_id']] = $data['transaction_id'];
		}
		
		$page['all_sales'] = $sales;
		
		$page['transaction_count'] = count($transactions);
		
		//Remove memberships, redundant
		unset($page['sales'][0]);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function salesReportDownloadAction() {
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$page['users'] = $this->user->getRecordsAdmin();
		
		$this->load->model('transaction');
		$page['transactions'] = $this->transaction->getRecords();
		
		$this->load->model('productold');
		$page['product_archive'] = $this->productold->getRecordsAdmin();
		
		$this->load->model('productsize');
		$page['product_sizes'] = $this->productsize->getRecords();
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecordsAdmin();
		
		$this->load->model('sale');
		
		$filter = $this->input->get('filter');
		
		if ($filter == 2) {
			$page['filter'] = 2;
			
			$sales = $this->sale->getRecordsByTime(168);
		} elseif ($filter == 3) {
			$page['filter'] = 3;
			
			$sales = $this->sale->getRecordsByTime(720);
		} elseif ($filter == 4) {
			$page['filter'] = 4;
			
			$sales = $this->sale->getRecordsByTime(8760);
		} elseif ($filter == '' || $filter == 1) {
			$page['filter'] = 1;
			
			$sales = $this->sale->getRecordsByTime(48);
		} else {
			$page['filter'] = $filter;
			$sales = $this->sale->getRecordsByDate($page['filter']);
		}
		
		foreach ($sales as $id => $data) {
			$page['sales'][$id] = $data;
		}
		
		$page['sales_array'] = array();
		$page['sales_array'][] = array('FIRST NAME', 'LAST NAME', 'EMAIL', 'PRODUCT NAME/SIZE', 'PRICE', 'QUANTITY', 'UNIT', 'SALE DATE');
		
		foreach ($page['sales'] as $id => $data) {
			if (!array_key_exists ($data['user_id'], $page['users'])) {
				$page['sales'][$id]['user_deleted'] = true;
			} else {
				$page['sales'][$id]['user_deleted'] = false;
			}
			
			$user = $this->user->getRecord($data['user_id']);
			$product = $this->product->getRecord($data['product_id']);
			$product_size = $this->productsize->getRecord($data['product_size_id']);
			
			$page['sales_array'][] = array(
							$user['first_name'],
							$user['last_name'],
							$user['email'],
							$product['name'] . "-" . $product_size['name'],
							$product_size['price'],
							$product_size['units'],
							$product['unit_name'],
							$data['created_date']
			);
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		
		function outputCSV($data) {
			$output = fopen("php://output", "w");
			foreach ($data as $row)
				fputcsv($output, $row); // here you can change delimiter/enclosure
				fclose($output);
		}
		
		outputCSV($page['sales_array']);
	}
	
	public function customerDownloadAction() {
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$page['users'] = $this->user->getCustomers();
		
		$page['users_array'] = array();
		$page['users_array'][] = array('FIRST NAME', 'LAST NAME', 'ADDRESS', 'EMAIL', 'PHONE', 'MEMBER');
		
		$members = array(
						0 => 'NO',
						1 => 'GREEN',
						2 => 'GOLD',
						3 => 'BLACK'
		);
		
		foreach ($page['users'] as $id => $data) {
			$page['users_array'][] = array(
							$data['first_name'],
							$data['last_name'],
							$data['address'],
							$data['email'],
							$data['phone'],
							$members[$data['membership']]
			);
		}
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=cumstomers" . time() . ".csv");
		
		function outputCSV($data) {
			$output = fopen("php://output", "w");
			foreach ($data as $row)
				fputcsv($output, $row); // here you can change delimiter/enclosure
				fclose($output);
		}
		
		outputCSV($page['users_array']);
	}
	
	public function deliveries()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		if (isset($_SESSION['admin_user_id']) && $_SESSION['admin_user_id'] > 0) {
			$page['page'] = 'admin/deliveries';
			
			$this->load->model('user');
			$page['users'] = $this->user->getUsers();
			
			$this->load->model('sale');
			$page['sales'] = $this->sale->getActiveRecords();
			
			$this->load->model('product');
			
			$page['products'] = array();
			$page['parents'] = array();
			
			foreach ($page['sales'] as $id => $data) {
				$page['products'][$data['product_id']] = $this->product->getRecord($data['product_id']);
				$page['parents'][$page['products'][$data['product_id']]['parent_id']] = $this->product->getRecord($page['products'][$data['product_id']]['parent_id']);
			}
			
			$this->load->model('transaction');
			
			$page['transactions'] = $this->transaction->getRecords();
			
			$page['accepted_count'] = 0;
			$page['not_accepted_count'] = 0;
			
			foreach ($page['transactions'] as $id => $data) {
				if ($data['order_accepted'] == 0) {
					$page['not_accepted_count']++;
				} else {
					$page['accepted_count']++;
				}
			}
			
			$page['accepted_by_me'] = array();
			
			foreach ($page['transactions'] as $id => $data) {
				if ($_SESSION['admin_user_id'] == $data['order_accepted']) {
					$page['accepted_by_me'][$id] = $data;
				}
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function deliveryOverview ()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/delivery-overview';
		
		$this->load->model('user');
		$page['users'] = $this->user->getUsers();
		$page['drivers'] = $this->user->getDrivers();
		
		$this->load->model('sale');
		$page['sales'] = $this->sale->getActiveRecords();
		
		$this->load->model('transaction');
		$transactions = $this->transaction->getRecordsAlt();
		
		$page['transactions'] = array();
		
		foreach ($transactions as $id => $data) {
			if ($data['cashed_out'] > 0) {
				unset($transactions[$id]);
			} else {
				$page['transactions'][$data['order_accepted']][$id] = $data;
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function userInventory ()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/user-inventory';
		
		$this->load->model('user');
		$page['users'] = $this->user->getAvailableHolders(true);
		
		$page['holders'] = $this->user->getAvailableHolders(true, true);
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getRecords();
		
		$page['inventory'] = array();
		
		foreach ($inventory as $id => $data) {
			if ($data['quantity'] > 0) {
				$page['inventory'][$data['user_id']][$id] = $data;
			}
		}
		
		unset($page['inventory'][0]);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewUserInventory ()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-user-inventory';
		
		$this->load->model('user');
		$page['user'] = $this->user->getRecord($this->input->get('user_id'));
		
		$this->load->model('inventory');
		$inventory = $this->inventory->getRecords();
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecords();
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		$page['inventory'] = array();
		
		foreach ($inventory as $id => $data) {
			if ($data['user_id'] == $this->input->get('user_id')) {
				$page['inventory'][$id] = $data;
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editOrder()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-order';
		
		$this->load->model('transaction');
		$page['transaction'] = $this->transaction->getRecord($this->input->get('transaction_id'));
		
		$this->load->model('sale');
		$page['sales'] = $this->sale->getRecordsByTransactionId($this->input->get('transaction_id'));
		
		$this->load->model('user');
		$page['user'] = $this->user->getRecord($page['transaction']['user_id']);
		
		$this->load->model('product');
		
		$page['products'] = array();
		
		foreach ($page['sales'] as $id => $data) {
			$page['products'][$id] = $this->product->getRecord($data['product_id']);
			$page['products'][$id]['parent'] = $this->product->getRecord($page['products'][$id]['parent_id']);
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function changePassword()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['user_id'] = $this->input->get('user_id');
		
		$this->load->model('user');
		$page['user'] = $this->user->getRecord($page['user_id']);
		
		$this->load->model('transaction');
		$page['transactions'] = $this->transaction->getRecordsByUserId($page['user_id']);
		
		$page['total_sales'] = 0;
		
		foreach ($page['transactions'] as $id => $data) {
			if ($data['alt_cost'] > 0) {
				$page['total_sales'] += $data['alt_cost'];
			} else {
				$page['total_sales'] += $data['cost'];
			}
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function acceptOrder()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$transaction_id = $this->input->post('transaction_id');
		
		$this->load->model('transaction');
		$status = $this->transaction->acceptOrder($transaction_id);
		
		print $status;
	}
	
	public function completeOrder()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$transaction_id = $this->input->post('transaction_id');
		
		$this->load->model('sale');
		$sales = $this->sale->getRecordsByTransactionId($transaction_id);
		
		$this->load->model('transaction');
		$transaction = $this->transaction->getRecord($transaction_id);
		
		$this->load->model('user');
		$status = $this->user->updateMembershipPaid($transaction['user_id'], 1);
		
		if ($status == 1) {
			$status = $this->transaction->completeOrder($transaction_id);
		}
		
		print $status;
	}
	
	public function memberships()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$this->load->model('zuser');
		
		$page['drivers'] = $this->user->getDrivers(true);
		$page['employees'] = $this->user->getEmployees(true);
		$page['reps'] = $this->user->getReps(true);
		$page['operators'] = $this->user->getPhoneOperators();
		$page['admins'] = $this->user->getAdmins(true);
		
		$page['users'] = $this->zuser->getRecords($page['init']['db']);

		$page['error'] = $this->input->get('error');
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function editContent()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-content';
		
		$page['content_type_id'] = $this->input->get('content_type_id');
		$page['content_id'] = $this->input->get('content_id');
		
		if ($page['content_id'] == '') {
			$page['content_id'] = 0;
		}
		
		if ($page['content_id'] > 0) {
			$this->load->model('content');
			$page['content'] = $this->content->getRecord($page['content_id']);
		}
		
		$this->load->model('contenttype');
		$page['content_types'] = $this->contenttype->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewContent()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-content';
		
		$page['content_type_id'] = $this->input->get('content_type_id');
		
		$this->load->model('content');
		$page['content'] = $this->content->getContentByContentType($page['content_type_id']);
		
		$this->load->model('contenttype');
		$page['content_types'] = $this->contenttype->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function deleteContent()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('content');
		$status = $this->content->delete($this->input->post('content_id'));
		
		print $status;
	}
	
	public function logout()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$_SESSION['user_id'] = 0;
		$_SESSION['admin_user_id'] = 0;
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		redirect('http://' . $company_info['site'] . '/admin');
	}
	
	public function editSale()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-sale';
		
		$this->load->model('sale');
		$page['sale'] = $this->sale->getRecord($this->input->get('sale_id'));
		
		$this->load->model('product');
		$this->load->model('productsize');
		$page['product_sizes'] = $this->productsize->getRecords();
		$page['all_products'] = $this->product->getRecords();
		
		$this->load->model('user');
		$page['user'] = $this->user->getRecord($page['sale']['user_id']);
		
		$page['products'] = array();
		
		if ($page['sale']['transaction_id'] > 0) {
			
			$this->load->model('transaction');
			
			$page['transaction'] = $this->transaction->getRecord($page['sale']['transaction_id']);
			
			$page['sales'] = $this->sale->getSalesByTransactionId($page['sale']['transaction_id']);
			
			foreach ($page['sales'] as $id => $data) {
				
				$page['products'][$data['product_id']] = $this->product->getRecord($data['product_id']);
				//$page['products'][$data['product_id']]['parent'] = $this->product->getRecord($page['products'][$data]['parent_id']);
				$page['products'][$data['product_id']]['size'] = $this->productsize->getRecord($data['product_size_id']);
			}
			//This is a membership sale
		} else {
			$membership_product = $this->product->getRecord($page['sale']['product_id']);
			$page['products'][$membership_product['id']] = $membership_product;
			$page['products'][$membership_product['id']]['parent']['name'] = 'Free Tree';
		}
		
		$page['total_cost'] = 0;
		$page['transaction_id'] = $page['sale']['transaction_id'];
		
		foreach ($page['products'] as $id => $data) {
			$page['total_cost'] += $data['price'];
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewSale()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'view-sale';
		
		$this->load->model('sale');
		$page['sale'] = $this->sale->getRecord($this->input->get('sale_id'));
		
		$this->load->model('product');
		$this->load->model('productsize');
		$page['product_sizes'] = $this->productsize->getRecords();
		$page['all_products'] = $this->product->getRecords();
		
		$this->load->model('user');
		$page['user'] = $this->user->getRecord($page['sale']['user_id']);
		
		$page['products'] = array();
		
		if ($page['sale']['transaction_id'] > 0) {
			
			$this->load->model('transaction');
			
			$page['transaction'] = $this->transaction->getRecord($page['sale']['transaction_id']);
			
			$page['sales'] = $this->sale->getSalesByTransactionId($page['sale']['transaction_id']);
			
			foreach ($page['sales'] as $id => $data) {
				
				$page['products'][$data['product_id']] = $this->product->getRecord($data['product_id']);
				//$page['products'][$data['product_id']]['parent'] = $this->product->getRecord($page['products'][$data]['parent_id']);
				$page['products'][$data['product_id']]['size'] = $this->productsize->getRecord($data['product_size_id']);
			}
			//This is a membership sale
		} else {
			$membership_product = $this->product->getRecord($page['sale']['product_id']);
			$page['products'][$membership_product['id']] = $membership_product;
			$page['products'][$membership_product['id']]['parent']['name'] = 'Free Tree';
		}
		
		$page['total_cost'] = 0;
		$page['transaction_id'] = $page['sale']['transaction_id'];
		
		foreach ($page['products'] as $id => $data) {
			$page['total_cost'] += $data['price'];
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewTransaction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$this->load->model('sale');
		$this->load->model('transaction');
		$this->load->model('product');
		$this->load->model('productsize');
		
		
		$transaction = $this->transaction->getRecord($this->input->get('transaction_id'));
		$user = $this->user->getRecord($transaction['user_id']);
		
		$sales = $this->sale->getRecordsByTransactionId($transaction['id']);
		
		$total = 0;
		
		foreach ($sales as $id => $data) {
			$sales[$id]['product'] = $this->product->getRecord($data['product_id']);
			$sales[$id]['product_size'] = $this->productsize->getRecord($data['product_size_id']);
			
			$total += $sales[$id]['product_size']['price'];
		}
		
		$page['transaction_id'] = $this->input->get('transaction_id');
		$page['transaction'] = $this->transaction->getRecord($this->input->get('transaction_id'));
		
		$page['sales'] = $this->sale->getRecordsByTransactionId($this->input->get('transaction_id'));
		
		
		$page['product_sizes'] = $this->productsize->getRecords();
		$page['all_products'] = $this->product->getRecords();
		
		$page['user'] = $this->user->getRecord($page['transaction']['user_id']);
		
		$page['products'] = array();
		
		foreach ($page['sales'] as $id => $data) {
			$page['products'][$data['product_id']] = $this->product->getRecord($data['product_id']);
			//$page['products'][$data['product_id']]['parent'] = $this->product->getRecord($page['products'][$data]['parent_id']);
			$page['products'][$data['product_id']]['size'] = $this->productsize->getRecord($data['product_size_id']);
		}
		
		$page['total_cost'] = 0;
		
		foreach ($page['products'] as $id => $data) {
			$page['total_cost'] += $data['price'];
		}
		
		$page['users'] = $this->user->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function switchActive()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$status = $this->product->switchActive($_POST['product_id'], $_POST['source']);
		
		print $status;
		exit;
	}
	
	public function switchAllowNegative()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$status = $this->companyinfo->switchAllowNegative($_POST['source']);
		
		print $status;
		exit;
	}
	
	public function tareAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('inventory');
		$records = $this->inventory->getRecordsByProductId($_POST['product_id']);
		
		foreach ($records as $id => $data) {
			if ($data['quantity'] < 0) {
				$status = $this->inventory->writeData($id, 0, $data['product_id'], $data['user_id']);
			}
		}
		
		print $status;
		exit;
	}
	
	public function updateProductName()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$status = $this->product->updateName($_POST['product_id'], $_POST['name']);
		
		print $status;
		exit;
	}
	
	public function updateTransactionsAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('transaction');
		
		$status = 1;
		
		foreach ($_POST['delete_list'] as $i => $transaction_id) {
			if ($status == 1) {
				$status = $this->transaction->delete($transaction_id);
			}
		}
		
		print 1;
		exit;
	}
	
	public function editTransaction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$this->load->model('sale');
		$this->load->model('transaction');
		$this->load->model('product');
		$this->load->model('productsize');
		
		$page['transaction_id'] = $this->input->get('transaction_id');
		$page['transaction'] = $this->transaction->getRecord($this->input->get('transaction_id'));
		
		$page['sales'] = $this->sale->getRecordsByTransactionId($this->input->get('transaction_id'));
		
		$page['product_sizes'] = $this->productsize->getRecords();
		$page['all_products'] = $this->product->getRecords();
		
		$page['user'] = $this->user->getRecord($page['transaction']['user_id']);
		
		$page['products'] = array();
		
		foreach ($page['sales'] as $id => $data) {
			
			$page['products'][$data['product_id']] = $this->product->getRecord($data['product_id']);
			//$page['products'][$data['product_id']]['parent'] = $this->product->getRecord($page['products'][$data]['parent_id']);
			$page['products'][$data['product_id']]['size'] = $this->productsize->getRecord($data['product_size_id']);
		}
		
		$page['total_cost'] = 0;
		
		foreach ($page['products'] as $id => $data) {
			$page['total_cost'] += $data['price'];
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function deleteSale()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('sale');
		$status = $this->sale->delete($this->input->post('sale_id'));
		
		print $status;
	}
	
	public function deleteTransaction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('transaction');
		$status = $this->transaction->delete($this->input->post('transaction_id'));
		
		print $status;
	}
	
	public function goUndercover ()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$_SESSION['user_id'] = $_POST['user_id'];
		
		print 1;
	}
	
	public function grower()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'grower';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editGrower()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-grower';
		
		$page['grower_id'] = $this->input->get('grower_id');
		
		if ($page['grower_id'] == '') {
			$page['grower_id'] = 0;
		}
		
		if ($page['grower_id'] > 0) {
			$this->load->model('grower');
			$page['grower'] = $this->grower->getRecord($page['grower_id']);
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewGrower()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-grower';
		
		$this->load->model('grower');
		$page['grower'] = $this->grower->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeGrower()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'grower_id' => $this->input->post('grower_id'),
						'rep_name' => $this->input->post('rep_name'),
						'phone' => $this->input->post('phone'),
						'address' => $this->input->post('address'),
						'website' => $this->input->post('website'),
						'name' => $this->input->post('name'),
						'description' => $this->input->post('description'),
						'attachment' => $this->input->post('attachment')
		);
		
		$this->load->model('grower');
		$status = $this->grower->writeData($data);
		
		print $status;
	}
	
	public function deleteGrower()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('grower');
		$status = $this->grower->delete($this->input->post('grower_id'));
		
		print $status;
	}
	
	public function cashOutAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		foreach ($_POST['orders'] as $i => $transaction_id) {
			$this->load->model('transaction');
			$status = $this->transaction->cashOut($transaction_id);
		}
		
		print $status;
	}
	
	public function vendor()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'vendor';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editVendor()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-vendor';
		
		$page['vendor_id'] = $this->input->get('vendor_id');
		
		if ($page['vendor_id'] == '') {
			$page['vendor_id'] = 0;
		}
		
		if ($page['vendor_id'] > 0) {
			$this->load->model('vendor');
			$page['vendor'] = $this->vendor->getRecord($page['vendor_id']);
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewVendor()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-vendor';
		
		$this->load->model('vendor');
		$page['vendor'] = $this->vendor->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeVendor()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'vendor_id' => $this->input->post('vendor_id'),
						'rep_name' => $this->input->post('rep_name'),
						'phone' => $this->input->post('phone'),
						'address' => $this->input->post('address'),
						'website' => $this->input->post('website'),
						'name' => $this->input->post('name'),
						'description' => $this->input->post('description'),
						'attachment' => $this->input->post('attachment')
		);
		
		$this->load->model('vendor');
		$status = $this->vendor->writeData($data);
		
		print $status;
	}
	
	public function deleteVendor()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('vendor');
		$status = $this->vendor->delete($this->input->post('vendor_id'));
		
		print $status;
	}
	
	public function expense()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'expense';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editExpense()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-expense';
		
		$page['expense_id'] = $this->input->get('expense_id');
		
		if ($page['expense_id'] == '') {
			$page['expense_id'] = 0;
		}
		
		if ($page['expense_id'] > 0) {
			$this->load->model('expense');
			$page['expense'] = $this->expense->getRecord($page['expense_id']);
		}
		
		$this->load->model('vendor');
		$page['vendors'] = $this->vendor->getRecords();
		
		array_unshift($page['vendors'], array('name' => '- Not Related To A Vendor -'));
		
		$this->load->model('product');
		$page['products'] = $this->product->getRecords();
		
		array_unshift($page['products'], array('name' => '- Not Related To A Product -'));
		
		$page['reoccurring'] = array(
						0 => 'No',
						1 => 'Daily',
						2 => 'Weekly',
						3 => 'Monthly',
						4 => 'Annually'
		);
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function expenses()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('expense');
		$page['expenses'] = $this->expense->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeExpense()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('expense');
		$status = $this->expense->writeData(
				$this->input->post('expense_id'),
				$this->input->post('title'),
				$this->input->post('amount'),
				$this->input->post('attachment'),
				$this->input->post('reoccurring'),
				$this->input->post('reoccurring_start'),
				$this->input->post('alt_date'),
				$this->input->post('note'),
				$this->input->post('left_owed'),
				$this->input->post('vendor_id'),
				$this->input->post('product_id')
				);
		
		print $status;
	}
	
	public function deleteExpense()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('expense');
		$status = $this->expense->delete($this->input->post('expense_id'));
		
		print $status;
	}
	
	
	public function education()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'education';
		$page['base'] = 'http://' . $_SERVER['SERVER_NAME'];
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function editEducation()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'edit-education';
		
		$page['education_id'] = $this->input->get('education_id');
		
		if ($page['education_id'] == '') {
			$page['education_id'] = 0;
		}
		
		if ($page['education_id'] > 0) {
			$this->load->model('education');
			$page['education'] = $this->education->getRecord($page['education_id']);
		}
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function viewEducation()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/view-education';
		
		$this->load->model('education');
		$page['education'] = $this->education->getRecords();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function writeEducation()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$data = array(
						'education_id' => $this->input->post('education_id'),
						'title' => $this->input->post('title'),
						'content_text' => $this->input->post('content_text'),
						'attachment' => $this->input->post('attachment')
		);
		
		$this->load->model('education');
		$status = $this->education->writeData($data);
		
		print $status;
	}
	
	public function deleteEducation()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('education');
		$status = $this->education->delete($this->input->post('education_id'));
		
		print $status;
	}
	
	public function promotion()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/promotion';
		
		$this->load->model('promotion');
		$this->load->model('promotiontype');
		
		$page['promotions'] = $this->promotion->getRecordsAdmin();
		$page['promotion_types'] = $this->promotiontype->getRecordsAdmin();
		
		$page['memberships'] = array(
						0 => '-',
						1 => 'Green',
						2 => 'Gold',
						3 => 'Black'
		);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function issuePromotion()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'admin/issue-promotion';
		
		$this->load->model('promotion');
		$this->load->model('promotiontype');
		
		$page['promotion_type_id'] = $this->input->get('id');
		$page['promotion_type'] = $this->promotiontype->getRecord($page['promotion_type_id']);
		
		$page['promotions'] = $this->promotion->getRecordsByPromotionTypeAdmin($page['promotion_type_id']);
		$page['promotion_types'] = $this->promotiontype->getRecordsAdmin();
		
		$page['memberships'] = array(
						0 => '-',
						1 => 'Green',
						2 => 'Gold',
						3 => 'Black'
		);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function generateCode()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		if ($this->input->post('promotion_type_id') == '') {
			$promotion_type_id = 0;
		} else {
			$promotion_type_id = $this->input->post('promotion_type_id');
		}
		
		if ($this->input->post('user_id') == '') {
			$user_id = 0;
		} else {
			$user_id = $this->input->post('user_id');
		}
		
		$this->load->model('promotion');
		$status = $this->promotion->generateCode($promotion_type_id, $user_id);
		
		print $status;
	}
	
	public function payMembershipAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		
		$record = $this->user->getRecord($this->input->post('user_id'));
		
		$status = $this->user->payMembership($record['id'], $record['membership']);
		
		print $status;
	}
	
	public function setThemeAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		if (!isset($_SESSION['admin_user_id']) || $_SESSION['admin_user_id'] == 0) {
			print -1;
		} else {
			$this->load->model('user');
			$status = $this->user->setTheme($_SESSION['admin_user_id'], $this->input->post('theme'));
			
			print $status;
		}
	}
	
	public function meta()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$page['page'] = 'meta';
		
		$this->load->model('metadata');
		$page['meta'] = $this->metadata->getRecord();
		
		//Init functions and page load
		$this->load->model('_admin');
		$page['loader'] = $this->_admin->load($page);
	}
	
	public function writeMetaAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('metadata');
		$status = $this->metadata->writeData($_POST['title'], $_POST['image'], $_POST['description'], $_POST['keywords'], $_POST['google'], $_POST['facebook']);
		
		print $status;
		exit;
	}
	
	public function changePrimaryHolderAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->database();
		$this->db->trans_start();
		
		$this->load->model('inventory');
		
		if ($_POST['old_inventory'] == 1) {
			$old_house_records = $this->inventory->getPrimaryHouseRecords();
		}
		
		$this->load->model('companyinfo');
		$status = $this->companyinfo->changePrimaryHolder($_POST['user_id']);
		
		if ($status == 1) {
			$records = $this->inventory->getProductsWithoutHolder($_POST['user_id']);
			
			foreach ($records as $product_id) {
				$status = $this->inventory->writeNewHolder($_POST['user_id'], 0, $product_id);
			}
		}
		
		if ($_POST['old_inventory'] == 1) {
			foreach ($old_house_records as $product_id2 => $data) {
				//Get new primary holders record for this product
				$record = $this->inventory->getUserRecord($_POST['user_id'], $product_id2);
				
				//Add old holders quantity to new holders total
				$status = $this->inventory->writeData($record['id'], $record['quantity'] + $data['quantity'], $product_id2, $_POST['user_id']);
				
				if ($status == 1) {
					//Zero out old holders inventory
					$status = $this->inventory->writeData($data['id'], 0, $product_id2, $data['user_id']);
				}
			}
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$status = 'DB transaction error';
		}
		
		print $status;
		exit;
	}
	
	public function overridePrimaryHolderAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$status = $this->product->changePrimaryHolder($_POST['product_id'], $_POST['user_id']);
		
		print $status;
		exit;
	}
	
	
	public function updatePNLAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		if ($_POST['type'] == 'card_tax') {
			$this->load->model('creditmerchant');
			
			$record = $this->creditmerchant->getRecord($_POST['credit_merchant_id']);
			$status = $this->creditmerchant->writeData($record['id'], $record['name'], $_POST['card_tax'], $record['rep_name'], $record['phone'], $record['email']);
			
		} else {
			
		}
		
		print $status;
	}
	
	public function invoice () {
		$this->load->model('transaction');
		$this->load->model('user');
		$this->load->model('sale');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('companyinfo');
		
		$company = $this->companyinfo->getRecord();
		
		$transaction = $this->transaction->getRecord($this->input->get('transaction_id'));
		$user = $this->user->getRecord($transaction['user_id']);
		
		$sales = $this->sale->getRecordsByTransactionId($transaction['id']);
		
		$total = 0;
		
		foreach ($sales as $id => $data) {
			$sales[$id]['product'] = $this->product->getRecord($data['product_id']);
			$sales[$id]['product_size'] = $this->productsize->getRecord($data['product_size_id']);
			
			$total += $sales[$id]['product_size']['price'] * $data['quantity'];
		}
		
		if ($transaction['alt_cost'] > 0) {
			$cost = $transaction['alt_cost'];
		} else {
			$cost = $transaction['cost'];
		}
		
		if ($transaction['sale_by'] > 0) {
			$record = $this->user->getRecord($transaction['sale_by']);
			$sale_by = $record['first_name'] . ' ' . $record['last_name'];
		} else {
			$sale_by = '';
		}
		
		$data = array(
						'name' => $user['first_name'] . ' ' . $user['last_name'],
						'transaction_id' => $transaction['id'],
						'customer' => $user['first_name'] . ' ' . $user['last_name'],
						'delivery_address1' => $user['address'],
						'delivery_address2' => 	$user['city'] . ', ' . $user['zip'],
						'company' => $company,
						'phone' => $user['phone'],
						'notes' => $transaction['notes'],
						'transaction' => $transaction,
						'sale_by' => $sale_by,
						'cost' => $cost,
						'total' => $total,
						'sales' => $sales,
						'normal' => true
		);
		
		$this->load->view('email/invoice2.phtml', $data);
	}
	
	public function sendInvoiceAction()
	{
		$this->load->model('transaction');
		$this->load->model('user');
		$this->load->model('sale');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('companyinfo');
		
		$company = $this->companyinfo->getRecord();
		
		$transaction = $this->transaction->getRecord($_POST['transaction_id']);
		$user = $this->user->getRecord($transaction['user_id']);
		
		$sales = $this->sale->getRecordsByTransactionId($transaction['id']);
		
		$total = 0;
		
		foreach ($sales as $id => $data) {
			$sales[$id]['product'] = $this->product->getRecord($data['product_id']);
			$sales[$id]['product_size'] = $this->productsize->getRecord($data['product_size_id']);
			
			$total += $sales[$id]['product_size']['price'] * $data['quantity'];
		}
		
		$this->load->library('email');
		$this->email->set_mailtype("html");
		
		$this->email->from('info@' . $company['site'], $company['name'] . ' Alerts');
		
		//$this->email->to('ahartjoe@gmail.com');
		$this->email->to($user['email']);
		
		//$this->email->cc('another@another-example.com');
		$this->email->bcc('apogeereports@gmail.com');
		
		$this->email->subject('Your Invoice From ' . $company['name']);
		
		if ($transaction['alt_cost'] > 0) {
			$cost = $transaction['alt_cost'];
		} else {
			$cost = $transaction['cost'];
		}
		
		if ($transaction['sale_by'] > 0) {
			$record = $this->user->getRecord($transaction['sale_by']);
			$sale_by = $record['first_name'] . ' ' . $record['last_name'];
		} else {
			$sale_by = '';
		}
		
		$data = array(
						'name' => $user['first_name'] . ' ' . $user['last_name'],
						'transaction_id' => $transaction['id'],
						'customer' => $user['first_name'] . ' ' . $user['last_name'],
						'delivery_address1' => $user['address'],
						'delivery_address2' => 	$user['city'] . ', ' . $user['zip'],
						'company' => $company,
						'phone' => $user['phone'],
						'notes' => $transaction['notes'],
						'transaction' => $transaction,
						'sale_by' => $sale_by,
						'cost' => $cost,
						'total' => $total,
						'sales' => $sales,
						'normal' => true
		);
		
		$body = $this->load->view('email/invoice2.phtml', $data, TRUE);
		$this->email->message($body);
		
		$this->email->send();
		
		$status = $this->email->print_debugger();
		
		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
		
		print $status;
		exit;
	}
	
	public function changePendingAction () {
		$this->load->model('transaction');
		$status = $this->transaction->changePending($_POST['transaction_id']);
		
		print $status;
		exit;
	}
	
	public function changeRemainingBalanceAction () {
		$this->load->model('transaction');
		$status = $this->transaction->changeRemainingBalance($_POST['transaction_id'], $_POST['partial_payment']);
		
		if ($status == 1) {
			$this->load->model('partialpayment');
			$status = $this->partialpayment->writeData(0, $_POST['transaction_id'], $_POST['partial_payment'], '');
		}
		
		print $status;
		exit;
	}
	
	public function datasets()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('zdatasets');
		$page['datasets'] = $this->zdatasets->getRecords($page['init']['db']);

		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function steps()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();

		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function meds()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('zmedicationdata');
		$meds = $this->zmedicationdata->getRecordsByMed($page['init']['db']);
		
		$page['meds'] = array();
		
		foreach ($meds as $name => $records) {
			
			$amount_total = 0;
			$total_doses = 0;
			$count = 0;

			foreach ($records as $i => $data) {
				$amount_total += $data['ZAMOUNT'];
				$total_doses += $data['ZNDOSES'];
				$count++;
			}
			
			$page['meds'][$name]['avg_amount'] = round($amount_total / $count, 2);
			$page['meds'][$name]['units'] = $data['ZUNITS'];
			$page['meds'][$name]['total_doses'] = $total_doses;
			$page['meds'][$name]['total_amount'] = $amount_total;
			$page['meds'][$name]['avg_doses'] = round($total_doses / $count, 2);
			$page['meds'][$name]['name'] = $name;
		}

		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function tests()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
	
	public function bloodPressure()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('zuser');
		$this->load->model('zheartrate');
		$this->load->model('zbloodpressure');
		
		
		$page['users'] = $this->zuser->getRecords($page['init']['db']);
		$heart_rates = $this->zheartrate->getRecordsByUser($page['init']['db']);
		$blood_pressures = $this->zbloodpressure->getRecordsByUser($page['init']['db']);

		$page_users = array();
		
		foreach ($page['users'] as $user_id => $data) {

			$all_heart_total = 0;
			$count = 0;
			
			if (isset($heart_rates[$user_id]) && count($heart_rates[$user_id]) > 0) {
				foreach ($heart_rates[$user_id] as $i => $data2) {
					$all_heart_total += $data2['ZRATE'];
					$count++;
				}
				
				$page['heart_avg'] = round($all_heart_total / $count, 2);
			} else {
				$page['heart_avg'] = 0;
			}
			
			$all_d_total = 0;
			$all_s_total = 0;
			$count = 0;

			if (isset($blood_pressures[$user_id]) && count($blood_pressures[$user_id]) > 0) {
				foreach ($blood_pressures[$user_id] as $i => $data2) {
					$all_d_total += $data2['ZDIASTOLIC'];
					$all_s_total += $data2['ZSYSTOLIC'];
					$count++;
				}
				
				$page['d_avg'] = round($all_d_total / $count, 2);
				$page['s_avg'] = round($all_s_total / $count, 2);
			} else {
				$page['d_avg'] = 0;
				$page['s_avg'] = 0;
			}

			$page_users[$user_id] = array(
				'heart_avg' => $page['heart_avg'],
				'd_avg' => $page['d_avg'],
				's_avg' => $page['s_avg']
			);
		}
		
		$page['page_users'] = $page_users;

		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	
	}
	
	public function diet()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('znutrientgoals');
		$page['goals'] = $this->znutrientgoals->getRecordsByUser($page['init']['db']);
		
		$carb = 0;
		$protein = 0;
		$calories = 0;
		$fiber = 0;
		$sugar = 0;
		$sodium = 0;
		$fat = 0;
		$cholesterol = 0;
		
		foreach ($page['goals'] as $user_id => $data) {
			
			$carb += $data[0]['ZVALUE'];
			$protein += $data[1]['ZVALUE'];
			$calories += $data[2]['ZVALUE'];
			$fiber += $data[3]['ZVALUE'];
			$sugar += $data[4]['ZVALUE'];
			$sodium += $data[5]['ZVALUE'];
			$fat += $data[6]['ZVALUE'];
			$cholesterol += $data[7]['ZVALUE'];
		}
		
		$page['carb'] = round($carb / count($page['goals']), 2) . ' ' . $page['goals'][2][0]['ZUNIT'];
		$page['protein'] = round($protein / count($page['goals']), 2) . ' ' . $page['goals'][2][1]['ZUNIT'];
		$page['calories'] = round($calories / count($page['goals']), 2) . ' ' . $page['goals'][2][2]['ZUNIT'];
		$page['fiber'] = round($fiber / count($page['goals']), 2) . ' ' . $page['goals'][2][3]['ZUNIT'];
		$page['sugar'] = round($sugar / count($page['goals']), 2) . ' ' . $page['goals'][2][4]['ZUNIT'];
		$page['sodium'] = round($sodium / count($page['goals']), 2) . ' ' . $page['goals'][2][5]['ZUNIT'];
		$page['fat'] = round($fat / count($page['goals']), 2) . ' ' . $page['goals'][2][6]['ZUNIT'];
		$page['cholesterol'] = round($cholesterol / count($page['goals']), 2) . ' ' . $page['goals'][2][7]['ZUNIT'];

		$this->load->model('znutrientdatanixid');
		$page['nutrients'] = $this->znutrientdatanixid->getRecords($page['init']['db']);
		
		//Init functions and page load
		$this->load->model('_admin');
		$this->_admin->load($page);
	}
}