<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MembershipController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		$this->load->helper('url');
		redirect('http://' . $page_data['company_info']['site'] . '/join');

	}
}