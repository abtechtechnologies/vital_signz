<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JoinController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->load->model('user');
		$all_codes = $this->user->getCodes();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
}