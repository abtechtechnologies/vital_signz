<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		}
		
		$this->load->model('user');
		$this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);

		$this->load->model('sale');
		$this->page_data['sales'] = $this->sale->getRecordsByUserId($_SESSION['user_id']);
		
		$this->load->model('transaction');
		$this->page_data['transactions'] = $this->transaction->getRecordsByUserId($_SESSION['user_id']);
		$recent_transaction = $this->transaction->getLastTransactionByUser($_SESSION['user_id']);
		
		if ($recent_transaction != false) {
			$this->page_data['recent_sales'] = $this->sale->getRecordsByTransactionId($recent_transaction['id']);
		} else {
			$this->page_data['recent_sales'] = array();
		}
		
		$this->load->model('product');
		$this->page_data['products'] = $this->product->getRecordsAll();
		
		$referrals = $this->user->getReferrals($_SESSION['user_id']);
		$this->page_data['referrals'] = array();
		$this->page_data['referral_earn'] = 0;
		
		foreach ($referrals as $id => $data) {
			$cost = 0;
			$this->page_data['referrals'][$id] = array();
			$referral_sales = $this->sale->getRecordsByUserId($id);
			$this->page_data['referrals'][$id]['sales'] = array();
			
			foreach ($referral_sales as $id2 => $data2) {
				$this->page_data['referrals'][$id]['sales'][] = $this->page_data['products'][$data2['product_id']]['price'];
				$cost += $this->page_data['products'][$data2['product_id']]['price'];
			}
			
			$this->page_data['referral_earn'] += $cost;
		}
		
		$this->page_data['referral_earn'] = ceil($this->page_data['referral_earn'] * 0.05);
		
		$this->page_data['levels'] = array(
			0 => 'None',
			1 => 'Green',
			2 => 'Gold',
			3 => 'Black'
		);
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function edit()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
	
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		}
	
		$this->load->model('user');
		$this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);
	
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function gift()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	    $this->load->model('companyinfo');
	    $this->page_data['company_info'] = $this->companyinfo->getRecord();
	
	    if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
	        //Don't allow access without a logged in user
	        redirect('http://' . $this->page_data['company_info']['site']);
	    }
	
	    $this->load->model('user');
	    $this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);
	
	    //Init functions and page load
	    $this->load->model('_loader');
	    $this->_loader->load($this->page_data);
	}
	
	public function updateInfoAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$name = explode(' ', $_POST['name']);
		
		$this->load->model('user');
		$user = $this->user->getRecord($_SESSION['user_id']);

		$status = $this->user->writeData($_SESSION['user_id'], $name[0], $name[1], $user['email'], 2, $_POST['phone'], '', $_POST['address'], $_POST['city'], $_POST['zip']);
		
		return $status;
	}
	
	public function sendReferral () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('user');
		$user = $this->user->getRecord($_SESSION['user_id']);
		
		//Check to make sure the user didnt enter his/her own email.... Stoners...		
		$users = $this->user->getUsers();
		$found = 0;
		
		foreach ($users as $id => $data) {
			if ($data['email'] == $_POST['email']) {
				$found = $id;
			}
		}
		
		if ($found > 0) {
			if ($user['email'] == $_POST['email']) {
				$status = 'Sooo, you are trying to referr yourself? No dice.';
			} else {
				$status = 'That email address is already in our system.';
			}
		} else {
			$this->load->library('email');
			$this->email->set_mailtype("html");
				
			$this->email->from('info@wwweed.org', $company['name']);
			
			$this->email->DKIM_domain = $company['site'];
			$this->email->DKIM_private = '../../../config/p_key.txt';
			$this->email->DKIM_selector = 'phpmailer';
			$this->email->DKIM_passphrase = '';
			$this->email->DKIM_identity = 'info@wwweed.org';
				
			$this->email->to($_POST['email']);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');
			
			$name = ucfirst($user['first_name']) . ' ' . ucfirst(substr($user['last_name'], 0, 1));
				
			$this->email->subject($name . ' has invited to you join WWWEED.ORG!');
				
			$data = array(
				'name' => $name
			);
				
			$body = $this->load->view('email/referral.phtml', $data, TRUE);
			$this->email->message($body);
				
			$this->email->send();
				
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$this->load->model('referral');
				$status = $this->referral->writeData($_SESSION['user_id'], $_POST['email']);
			} else {
				$status = 'There was an error sending the email. Please try again.';
			}
		}
					
		print $status;
		exit;
	}
	
	public function sendGift () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	    $this->load->model('companyinfo');
	    $company = $this->companyinfo->getRecord();
	    
	    do {
    	    if ($_POST['friend_user_id'] == $_SESSION['user_id']) {
    	        $status = 'Sooo, you are trying to gift yourself? No dice.';
    	        break;
    	    }
    	
    	    if ($_POST['friend_user_id'] == 0 || $_POST['friend_user_id'] < 0 || $_POST['friend_user_id'] == 1 || $_POST['friend_user_id'] == 2) {
    	        $status = 'Not a valid user';
    	        break;
    	    }
    	    
    	    $this->load->model('user');
    	    $user = $this->user->getRecord($_SESSION['user_id']);
    	    
    	    //Check to make sure the user didnt enter his/her own ID.... Stoners...
    	    $users = $this->user->getUsers();
    	    $found = 0;
    	
    	    foreach ($users as $id => $data) {
    	        if ($id == $_POST['friend_user_id']) {
    	            $found = $id;
    	        }
    	    }
    	
    	    //ID exists
    	    if ($found > 0) {
    	        $status = $this->user->moveBalance($_SESSION['user_id'], $_POST['friend_user_id'], $_POST['amount']);
//     	        $this->load->library('email');
//     	        $this->email->set_mailtype("html");
    	         
//     	        $this->email->from('info@wwweed.org', $company['name']);
    	        
//     	        $this->email->DKIM_domain = $company['site'];
//     	        $this->email->DKIM_private = '../../../config/p_key.txt';
//     	        $this->email->DKIM_selector = 'phpmailer';
//     	        $this->email->DKIM_passphrase = '';
//     	        $this->email->DKIM_identity = 'info@wwweed.org';
    	         
//     	        $this->email->to($_POST['email']);
//     	        //$this->email->cc('another@another-example.com');
//     	        //$this->email->bcc('them@their-example.com');
    	        
//     	        $name = ucfirst($user['first_name']) . ' ' . ucfirst(substr($user['last_name'], 0, 1));
    	         
//     	        $this->email->subject($name . ' has invited to you join WWWEED.ORG!');
    	         
//     	        $data = array(
//     	                'name' => $name
//     	        );
    	         
//     	        $body = $this->load->view('email/referral.phtml', $data, TRUE);
//     	        $this->email->message($body);
    	         
//     	        $this->email->send();
    	         
//     	        $status = $this->email->print_debugger();
    	        
//     	        if (trim(strip_tags($status)) == 0) {
//     	            $this->load->model('referral');
//     	            $status = $this->referral->writeData($_SESSION['user_id'], $_POST['email']);
//     	        } else {
//     	            $status = 'There was an error sending the email. Please try again.';
//     	        }

    	        
    	    } else {
    	        $status = 'That ID is not found in our system';
    	        break;
    	    }
	    } while (false);
	    	
	    print $status;
	    exit;
	}
}