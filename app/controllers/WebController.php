<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sitemap()
	{
		$this->load->view('home/urllist.txt');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();	
		
		if (isset($_SESSION['company_id']) && $_SESSION['company_id'] > 0) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$this->load->helper('url');
			redirect('http://' . $company['site'] . '/command');
		}

		$this->load->model('zuser');
		$user = $this->zuser->getRecord($_SESSION['user_id']);
		
		$this->load->model('zalert');
		$this->page_data['alerts'] = $this->zalert->getCompanyRecords($user['company_id']);

		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('activity');
		$this->page_data['activity'] = $this->activity->getRecordsByUser($_SESSION['user_id']);
		
		$this->page_data['users'] = $this->zuser->getRecords();
		
		$this->load->model('company');
		$this->page_data['company'] = $this->company->getRecord($user['company_id']);
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function admin()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		

		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function diet()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		

		$this->load->model('znutrientgoals');
		$this->page_data['goals'] = $this->znutrientgoals->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('Zfooddata');
		$this->page_data['food'] = $this->Zfooddata->getRecords();

		$time = time();

		foreach ($this->page_data['food'] as $i => $data) {
			//$this->page_data['food'][$i]['date'] = date('M d, Y h:i A', $data['ZDATE'] + 978307200);
			
		//	$this->page_data['food'][$i]['unix'] = strtotime($this->page_data['food'][$i]['date']);
		}
		
		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		$this->page_data['today'] = array();
		
		//Data for graph
		for ($i = 0; $i < 7; $i++) {
			
			//$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
			$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->Zfooddata->getRecordsByDateRange($_SESSION['user_id'], $last, $last);
			
			if ($i == 0) {
				$this->page_data['today'] = $records;
			}

			$day_sum_cal = 0;
			
			foreach ($records as $id => $data) {
				$day_sum_cal += $data['calories'];
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['cals'] = 0;
			} else {
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['cals'] = $day_sum_cal;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);
		
		$this->page_data['t_calories'] = 0;
		$this->page_data['t_cholesterol'] = 0;
		$this->page_data['t_dietary_fiber'] = 0;
		$this->page_data['t_potassium'] = 0;
		$this->page_data['t_protein'] = 0;
		$this->page_data['t_saturated_fat'] = 0;
		$this->page_data['t_sodium'] = 0;
		$this->page_data['t_sugars'] = 0;
		$this->page_data['t_total_carbohydrate'] = 0;
		$this->page_data['t_total_fat'] = 0;
		
		foreach ($this->page_data['today'] as $id => $data) {
			$this->page_data['t_calories'] += $data['calories'];
			$this->page_data['t_cholesterol'] += $data['cholesterol'];
			$this->page_data['t_dietary_fiber'] += $data['dietary_fiber'];
			$this->page_data['t_potassium'] += $data['potassium'];
			$this->page_data['t_protein'] += $data['protein'];
			$this->page_data['t_saturated_fat'] += $data['saturated_fat'];
			$this->page_data['t_sodium'] += $data['sodium'];
			$this->page_data['t_sugars'] += $data['sugars'];
			$this->page_data['t_total_carbohydrate'] += $data['total_carbohydrate'];
			$this->page_data['t_total_fat'] += $data['total_fat'];
		}
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function blood()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('zbloodpressure');
		$this->page_data['bp'] = $this->zbloodpressure->getUserRecords($_SESSION['user_id']);
		
		$this->page_data['filter'] = $this->input->get('filter');

		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zbloodpressure');

		//Data for graph
		for ($i = 0; $i < count($dates); $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' month'));
			} else if ($this->page_data['filter'] == 5) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' months'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' months'));
			} else if ($this->page_data['filter'] == 1) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' hours'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' hours'));
			} else {
				$first = date('Y-m-d', strtotime( '-' . $i . ' day'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));

			$date = explode('-', $last);

			//Get records for each day
			//first and last same for now

			$records = $this->zbloodpressure->getRecordsByDateRange($_SESSION['user_id'], $first, $last);

			$day_sum_dia = 0;
			$day_sum_sys = 0;

			foreach ($records as $id => $data) {
				$day_sum_dia += $data['ZDIASTOLIC'];
				$day_sum_sys += $data['ZSYSTOLIC'];
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_dia'] = 0;
				$this->page_data['totals'][$last]['avg_sys'] = 0;
			} else {
				$day_avg_dia = $day_sum_dia / count($records);
				$day_avg_sys = $day_sum_sys / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['avg_dia'] = $day_avg_dia;
				$this->page_data['totals'][$last]['avg_sys'] = $day_avg_sys;
			}
			
		}

		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['bp'] = array_reverse($this->page_data['bp']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function compare()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['url'] = 'lab_array=' . $this->input->get('lab_array') . '&vital_array=' . $this->input->get('vital_array') . '&med_array=' . $this->input->get('med_array') . '&nutrient_array=' . $this->input->get('nutrient_array');

		$this->page_data['filter'] = $this->input->get('filter');

		$this->load->model('_utility');
		$dates = $this->_utility->returnFilterDates($this->page_data['filter'], 'labs');		

		$this->load->model('labs');
		$this->load->model('zlabdata');
		$this->load->model('zdatasets');
		$this->load->model('zbloodpressure');
		
		$lab_array = explode(',', $this->input->get('lab_array'));
		$this->page_data['lab_array'] = array();

		foreach ($lab_array as $i => $id) {		
			if ($id > 0) {
				$test = $this->zlabdata->getRecord($id);
				$this->page_data['lab_array'][$i]['ZNAME'] = $test['ZNAME'];
			}
		}

		foreach ($this->page_data['lab_array'] as $id => $lab_record_data_yo) {			
			$lab_name = $lab_record_data_yo['ZNAME'];
			$lab_info = $this->zdatasets->getNameMatch($lab_record_data_yo['ZNAME']);
			
			$this->page_data['lab_array'][$id]['ZUNITS'] = $lab_info['ZUNITS'];

			$labs = $this->labs->getUserRecordsByName($_SESSION['user_id'], $lab_record_data_yo['ZNAME']);
			
			foreach ($dates as $date => $date_record) {
				$dates[$date][$lab_record_data_yo['ZNAME']] = array();
				
				if (!isset($date_record[$lab_name]['records'])) {
					$dates[$date][$lab_name]['records'] = array();
				}
			}
		
			foreach ($labs as $lab_id => $data) {
				if (date('I')) {
					$gap = 978303600;
				} else {
					$gap = 978307200;
				}
				$this->page_data['labs'][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + $gap);
				$this->page_data['labs'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + $gap);
				$record_format_date = date('m-d-y', $data['ZDATE'] + $gap);
				$record_format_date_hours = date('d-m-Y H:i', $data['ZDATE'] + $gap);
				
				
				if ($this->page_data['filter'] == 1) {
					//**************HOURS FOR A SINGLE DAY****************
					$count = 0;
					
					foreach ($dates as $date => $date_record) {
						$parts = explode('-', $date);
						$parts2 = explode(' ', $parts[2]);
						
						if (!isset($parts2[1])) {
							$new_date_string = $parts[1] . '-' . $parts[0] . '-20' . $parts[2] . ' 00:01';
							$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts[2] . ' 00:01');
						} else {
							$new_date_string = $parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1];
							$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1]);
						}

						$lab_time = strtotime($record_format_date_hours);
	
						if (isset(array_keys($dates)[$count+1])) {
							
							$next_time = array_keys($dates)[$count+1];
							
							$x_parts = explode('-', $next_time);
							
							$x_parts2 = explode(' ', $x_parts[2]);
							
							if (!isset($x_parts2[1])) {
								$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts[2] . ' 00:01');
							} else {
								$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts2[0] . ' ' . $x_parts2[1]);
							}
							
							if ($lab_time <= $new_date && $lab_time >= $next_time) {
								$dates[$date][$lab_name]['records'][$lab_id] = $data['ZVALUE'];
							} else {
								
							}
						} else {
							
							if ($lab_time <= $new_date && $lab_time >= strtotime($new_date_string . ' - 1hour')) {
								$dates[$date][$lab_name]['records'][$lab_id] = $data['ZVALUE'];
							} else {
								
							}
						}
						
						$count++;
					}
					//**************END HOURS FOR A SINGLE DAY****************
				} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
					//**************MONTHS FOR A YEAR****************

					$count = 0;
					
					foreach ($dates as $date => $date_record) {

						$parts = explode('-', $date);
						$parts2 = explode(' ', $parts[2]);

						$new_date_string = '20' . $parts2[0] . '/' . $parts[0] . '/' . $parts[1] . ' ' . $parts2[1];
						$now_time = strtotime($new_date_string);
						$next_time = $new_date = strtotime($new_date_string . ' -1 month');
						$lab_time = strtotime($data['date']);
						
						if ($lab_time <= $now_time && $lab_time >= $next_time) {
							$dates[$date][$lab_name]['records'][$lab_id] = $data['ZVALUE'];
						} else {
							
						}
						
						$count++;
					}
					//**************END MONTHS FOR A YEAR****************
				} else {
					
					if (isset($dates[$record_format_date])) {
						$dates[$record_format_date][$lab_name]['records'][$lab_id] = $data['ZVALUE'];
					}
				}
				
			}

			foreach ($dates as $date => $date_record) {
				foreach ($date_record as $date_lab_name => $date_lab_record) {
					
					if (count($date_lab_record['records']) == 0) {
						$dates[$date][$date_lab_name]['avg'] = 0;
					} else {
						$sum = 0;

						foreach ($date_lab_record['records'] as $date_lab_id => $date_lab_record_value) {
							$sum += $date_lab_record_value;
						}
						
						$dates[$date][$date_lab_name]['avg'] = $sum / count($date_lab_record['records']);
					}
					
					if (!isset($dates[$date][$date_lab_name]['low'])) {
						$dates[$date][$date_lab_name]['low'] = $lab_info['ZRANGELOW'];
					}
					
					if (!isset($dates[$date][$date_lab_name]['high'])) {
						$dates[$date][$date_lab_name]['high'] = $lab_info['ZRANGEHIGH'];
					}
				}
			}
		}
		
		$this->page_data['dates'] = array_reverse($dates);
		
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zbloodpressure');
		
		$vital_array = explode(',', $this->input->get('vital_array'));
		$this->page_data['vital_array'] = array();
		
		foreach ($vital_array as $i => $id) {
			if ($id > 0) {
				if ($id == 1) {
					$this->page_data['vital_array'][$i]['ZNAME'] = 'sys';
				} else {
					$this->page_data['vital_array'][$i]['ZNAME'] = 'dia';
				}
			}
		}

		foreach ($vital_array as $id) {
			
			$records = $this->zbloodpressure->getUserRecords($_SESSION['user_id']);
			
			foreach ($records as $lab_id => $data) {
	
				if ($this->page_data['filter'] == 1) {
					//**************HOURS FOR A SINGLE DAY****************
					$count = 0;
					
					foreach ($dates as $date => $date_record) {
				
						$new_date = strtotime($date);
						$next_date = strtotime($date . ' - 1hour');
						$lab_time = strtotime(str_replace('-', '/', $data['date']));
						
						if ($lab_time <= $new_date && $lab_time >= $next_date) {
							if ($id == 1) {
								$dates[$date]['sys']['records'][$data['id']] = $data['ZSYSTOLIC'];
							} else {
								$dates[$date]['dia']['records'][$data['id']] = $data['ZDIASTOLIC'];
							}
						} else {
							
						}
						
						$count++;
					}
					//**************END HOURS FOR A SINGLE DAY****************
				} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
					//**************MONTHS FOR A YEAR****************
					$count = 0;
					
					foreach ($dates as $date => $date_record) {

						$new_date = strtotime($date);
						$vital_time = strtotime(str_replace('-', '/', $data['date']));
						
						if (isset(array_keys($dates)[$count+1])) {
							
							$next_time = strtotime(array_keys($dates)[$count+1]);

							if ($vital_time <= $new_date && $vital_time >= $next_time) {
								if ($id == 1) {
									$dates[$date]['sys']['records'][$data['id']] = $data['ZSYSTOLIC'];
								} else {
									$dates[$date]['dia']['records'][$data['id']] = $data['ZDIASTOLIC'];
								}
							} else {
								
							}
						} else {
							
							if ($vital_time <= $new_date && $vital_time >= strtotime($date . ' - 1hour')) {
								if ($id == 1) {
									$dates[$date]['sys']['records'][$data['id']] = $data['ZSYSTOLIC'];
								} else {
									$dates[$date]['dia']['records'][$data['id']] = $data['ZDIASTOLIC'];
								}
							} else {
								
							}
						}
						
						$count++;
					}
					//**************END MONTHS FOR A YEAR****************
				} else {
					

					$date_pieces = explode(' ', $data['date']);
					
					$record_date = str_replace('-', '/', $date_pieces[0]);

					if (isset($dates[$record_date])) {
						if ($id == 1) {
							$dates[$record_date]['sys']['records'][$data['id']] = $data['ZSYSTOLIC'];
						} else {
							$dates[$record_date]['dia']['records'][$data['id']] = $data['ZDIASTOLIC'];
						}
					}
				}
			}

			foreach ($dates as $date => $date_record) {
				foreach ($date_record as $date_vital_name => $date_vital_record) {
					
					if (count($date_vital_record['records']) == 0) {
						$dates[$date][$date_vital_name]['avg'] = 0;
					} else {
						$sum = 0;
						
						foreach ($date_vital_record['records'] as $date_lab_id => $date_lab_record_value) {
							$sum += $date_lab_record_value;
						}
						
						$dates[$date][$date_vital_name]['avg'] = $sum / count($date_vital_record['records']);
					}
				}
			}
		
		}
		
		$this->page_data['vital_dates'] = array_reverse($dates);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function meds()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('meds');
		$this->page_data['meds'] = $this->meds->getUserRecords($_SESSION['user_id']);
		
		$this->page_data['u_meds'] = array();
		
		foreach ($this->page_data['meds'] as $id => $data) {
			$this->page_data['u_meds'][$data['ZGENERIC']] = $data['Z_PK'];
		}

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function login()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$_SESSION['user_id'] = 0;

		if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$this->load->helper('url');
			redirect('http://' . $company['site'] . '/web');
		}
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function account()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['heights'] = array(
			0 => "4'0\"",
			1 => "4'1\"",
			2 => "4'2\"",
			3 => "4'3\"",
			4 => "4'4\"",
			5 => "4'5\"",
			6 => "4'6\"",
			7 => "4'7\"",
			8 => "4'8\"",
			9 => "4'9\"",
			10 => "4'10\"",
			11 => "4'11\"",
						
			12 => "5'0\"",
			13 => "5'1\"",
			14 => "5'2\"",
			15 => "5'3\"",
			16 => "5'4\"",
			17 => "5'5\"",
			18 => "5'6\"",
			19 => "5'7\"",
			20 => "5'8\"",
			21 => "5'9\"",
			22 => "5'10\"",
			23 => "5'11\"",
			
			24 => "6'0\"",
			25 => "6'1\"",
			26 => "6'2\"",
			27 => "6'3\"",
			28 => "6'4\"",
			29 => "6'5\"",
			30 => "6'6\"",
			31 => "6'7\"",
			32 => "6'8\"",
			33 => "6'9\"",
			34 => "6'10\"",
			35 => "6'11\"",
			
			36 => "7'0\"",
			37 => "7'1\"",
			38 => "7'2\"",
			39 => "7'3\"",
			40 => "7'4\"",
			41 => "7'5\"",
			42 => "7'6\"",
			43 => "7'7\"",
			44 => "7'8\"",
			45 => "7'9\"",
			46 => "7'10\"",
			47 => "7'11\""
		);
		
		$this->page_data['sexes'] = array(
			1 => "Male",
			2 => "Female"
		);
		

		
		$this->load->model('znutrientgoals');
		$this->page_data['goals'] = $this->znutrientgoals->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('zuser');
		$this->page_data['user'] = $this->zuser->getRecord($_SESSION['user_id']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function tests()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		$this->load->model('labs');
		$this->page_data['labs'] = $this->labs->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('zdatasets');
		
		$this->page_data['u_labs'] = array();
		
		foreach ($this->page_data['labs'] as $id => $data) {
			$match = $this->zdatasets->getNameMatch($data['ZNAME']);
			
			if (count($match) > 0) {
				$this->page_data['labs'][$id]['test'] = $match;
				$this->page_data['u_labs'][$data['ZNAME']] = $this->page_data['labs'][$id]['test']['ZNAME'];
			} else {
				
			}
		}

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function viewTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$test_name = $this->input->get('name');
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('labs');
		if ($this->page_data['filter'] == 5) {
			$this->page_data['labs'] = $this->labs->getUserRecordsByName($_SESSION['user_id'], $test_name, true);
		} else {
			$this->page_data['labs'] = $this->labs->getUserRecordsByName($_SESSION['user_id'], $test_name);
		}

		$dates = array();
		
		if ($this->page_data['filter'] == 1) {
			$filter_count = 24;
		} elseif ($this->page_data['filter'] == '' || $this->page_data['filter'] == 2) {
			$filter_count = 7;
		} elseif ($this->page_data['filter'] == 3) {
			$filter_count = 30;
		} elseif ($this->page_data['filter'] == 4) {
			$filter_count = 12;
		} else {
			
		}
		
		if ($this->page_data['filter'] == 1) {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'hours'))] = array(
								'tests' => array()
				);
			}
		} else if ($this->page_data['filter'] == 4) {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'months'))] = array(
								'tests' => array()
				);
			}
		} else if ($this->page_data['filter'] == 5) {

			$start = date('d-m-Y H:i', $this->page_data['labs'][0]['ZDATE'] + 978307200);
			$end = date('d-m-Y H:i', $this->page_data['labs'][count($this->page_data['labs']) - 1]['ZDATE'] + 978307200);

			$d1 = new DateTime($start);
			$d2 = new DateTime($end);

			$filter_count = ($d1->diff($d2)->m + ($d1->diff($d2)->y*12)) + 1;
			
			if ($filter_count > 4) {
				for ($i = 0; $i < $filter_count; $i++) {
					$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'months'))] = array(
									'tests' => array()
					);
				}
			} else {
				$filter_count = round($filter_count * 4.5);
				
				for ($i = 0; $i < $filter_count; $i++) {
					$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'weeks'))] = array(
						'tests' => array()
					);
				}
			}
			
		} else {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y', strtotime( '- ' . $i . 'days'))] = array(
								'tests' => array()
				);
			}
		}
		
		$this->load->model('zdatasets');		

		$this->page_data['lab_info'] = $this->zdatasets->getNameMatch($test_name);		
		
		$this->page_data['in_range'] = array();
		$this->page_data['out_range'] = array();
		
		foreach ($this->page_data['labs'] as $id => $data) {
			
			if ($data['ZNAME'] != $test_name) {
				unset($this->page_data['labs'][$id]);
			} else {
				
				if ($this->page_data['filter'] == 1) {
					//**************HOURS FOR A SINGLE DAY****************
					$this->page_data['labs'][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
					$this->page_data['labs'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);

					$count = 0; 
		
					foreach ($dates as $date => $date_record) {
						$parts = explode('-', $date);
						$parts2 = explode(' ', $parts[2]);
						
						$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1]);
						$lab_time = strtotime($this->page_data['labs'][$id]['date']);
						
						if (isset(array_keys($dates)[$count+1])) {

							$next_time = array_keys($dates)[$count+1];
							
							$x_parts = explode('-', $next_time);
							$x_parts2 = explode(' ', $x_parts[2]);
							
							$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts2[0] . ' ' . $x_parts2[1]);

							if ($lab_time <= $new_date && $lab_time >= $next_time) {
								$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
							} else {
								
							}
						} else {

							if ($lab_time <= $new_date && $lab_time >= strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1] . ' - 1hour')) {
								$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
							} else {
								
							}
						}
						
						$count++;
					}
					//**************END HOURS FOR A SINGLE DAY****************
				} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
					//**************MONTHS FOR A YEAR****************
					$this->page_data['labs'][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
					$this->page_data['labs'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
					
					$count = 0;
					
					foreach ($dates as $date => $date_record) {
						$parts = explode('-', $date);
						$parts2 = explode(' ', $parts[2]);
		
						$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1]);
						$lab_time = strtotime($this->page_data['labs'][$id]['date']);
						
						if (isset(array_keys($dates)[$count+1])) {
							
							$next_time = array_keys($dates)[$count+1];
							
							$x_parts = explode('-', $next_time);
							$x_parts2 = explode(' ', $x_parts[2]);
							
							$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts2[0] . ' ' . $x_parts2[1]);
							
							if ($lab_time <= $new_date && $lab_time >= $next_time) {
								$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
							} else {
								
							}
						} else {
							
							if ($lab_time <= $new_date && $lab_time >= strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1] . ' - 1hour')) {
								$dates[$date]['tests'][$id] = $this->page_data['labs'][$id];
							} else {
								
							}
						}
						
						$count++;
					}
					//**************END MONTHS FOR A YEAR****************
				} else {
				
					$this->page_data['labs'][$id]['date'] = date('d-m-Y h:i', $data['ZDATE'] + 978307200);
					$this->page_data['labs'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
					
					if (isset($dates[$this->page_data['labs'][$id]['format_date']])) {
						$dates[$this->page_data['labs'][$id]['format_date']]['tests'][$id] = $this->page_data['labs'][$id];
					}
				}
				
				
				if ($data['ZVALUE'] >= $this->page_data['lab_info']['ZRANGELOW'] && $data['ZVALUE'] <= $this->page_data['lab_info']['ZRANGEHIGH']) {
					$this->page_data['labs'][$id]['range'] = 1;
				} else {
					if ($data['ZVALUE'] > $this->page_data['lab_info']['ZRANGEHIGH']) {
						$this->page_data['labs'][$id]['range'] = 0;
					} else {
						if ($data['ZVALUE'] < $this->page_data['lab_info']['ZRANGELOW']) {
							$this->page_data['labs'][$id]['range'] = 0;
						} else {
							//Nothing should be here
						}
					}
				}
			}
		}
		
		foreach ($dates as $id => $data) {
			$total = 0;
			
			foreach ($data['tests'] as $lab_id => $lab_data) {
				$total += $lab_data['ZVALUE'];
			}
			
			if (count($data['tests']) == 0) {
				$dates[$id]['avg'] = 0;
			} else {
				$dates[$id]['avg'] = $total / count($data['tests']);
			}
		}
		
		$this->page_data['dates'] = array_reverse($dates);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function viewMed()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$test_name = $this->input->get('name');
		
		$this->load->model('zmedicationdata');
		$this->page_data['meds'] = $this->zmedicationdata->getUserRecordsByName($_SESSION['user_id'], $test_name);

		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->page_data['med_name'] = $test_name;
		
		$dates = array();
		
		if ($this->page_data['filter'] == 1) {
			$filter_count = 24;
		} elseif ($this->page_data['filter'] == '' || $this->page_data['filter'] == 2) {
			$filter_count = 7;
		} elseif ($this->page_data['filter'] == 3) {
			$filter_count = 30;
		} elseif ($this->page_data['filter'] == 4) {
			$filter_count = 12;
		} else {
			
		}
		
		if ($this->page_data['filter'] == 1) {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'hours'))] = array(
								'tests' => array()
				);
			}
		} else if ($this->page_data['filter'] == 4) {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'months'))] = array(
								'tests' => array()
				);
			}
		} else if ($this->page_data['filter'] == 5) {
			
			$start = date('d-m-Y H:i', $this->page_data['meds'][0]['ZDATE'] + 978307200);
			$end = date('d-m-Y H:i', $this->page_data['meds'][count($this->page_data['meds']) - 1]['ZDATE'] + 978307200);
			
			$d1 = new DateTime($start);
			$d2 = new DateTime($end);
			
			$filter_count = ($d1->diff($d2)->m + ($d1->diff($d2)->y*12)) + 1;
			
			if ($filter_count > 4) {
				for ($i = 0; $i < $filter_count; $i++) {
					$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'months'))] = array(
									'tests' => array()
					);
				}
			} else {
				$filter_count = round($filter_count * 4.5);
				
				for ($i = 0; $i < $filter_count; $i++) {
					$dates[date('m-d-y H:i', strtotime( '- ' . $i . 'weeks'))] = array(
									'tests' => array()
					);
				}
			}
			
		} else {
			for ($i = 0; $i < $filter_count; $i++) {
				$dates[date('m-d-y', strtotime( '- ' . $i . 'days'))] = array(
								'tests' => array()
				);
			}
		}

		$this->page_data['in_range'] = array();
		$this->page_data['out_range'] = array();
		
		foreach ($this->page_data['meds'] as $id => $data) {

			if ($this->page_data['filter'] == 1) {
				//**************HOURS FOR A SINGLE DAY****************
				$this->page_data['meds'][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
				$this->page_data['meds'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
				
				$count = 0;
				
				foreach ($dates as $date => $date_record) {
					$parts = explode('-', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1]);
					$med_time = strtotime($this->page_data['meds'][$id]['date']);
					
					if (isset(array_keys($dates)[$count+1])) {
						
						$next_time = array_keys($dates)[$count+1];
						
						$x_parts = explode('-', $next_time);
						$x_parts2 = explode(' ', $x_parts[2]);
						
						$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts2[0] . ' ' . $x_parts2[1]);
						
						if ($med_time <= $new_date && $med_time >= $next_time) {
							$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						} else {
							
						}
					} else {
						
						if ($med_time <= $new_date && $med_time >= strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1] . ' - 1hour')) {
							$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						} else {
							
						}
					}
					
					$count++;
				}
				//**************END HOURS FOR A SINGLE DAY****************
			} else if ($this->page_data['filter'] == 4 || $this->page_data['filter'] == 5) {
				//**************MONTHS FOR A YEAR****************
				$this->page_data['meds'][$id]['date'] = date('d-m-Y H:i', $data['ZDATE'] + 978307200);
				$this->page_data['meds'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
				
				$count = 0;
				
				foreach ($dates as $date => $date_record) {
					$parts = explode('-', $date);
					$parts2 = explode(' ', $parts[2]);
					
					$new_date = strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1]);
					$med_time = strtotime($this->page_data['meds'][$id]['date']);
					
					if (isset(array_keys($dates)[$count+1])) {
						
						$next_time = array_keys($dates)[$count+1];
						
						$x_parts = explode('-', $next_time);
						$x_parts2 = explode(' ', $x_parts[2]);
						
						$next_time = strtotime($x_parts[1] . '-' . $x_parts[0] . '-20' . $x_parts2[0] . ' ' . $x_parts2[1]);
						
						if ($med_time <= $new_date && $med_time >= $next_time) {
							$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						} else {
							
						}
					} else {
						
						if ($med_time <= $new_date && $med_time >= strtotime($parts[1] . '-' . $parts[0] . '-20' . $parts2[0] . ' ' . $parts2[1] . ' - 1hour')) {
							$dates[$date]['tests'][$id] = $this->page_data['meds'][$id];
						} else {
							
						}
					}
					
					$count++;
				}
				//**************END MONTHS FOR A YEAR****************
			} else {
				
				$this->page_data['meds'][$id]['date'] = date('d-m-Y h:i', $data['ZDATE'] + 978307200);
				$this->page_data['meds'][$id]['format_date'] = date('m-d-y', $data['ZDATE'] + 978307200);
				
				if (isset($dates[$this->page_data['meds'][$id]['format_date']])) {
					$dates[$this->page_data['meds'][$id]['format_date']]['tests'][$id] = $this->page_data['meds'][$id];
				}
			}
			
		
		}
		
		foreach ($dates as $id => $data) {
			$total = 0;
			
			foreach ($data['tests'] as $med_id => $med_data) {
				$total += $med_data['ZAMOUNT'];
			}
			
			if (count($data['tests']) == 0) {
				$dates[$id]['avg'] = 0;
			} else {
				$dates[$id]['avg'] = $total / count($data['tests']);
			}
		}
		
		$this->page_data['dates'] = array_reverse($dates);
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function viewBlood()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('zbloodpressure');
		
		$this->page_data['bp'] = $this->zbloodpressure->getUserRecords($_SESSION['user_id']);
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		// 		$status = $this->zbloodpressure->writeData(
		// 			$this->page_data['init']['db'],
		// 			0,
		// 			5,
		// 			1,
		// 			1,
		// 			strtotime(date('Y-m-d h:i:s', strtotime( '-31 years'))),
		// 			83,
		// 			122,
		// 			2
		// 		);
		
		$time = time();
		
		foreach ($this->page_data['bp'] as $i => $data) {
			$this->page_data['bp'][$i]['date'] = date('M d, Y h:i A', $data['ZDATE'] + 978307200);
			
			$this->page_data['bp'][$i]['unix'] = strtotime($this->page_data['bp'][$i]['date']);
		}
		
		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		if ($this->page_data['filter'] == 1) {
			
		} elseif ($this->page_data['filter'] == '' || $this->page_data['filter'] == 2) {
			$filter_count = 7;
		} elseif ($this->page_data['filter'] == 3) {
			$filter_count = 30;
		} elseif ($this->page_data['filter'] == 4) {
			$filter_count = 12;
		} else {
			
		}
		
		//Data for graph
		for ($i = 0; $i < $filter_count; $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' month'));
			} else {
				$first = date('Y-m-d', strtotime( '-' . $i . ' day'));
				$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->zbloodpressure->getRecordsByDateRange($_SESSION['user_id'], $first, $last);
			
			$day_sum_dia = 0;
			$day_sum_sys = 0;
			
			foreach ($records as $id => $data) {
				$day_sum_dia += $data['ZDIASTOLIC'];
				$day_sum_sys += $data['ZSYSTOLIC'];
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_dia'] = 0;
				$this->page_data['totals'][$last]['avg_sys'] = 0;
			} else {
				$day_avg_dia = $day_sum_dia / count($records);
				$day_avg_sys = $day_sum_sys / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['avg_dia'] = $day_avg_dia;
				$this->page_data['totals'][$last]['avg_sys'] = $day_avg_sys;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['bp'] = array_reverse($this->page_data['bp']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function viewTests()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zdatasets');
		$this->page_data['tests'] = $this->zdatasets->getRecords();

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function viewHeart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zheartrate');
		$this->page_data['heart'] = $this->zheartrate->getUserRecords($_SESSION['user_id']);

		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
		
		$this->page_data['filter'] = $this->input->get('filter');
		
		$this->load->model('_utility');
		$dates = $this->_utility->NEWreturnFilterDates($this->page_data['filter'], 'zheartrate');

		$this->page_data['records_found'] = 0;
		
		//Data for graph
		for ($i = 0; $i < count($dates); $i++) {
			
			if ($this->page_data['filter'] == 4) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' month'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' month'));
			} else if ($this->page_data['filter'] == 5) {
				
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' months'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' months'));
				
			} else if ($this->page_data['filter'] == 1) {
				$last = date('Y/m/d H:i:s', strtotime( '-' . $i . ' hours'));
				$first = date('Y/m/d H:i:s', strtotime( '-' . $i - 1 . ' hours'));
			} else {
				$first = date('Y/m/d', strtotime( '-' . $i . ' day'));
				$last = date('Y/m/d', strtotime( '-' . $i . ' day'));
			}
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));
			
			$date = explode('-', $last);

			//Get records for each day
			//first and last same for now

			$records = $this->zheartrate->getUserRecordsByDateRange($_SESSION['user_id'], $first, $last);

			$day_sum_rate = 0;
			
			foreach ($records as $id => $data) {
				$day_sum_rate += $data['ZRATE'];
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_rate'] = 0;
			} else {
				$day_avg_rate = $day_sum_rate / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['avg_rate'] = $day_avg_rate;
				
				$this->page_data['records_found'] = 1;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['heart'] = array_reverse($this->page_data['heart']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function db()
	{

		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		
// 		$string = file_get_contents("./uploads/drug-label-0001-of-0006.json");
// 		$json = json_decode($string, true);

		$this->load->model('zbloodpressure');
		$this->load->model('zdatasets');
		$this->load->model('zheartrate');
		$this->load->model('zlabdata');
		$this->load->model('zmedicationdata');
		$this->load->model('zmedtracking');
		$this->load->model('znixfoodaux');
		$this->load->model('znixfooddata');
		$this->load->model('zfooddata');
		$this->load->model('znutrientdatanixid');
		$this->load->model('znutrientgoals');
		$this->load->model('znutrientnamesnixid');
		$this->load->model('zproximatedatanixid');
		$this->load->model('zproximatenamesnixid');
		$this->load->model('zuser');
		$this->load->model('zsteps');
		$this->load->model('zstepgoal');
		
		$limit = 20;
						
		$this->page_data['users'] = $this->zuser->getRecords($conn);
		$goals = $this->znutrientgoals->getRecords($conn);
		$this->page_data['meds'] = $this->zmedicationdata->getRecords($limit);
		$this->page_data['med_tracks'] = $this->zmedtracking->getRecords($conn);

		$this->page_data['lab_data'] = $this->zlabdata->getRecords($conn);
		$heartrates = $this->zheartrate->getRecords($conn);
		$this->page_data['data_sets'] = $this->zdatasets->getRecords($limit);
		$this->page_data['blood'] = $this->zbloodpressure->getRecords($limit);
		$this->page_data['steps'] = $this->zsteps->getRecords($limit);
		$this->page_data['goals'] = $this->zstepgoal->getRecords($limit);
		$this->page_data['food'] = $this->zfooddata->getRecords($limit);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function steps()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zsteps');
		$this->page_data['steps'] = $this->zsteps->getUserRecords($_SESSION['user_id']);

		$day_steps = array();
		
		foreach ($this->page_data['steps'] as $id => $data) {
			$date = explode(' ', str_replace('-', '/', $data['date']));
			
			$day_steps[$date[0]][$id] = $data;
		}
		
		for ($i = 0; $i < 7; $i++) {
			$day_format = date('Y/m/d', strtotime( '- ' . $i . 'days'));
			
			$day_total = 0;

			if (isset($day_steps[$day_format])) {
				$dates[$day_format] = array(
					'steps' => $day_steps[$day_format]
				);
				
				foreach ($day_steps[$day_format] as $id => $data) {
					$day_total += $data['steps'];
				}
				
				$dates[$day_format]['day_total'] = $day_total;
			} else {
				$dates[$day_format] = array(
					'steps' => array(),
					'day_total' => 0
				);
			}
		}

		$this->page_data['dates'] = array_reverse($dates);
		
		$this->load->model('zstepgoal');
		$this->page_data['goal'] = $this->zstepgoal->getUserRecord($_SESSION['user_id']);

		if (count($this->page_data['goal']) == 0) {
			$this->page_data['goal_flag'] = 0;
			
			$next_id = $this->zstepgoal->getNextId();
			$status = $this->zstepgoal->writeData(0, $next_id, $_SESSION['user_id'], 5000);
		} else {
			$this->page_data['goal_flag'] = 1;
		}
		

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addFood()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zfooddata');
		$this->page_data['food'] = $this->zfooddata->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('znutrients');
		$this->page_data['nutrients'] = $this->znutrients->getRecords();

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addMed()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		
	
		$this->load->model('zmedicationdata');
		$this->page_data['meds'] = $this->zmedicationdata->getUserRecords($_SESSION['user_id']);
		
		$this->page_data['medications'] = array();

		foreach ($this->page_data['meds'] as $id => $data) {
			$this->page_data['medications'][$data['ZGENERIC']]['name'] = $data['ZGENERIC'];
			$this->page_data['medications'][$data['ZGENERIC']]['core_data_date'] = $data['ZDATE'];
			
			$time = strtotime(date("Y-m-d H:i:s",$data['ZDATE']) . ' +31 years');
			
			$this->page_data['medications'][$data['ZGENERIC']]['date'] = date("Y-m-d H:i:s", $time);
			$this->page_data['medications'][$data['ZGENERIC']]['dose_quantity'] = $data['ZNDOSES'];
			$this->page_data['medications'][$data['ZGENERIC']]['dose_info'] = $data['ZDOSAGEINFO'];
			$this->page_data['medications'][$data['ZGENERIC']]['Z_PK'] = $data['Z_PK'];
		}

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addBlood()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function nutrients()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function foods()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function selectTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('zlabdata');
		$this->page_data['lab_data'] = $this->zlabdata->getUserRecords($_SESSION['user_id']);
		
		$found = array();
		
		//Filter out multiples for this page
		foreach ($this->page_data['lab_data'] as $id => $data) {
			if (isset($found[$data['ZNAME']])) {
				unset($this->page_data['lab_data'][$id]);
			}
			
			$found[$data['ZNAME']] = $data['Z_PK'];
		}

		$this->load->model('zdatasets');
		
		foreach ($this->page_data['lab_data'] as $id => $data) {
			$time = strtotime(date("Y-m-d H:i:s",$data['ZDATE']) . ' +31 years');
			
			$this->page_data['lab_data'][$id]['date'] = date("Y-m-d H:i:s", $time);
			
			$this->page_data['lab_data'][$id]['data_set'] = $this->zdatasets->getNameMatch($data['ZNAME']);
		}
		
		$this->load->model('zmedicationdata');
		$this->page_data['meds'] = $this->zmedicationdata->getUserRecords($_SESSION['user_id']);

		$found = array();
		
		//Filter out multiples for this page
		foreach ($this->page_data['meds'] as $id => $data) {
			if (isset($found[$data['ZGENERIC']])) {
				unset($this->page_data['meds'][$id]);
			}
			
			$found[$data['ZGENERIC']] = $data['Z_PK'];
		}
		
		$this->load->model('zfooddata');
		$this->page_data['food'] = $this->zfooddata->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('znutrients');
		$this->page_data['list_nutrients'] = $this->znutrients->getListRecords();

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function vitals()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('zlabdata');
		$this->page_data['lab_data'] = $this->zlabdata->getUserRecords($_SESSION['user_id']);
				
		foreach ($this->page_data['lab_data'] as $id => $data) {
			$time = strtotime(date("Y-m-d H:i:s", $data['ZDATE']) . ' +31 years');
			
			$this->page_data['lab_data'][$id]['date'] = date("m-d-Y H:i:s", $time);
		}
		
		$this->load->model('zbloodpressure');
		$this->page_data['blood'] = $this->zbloodpressure->getUserRecords($_SESSION['user_id']);
		
		$this->load->model('zheartrate');
		$this->page_data['heart'] = $this->zheartrate->getUserRecords($_SESSION['user_id']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function writeBloodPressure()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		

		$this->load->model('zbloodpressure');
		
		$blood = $this->zbloodpressure->getRecords();
		
		$new_index = 0;
		
		foreach ($blood as $i => $data) {
			if ($new_index < $data['id']) {
				$new_index = $data['id'];
			}
		}
		
		$new_index += 1;
				
		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZDIASTOLIC, $ZSYSTOLIC, $ZUSERID
		$status = $this->zbloodpressure->writeData(
			0,
			$_POST['date'],
			$_POST['diastolic'],
			$_POST['systolic'],
			$_SESSION['user_id']
		);

		print $status;
		exit;
	}
	
	public function writeGoalAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$this->load->model('zstepgoal');
		$goal = $this->zstepgoal->getUserRecord($_SESSION['user_id']);
		
	//	$time = strtotime($_POST['date'] . ' -31 years');
		
		//$id, $Z_PK, $user_id, $goal
		$status = $this->zstepgoal->writeData($goal['id'], 0, $_SESSION['user_id'], $_POST['goal']);
		
		print $status;
		exit;
	}
	
	public function writeFoodAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		$this->load->model('znutrients');
		$nutrients = $this->znutrients->getRecords();

		$user_id = $_POST['user_id'];
		$quantity = $_POST['quantity'];
		$meal_type = $_POST['meal_type'];
		$food_name = $_POST['food_name'];
		$serving_qty = $_POST['serving_qty'];
		$serving_unit = $_POST['serving_unit'];
		$serving_weight_grams = $_POST['serving_weight_grams'];
		$calories = $_POST['calories'];
		$cholesterol = $_POST['cholesterol'];
		$dietary_fiber = $_POST['dietary_fiber'];
		$potassium = $_POST['potassium'];
		$protein = $_POST['protein'];
		$saturated_fat = $_POST['saturated_fat'];
		$sodium = $_POST['sodium'];
		$sugars = $_POST['sugars'];
		$total_carbohydrate = $_POST['total_carbohydrate'];
		$total_fat = $_POST['total_fat'];
		
		$date = $this->input->post('date');
		
		if ($date == '') {
			$date = date('Y-m-d H:i:s', time());
		}
		

		$this->load->model('zfooddata');
		//$id, $Z_PK, $user_id, $goal
		$status = $this->zfooddata->writeData(0, $user_id, $quantity, $food_name, $serving_qty, $serving_unit, $serving_weight_grams, $calories, $cholesterol, $dietary_fiber, $potassium, $protein, $saturated_fat, $sodium, $sugars, $total_carbohydrate, $total_fat, $meal_type, $date);
		
		print $status;
		exit;
	}
	
	public function writeMedAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();

		$user_id = $_POST['user_id'];
		$quantity = $_POST['quantity'];
		$med_name = $_POST['med_name'];
		$date = $_POST['date'];
		$med_dose = $_POST['med_dose'];
		$note = trim($_POST['note']);
		
		$dosage_ar = explode(' ', trim($med_dose));

		$value = $dosage_ar[0];
		$value = $value * $quantity;

		$this->load->model('zmedicationdata');
		$new_index = $this->zmedicationdata->getNextId();
		
		if ($date == '') {
			$time = strtotime(date('Y-m-d H:i:s') . ' -31 years');
		} else {
			$time = strtotime($_POST['date'] . ' -31 years');
		}

		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZVAL1, $ZBRAND, $ZCOMMENT, $ZDOSAGEINFO, $ZFDAID, $ZGENERIC, $ZMANUFACTURER, $ZSTR1, $ZUNITS, $ZUSERID
		$status = $this->zmedicationdata->writeData(0, $new_index, $time, $value, $quantity, 0, '', $note, $med_dose, '', $med_name, '', '', 'mg', $user_id);
		
		print $status;
		exit;
	}
	
	public function writeHeartAction() {
		$this->load->model('_preloader');
		$init = $this->_preloader->load();

		$this->load->model('zheartrate');
		//$id, $Z_PK, $date, $user_id, $steps
		$status = $this->zheartrate->writeData(0, 1, 1, $_POST['date'], $_POST['bpm'], $_POST['note'], $_SESSION['user_id']);
		
		print $status;
		exit;
	}
	
	public function writeTestData()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$this->load->model('zdatasets');
		$match = $this->zdatasets->getNameMatch($_POST['name']);
		
		if (count($match) == 0) {
			print 'NOT FOUND';
			exit;
		} else {
			$this->load->model('zlabdata');

			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
			$status = $this->zlabdata->writeData(
				0,
				1,
				1,
					$_POST['date'],
				$_POST['value'],
				$_POST['comment'],
				$_POST['name'],
				$_SESSION['user_id']
			);
			
			if ($status == 1) {
				$this->load->model('activity');
				
				if ($_POST['comment'] == '') {
					$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data added');
				} else {
					$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data added - ' . $_POST['comment']);
				}
			}
			
			print $status;
			exit;
		}
	}
	
	public function updateLabAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		

		$this->load->model('zlabdata');
		
		$time = strtotime($_POST['date'] . ' -31 years');
		
		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
		$status = $this->zlabdata->writeData(
			$_POST['id'],
			$_POST['id'],
			1,
			1,
			$time,
			$_POST['value'],
			$_POST['comment'],
			'',
			$_SESSION['user_id']
		);
		
		if ($status == 1) {
			$this->load->model('activity');
			
			if ($_POST['comment'] == '') {
				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data updated');
			} else {
				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data updated - ' . $_POST['comment']);
			}
		}
		
		print $status;
		exit;
		
	}
	
	public function updateMedAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		$time = strtotime($_POST['date'] . ' -31 years');

		$dosage_ar = explode(' ', trim($_POST['dosage']));
		$value = $dosage_ar[0];
		$value = $value * $_POST['quantity'];

		$this->load->model('zmedicationdata');
		
		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
		$status = $this->zmedicationdata->updateData($_POST['id'], $time, $value, $_POST['quantity'], $_POST['comment']);
		
		if ($status == 1) {
			$this->load->model('activity');
			
			if ($_POST['comment'] == '') {
				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Medication record updated');
			} else {
				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Medication record updated - ' . $_POST['comment']);
			}
		}
		
		print $status;
		exit;
		
	}
	
	public function updateUserAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		

		$this->load->model('zuser');
		$this->load->model('znutrientgoals');
	
		$goals = $this->znutrientgoals->getUserRecords($_POST['user_id']);
		
		foreach ($goals as $id => $data) {

			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
			$status = $this->znutrientgoals->writeData(0, $data['Z_PK'], 1, 1, $_POST[$id], $data['ZCOMMON'], $data['ZNAME'], $data['ZUNIT'], $data['ZUSERID']);
		}
		
		$user = $this->zuser->getRecord($_POST['user_id']);
		
		$status = $this->zuser->writeData(0, $user['Z_PK'], $user['ZFIRSTNAME'], $user['ZLASTNAME'], $user['ZEMAIL'], $user['ZPASSWORD'], $_POST['age'], $_POST['sex'], $_POST['height'], $_POST['weight']);
		
// 		if ($status == 1) {
// 			$this->load->model('activity');
			
// 			if ($_POST['comment'] == '') {
// 				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data updated');
// 			} else {
// 				$status = $this->activity->writeData(0, $_SESSION['user_id'], 'Lab data updated - ' . $_POST['comment']);
// 			}
// 		}
		
		print $status;
		exit;
		
	}
	
	public function deleteLabAction()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$this->load->model('zlabdata');
				
		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
		$status = $this->zlabdata->delete($conn,$_POST['id']);
		
		print $status;
		exit;
		
	}
	
	public function writeDataSet()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$this->load->model('zdatasets');
		$match = $this->zdatasets->getNameMatch($_POST['name']);
		
		if ($_POST['id'] != '') {
			$id = $_POST['id'];
		} else {
			$id = 0;
		}
		
		if ($id > 0) {
			$sets = $this->zdatasets->getRecords(10);
			
			$new_index = 0;
			
			foreach ($sets as $i => $data) {
				if ($new_index < $data['Z_PK']) {
					$new_index = $data['Z_PK'];
				}
			}
			
			$new_index += 1;
			
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZRANGEHIGH, $ZRANGELOW, $ZCOMMENT, $ZDESC, $ZNAME, $ZTAGS, $ZUNITS, $ZUSERID
			$status = $this->zdatasets->writeData(
					$conn,
					$id,
					$new_index,
					1,
					1,
					$_POST['high'],
					$_POST['low'],
					$_POST['comment'],
					$_POST['name'],
					$_POST['description'],
					$_POST['tags'],
					$_POST['units'],
					$_SESSION['user_id']
					);
			
			print $status;
			exit;
		} else {
			if (count($match) == 0) {
				
				$sets = $this->zdatasets->getRecords(10);
				
				$new_index = 0;
				
				foreach ($sets as $i => $data) {
					if ($new_index < $data['Z_PK']) {
						$new_index = $data['Z_PK'];
					}
				}
				
				$new_index += 1;
				
				//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZRANGEHIGH, $ZRANGELOW, $ZCOMMENT, $ZDESC, $ZNAME, $ZTAGS, $ZUNITS, $ZUSERID
				$status = $this->zdatasets->writeData(
						$conn,
						$id,
						$new_index,
						1,
						1,
						$_POST['high'],
						$_POST['low'],
						$_POST['comment'],
						$_POST['description'],
						$_POST['name'],
						$_POST['tags'],
						$_POST['units'],
						$_SESSION['user_id']
						);
				
				print $status;
				exit;
				
			} else {
				print 'FOUND';
				exit;
			}
		}
		
	}
	
	public function deleteRecord()
	{
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$model_name = strtolower($this->input->post('table'));
		$table_name = $this->input->post('table');
		$id = $this->input->post('id');
		
		$sql = "DELETE FROM `" . $table_name . "` WHERE Z_PK = " . $id;

		$result = $conn->query($sql);
		
		if ($result == '') {
			print '<pre>';
			print_r($conn);
			print '</pre>';
			exit;
		}
		
		print $result;
		exit;
	}
	
	public function writeMedRecord() {
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		
		$this->load->model('zmedicationdata');
		
		$blood = $this->zmedicationdata->getRecords($conn);
		
		$new_index = 0;
		
		foreach ($blood as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$dose_parts = explode(' ', $_POST['dose']);

		$time = strtotime($_POST['date'] . ' -31 years');
				
		//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZVAL1, $ZBRAND, $ZCOMMENT, $ZDOSAGEINFO, $ZFDAID, $ZGENERIC, $ZMANUFACTURER, $ZSTR1, $ZUNITS, $ZUSERID
		$status = $this->zmedicationdata->writeData(
			$conn,
			0,
			$new_index,
			1,
			1,
			$time,
			
			//Amount
			$dose_parts[0],
			$_POST['quantity'],
			0,
			'',
			$_POST['comment'],
			$_POST['dose'],
			'',
			$_POST['med_name'],
			'',
			'',
			$dose_parts[1],
			$_SESSION['user_id']
		);
		
		print $status;
		exit;
	}
	
	public function addHeart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addLab()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('zdatasets');
		$this->page_data['sets'] = $this->zdatasets->getRecords();
		$this->page_data['sets_sort'] = $this->zdatasets->getRecordsByName();

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addStep()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function addLabTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		$this->page_data['name'] = $this->input->get('name');
		$this->page_data['id'] = $this->input->get('id');
		
		if ($this->page_data['id'] != '') {
			$this->load->model('zdatasets');
			$this->page_data['test'] = $this->zdatasets->getRecord($this->page_data['id']);
		}

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		
		
		$this->load->model('zbloodpressure');
		$this->load->model('zdatasets');
		$this->load->model('zheartrate');
		$this->load->model('zlabdata');
		$this->load->model('zmedicationdata');
		$this->load->model('zmedtracking');
		$this->load->model('znixfoodaux');
		$this->load->model('znixfooddata');
		$this->load->model('znutrientdatanixid');
		$this->load->model('znutrientgoals');
		$this->load->model('znutrientnamesnixid');
		$this->load->model('zproximatedatanixid');
		$this->load->model('zproximatenamesnixid');
		$this->load->model('znutrientdefs');
		$this->load->model('zuser');
		
// 		$this->znutrientdefs->writeData(203,"PROCNT","Protein","g");
// 		$this->znutrientdefs->writeData(204,"FAT,Total lipid (fat)","g");
// 		$this->znutrientdefs->writeData(205","CHOCDF",""Carbohydrate"," by difference"","g");
// 				$this->znutrientdefs->writeData(207","ASH","Ash","g");
// 		$this->znutrientdefs->writeData(208","ENERC_KCAL","Energy","kcal");
// 				$this->znutrientdefs->writeData(209","STARCH","Starch","g");
// 		$this->znutrientdefs->writeData(210","SUCS","Sucrose","g");
// 				$this->znutrientdefs->writeData(211","GLUS","Glucose (dextrose)","g");
// 		$this->znutrientdefs->writeData(212","FRUS","Fructose","g");
// 				$this->znutrientdefs->writeData(213","LACS","Lactose","g");
// 		$this->znutrientdefs->writeData(214","MALS","Maltose","g");
// 				$this->znutrientdefs->writeData(221","ALC",""Alcohol"," ethyl"","g");
// 		$this->znutrientdefs->writeData(255","WATER","Water","g");
// 				$this->znutrientdefs->writeData(257","NULL","Adjusted Protein","g");
// 		$this->znutrientdefs->writeData(262","CAFFN","Caffeine","mg");
// 				$this->znutrientdefs->writeData(263","THEBRN","Theobromine","mg");
// 		$this->znutrientdefs->writeData(268","ENERC_KJ","Energy","kJ");
// 				$this->znutrientdefs->writeData(269","SUGAR",""Sugars"," total"","g");
// 		$this->znutrientdefs->writeData(287","GALS","Galactose","g");
// 				$this->znutrientdefs->writeData(291","FIBTG",""Fiber"," total dietary"","g");
// 		$this->znutrientdefs->writeData(301","CA",""Calcium"," Ca"","mg");
// 				$this->znutrientdefs->writeData(303","FE",""Iron"," Fe"","mg");
// 		$this->znutrientdefs->writeData(304","MG",""Magnesium"," Mg"","mg");
// 				$this->znutrientdefs->writeData(305","P",""Phosphorus"," P"","mg");
// 		$this->znutrientdefs->writeData(306","K",""Potassium"," K"","mg");
// 				$this->znutrientdefs->writeData(307","NA",""Sodium"," Na"","mg");
// 		$this->znutrientdefs->writeData(309","ZN",""Zinc"," Zn"","mg");
// 				$this->znutrientdefs->writeData(312","CU",""Copper"," Cu"","mg");
// 		$this->znutrientdefs->writeData(313","FLD",""Fluoride"," F"","µg");
// 				$this->znutrientdefs->writeData(315","MN",""Manganese"," Mn"","mg");
// 		$this->znutrientdefs->writeData(317","SE",""Selenium"," Se"","µg");
// 				$this->znutrientdefs->writeData(318","VITA_IU",""Vitamin A"," IU"","IU");
// 		$this->znutrientdefs->writeData(319","RETOL","Retinol","µg");
// 				$this->znutrientdefs->writeData(320","VITA_RAE",""Vitamin A"," RAE"","µg");
// 		$this->znutrientdefs->writeData(321","CARTB",""Carotene"," beta"","µg");
// 				$this->znutrientdefs->writeData(322","CARTA",""Carotene"," alpha"","µg");
// 		$this->znutrientdefs->writeData(323","TOCPHA","Vitamin E (alpha-tocopherol)","mg");
// 				$this->znutrientdefs->writeData(324","VITD","Vitamin D","IU");
// 		$this->znutrientdefs->writeData(325","ERGCAL","Vitamin D2 (ergocalciferol)","µg");
// 				$this->znutrientdefs->writeData(326","CHOCAL","Vitamin D3 (cholecalciferol)","µg");
// 		$this->znutrientdefs->writeData(328","VITD","Vitamin D (D2 + D3)","µg");
// 				$this->znutrientdefs->writeData(334","CRYPX",""Cryptoxanthin"," beta"","µg");
// 		$this->znutrientdefs->writeData(337","LYCPN","Lycopene","µg");
// 				$this->znutrientdefs->writeData(338","LUT+ZEA","Lutein + zeaxanthin","µg");
// 		$this->znutrientdefs->writeData(341","TOCPHB",""Tocopherol"," beta"","mg");
// 				$this->znutrientdefs->writeData(342","TOCPHG",""Tocopherol"," gamma"","mg");
// 		$this->znutrientdefs->writeData(343","TOCPHD",""Tocopherol"," delta"","mg");
// 				$this->znutrientdefs->writeData(401","VITC",""Vitamin C"," total ascorbic acid"","mg");
// 		$this->znutrientdefs->writeData(404","THIA","Thiamin","mg");
// 				$this->znutrientdefs->writeData(405","RIBF","Riboflavin","mg");
// 		$this->znutrientdefs->writeData(406","NIA","Niacin","mg");
// 				$this->znutrientdefs->writeData(410","PANTAC","Pantothenic acid","mg");
// 		$this->znutrientdefs->writeData(415","VITB6A","Vitamin B-6","mg");
// 				$this->znutrientdefs->writeData(417","FOL",""Folate"," total"","µg");
// 		$this->znutrientdefs->writeData(418","VITB12","Vitamin B-12","µg");
// 				$this->znutrientdefs->writeData(421","CHOLN",""Choline"," total"","mg");
// 		$this->znutrientdefs->writeData(428","MK4","Menaquinone-4","µg");
// 				$this->znutrientdefs->writeData(429","VITK1D","Dihydrophylloquinone","µg");
// 		$this->znutrientdefs->writeData(430","VITK1","Vitamin K (phylloquinone)","µg");
// 				$this->znutrientdefs->writeData(431","FOLAC","Folic acid","µg");
// 		$this->znutrientdefs->writeData(432","FOLFD",""Folate"," food"","µg");
// 				$this->znutrientdefs->writeData(435","FOLDFE",""Folate"," DFE"","µg");
// 		$this->znutrientdefs->writeData(454","BETN","Betaine","mg");
// 				$this->znutrientdefs->writeData(501","TRP_G","Tryptophan","g");
// 		$this->znutrientdefs->writeData(502","THR_G","Threonine","g");
// 				$this->znutrientdefs->writeData(503","ILE_G","Isoleucine","g");
// 		$this->znutrientdefs->writeData(504","LEU_G","Leucine","g");
// 				$this->znutrientdefs->writeData(505","LYS_G","Lysine","g");
// 		$this->znutrientdefs->writeData(506","MET_G","Methionine","g");
// 				$this->znutrientdefs->writeData(507","CYS_G","Cystine","g");
// 		$this->znutrientdefs->writeData(508","PHE_G","Phenylalanine","g");
// 				$this->znutrientdefs->writeData(509","TYR_G","Tyrosine","g");
// 		$this->znutrientdefs->writeData(510","VAL_G","Valine","g");
// 				$this->znutrientdefs->writeData(511","ARG_G","Arginine","g");
// 		$this->znutrientdefs->writeData(512","HISTN_G","Histidine","g");
// 				$this->znutrientdefs->writeData(513","ALA_G","Alanine","g");
// 		$this->znutrientdefs->writeData(514","ASP_G","Aspartic acid","g");
// 				$this->znutrientdefs->writeData(515","GLU_G","Glutamic acid","g");
// 		$this->znutrientdefs->writeData(516","GLY_G","Glycine","g");
// 				$this->znutrientdefs->writeData(517","PRO_G","Proline","g");
// 		$this->znutrientdefs->writeData(518","SER_G","Serine","g");
// 				$this->znutrientdefs->writeData(521","HYP","Hydroxyproline","g");
// 		$this->znutrientdefs->writeData(573","NULL",""Vitamin E"," added"","mg");
// 				$this->znutrientdefs->writeData(578","NULL",""Vitamin B-12"," added"","µg");
// 		$this->znutrientdefs->writeData(601","CHOLE","Cholesterol","mg");
// 				$this->znutrientdefs->writeData(605","FATRN",""Fatty acids"," total trans"","g");
// 		$this->znutrientdefs->writeData(606","FASAT",""Fatty acids"," total saturated"","g");
// 				$this->znutrientdefs->writeData(607","F4D0","4:00","g");
// 		$this->znutrientdefs->writeData(608","F6D0","6:00","g");
// 				$this->znutrientdefs->writeData(609","F8D0","8:00","g");
// 		$this->znutrientdefs->writeData(610","F10D0","10:00","g");
// 				$this->znutrientdefs->writeData(611","F12D0","12:00","g");
// 		$this->znutrientdefs->writeData(612","F14D0","14:00","g");
// 				$this->znutrientdefs->writeData(613","F16D0","16:00","g");
// 		$this->znutrientdefs->writeData(614","F18D0","18:00","g");
// 				$this->znutrientdefs->writeData(615","F20D0","20:00","g");
// 		$this->znutrientdefs->writeData(617","F18D1","18:1 undifferentiated","g");
// 				$this->znutrientdefs->writeData(618","F18D2","18:2 undifferentiated","g");
// 		$this->znutrientdefs->writeData(619","F18D3","18:3 undifferentiated","g");
// 				$this->znutrientdefs->writeData(620","F20D4","20:4 undifferentiated","g");
// 		$this->znutrientdefs->writeData(621","F22D6","22:6 n-3 (DHA)","g","omega-3 docosahexaenoic aci);
// 		$this->znutrientdefs->writeData(624","F22D0","22:00","g");
// 				$this->znutrientdefs->writeData(625","F14D1","14:01","g");
// 		$this->znutrientdefs->writeData(626","F16D1","16:1 undifferentiated","g");
// 				$this->znutrientdefs->writeData(627","F18D4","18:04","g");
// 		$this->znutrientdefs->writeData(628","F20D1","20:01","g");
// 				$this->znutrientdefs->writeData(629","F20D5","20:5 n-3 (EPA)","g","omega-3 eicosapentaenoic ac);
// 				$this->znutrientdefs->writeData(630","F22D1","22:1 undifferentiated","g");
// 		$this->znutrientdefs->writeData(631","F22D5","22:5 n-3 (DPA)","g");
// 				$this->znutrientdefs->writeData(636","PHYSTR","Phytosterols","mg");
// 		$this->znutrientdefs->writeData(638","STID7","Stigmasterol","mg");
// 				$this->znutrientdefs->writeData(639","CAMD5","Campesterol","mg");
// 		$this->znutrientdefs->writeData(641","SITSTR","Beta-sitosterol","mg");
// 				$this->znutrientdefs->writeData(645","FAMS",""Fatty acids"," total monounsaturated"","g");
// 		$this->znutrientdefs->writeData(646","FAPU",""Fatty acids"," total polyunsaturated"","g");
// 				$this->znutrientdefs->writeData(652","F15D0","15:00","g");
// 		$this->znutrientdefs->writeData(653","F17D0","17:00","g");
// 				$this->znutrientdefs->writeData(654","F24D0","24:00:00","g");
// 		$this->znutrientdefs->writeData(662","F16D1T","16:1 t","g");
// 				$this->znutrientdefs->writeData(663","F18D1T","18:1 t","g");
// 		$this->znutrientdefs->writeData(664","NULL","22:1 t","g");
// 				$this->znutrientdefs->writeData(665","NULL","18:2 t not further defined","g");
// 		$this->znutrientdefs->writeData(666","NULL","18:2 i","g");
// 				$this->znutrientdefs->writeData(669","F18D2TT",""18:2 t","t"","g");
// 		$this->znutrientdefs->writeData(670","F18D2CLA","18:2 CLAs","g");
// 				$this->znutrientdefs->writeData(671","F24D1C","24:1 c","g");
// 		$this->znutrientdefs->writeData(672","F20D2CN6",""20:2 n-6 c","c"","g");
// 				$this->znutrientdefs->writeData(673","F16D1C","16:1 c","g");
// 		$this->znutrientdefs->writeData(674","F18D1C","18:1 c","g");
// 				$this->znutrientdefs->writeData(675","F18D2CN6",""18:2 n-6 c","c"","g");
// 		$this->znutrientdefs->writeData(676","NULL","22:1 c","g");
// 				$this->znutrientdefs->writeData(685","F18D3CN6",""18:3 n-6 c","c","c"","g");
// 		$this->znutrientdefs->writeData(687","F17D1","17:01","g");
// 				$this->znutrientdefs->writeData(689","F20D3","20:3 undifferentiated","g");
// 		$this->znutrientdefs->writeData(693","FATRNM",""Fatty acids"," total trans-monoenoic"","g");
// 				$this->znutrientdefs->writeData(695","FATRNP",""Fatty acids"," total trans-polyenoic"","g");
// 		$this->znutrientdefs->writeData(696","F13D0","13:00","g");
// 				$this->znutrientdefs->writeData(697","F15D1","15:01","g");
// 		$this->znutrientdefs->writeData(851","F18D3CN3",""18:3 n-3 c","c","c (ALA)"","g","omega-3 alpha-linolenic ac);
// 		$this->znutrientdefs->writeData(852","F20D3N3","20:3 n-3","g");
// 				$this->znutrientdefs->writeData(853","F20D3N6","20:3 n-6","g");
// 		$this->znutrientdefs->writeData(855","F20D4N6","20:4 n-6","g");
// 				$this->znutrientdefs->writeData(856","NULL","18:3i","g");
// 		$this->znutrientdefs->writeData(857","F21D5","21:05","g");
// 				$this->znutrientdefs->writeData(858","F22D4","22:04","g");
// 		$this->znutrientdefs->writeData(859","F18D1TN7","18:1-11t (18:1t n-7)","g");
		
		for ($z = 0; $z < 1; $z++) {
			
		
		//****************START CREATE NEW USER METHOD*************************
		
		$users = $this->zuser->getRecords($conn);
		$goals = $this->znutrientgoals->getRecords($conn);
		$meds = $this->zmedicationdata->getRecords($conn);
		$lab_data = $this->zlabdata->getRecords($conn);
		$heartrates = $this->zheartrate->getRecords($conn);
		$blood = $this->zbloodpressure->getRecords($conn);

		//****************NUTRIENT GOALS*************************
		
		$user_id_to_set = 0;
		
		$names = array(
			0 => 'Noah',
			1 => 'Liam',
			2 => 'Mason',
			3 => 'Jacob',
			4 => 'William',
			5 => 'Ethan',
			6 => 'James',
			7 => 'Alexander',
			8 => 'Michael',
			9 => 'Benjamin',
			10 => 'Elijah',
			11 => 'Daniel',
			12 => 'Aiden',
			13 => 'Logan',
			14 => 'Matthew',
			15 => 'Lucas',
			16 => 'Jackson',
			17 => 'David',
			18 => 'Oliver',
			19 => 'Jayden',
			20 => 'Joseph',
			21 => 'Gabriel',
			22 => 'Samuel',
			23 => 'Carter',
			24 => 'Anthony',
			25 => 'John',
			26 => 'Dylan',
			27 => 'Luke',
			28 => 'Henry',
			29 => 'Andrew',
			30 => 'Isaac',
			31 => 'Christopher',
			32 => 'Joshua',
			33 => 'Wyatt',
			34 => 'Sebastian',
			35 => 'Owen',
			36 => 'Caleb',
			37 => 'Nathan',
			38 => 'Ryan',
			39 => 'Jack',
			40 => 'Hunter',
			41 => 'Levi',
			42 => 'Christian',
			43 => 'Jaxon',
			44 => 'Julian',
			45 => 'Landon',
			46 => 'Grayson',
			47 => 'Jonathan',
			48 => 'Isaiah',
			49 => 'Charles',
			50 => 'Thomas',
			51 => 'Emma',
			52 => 'Olivia',
			53 => 'Sophia',
			54 => 'Ava',
			55 => 'Isabella',
			56 => 'Mia',
			57 => 'Abigail',
			58 => 'Emily',
			59 => 'Charlotte',
			60 => 'Harper',
			61 => 'Madison',
			62 => 'Amelia',
			63 => 'Elizabeth',
			64 => 'Sofia',
			65 => 'Evelyn',
			66 => 'Avery',
			67 => 'Chloe',
			68 => 'Ella',
			69 => 'Grace',
			70 => 'Victoria',
			71 => 'Aubrey',
			72 => 'Scarlett',
			73 => 'Zoey',
			74 => 'Addison',
			75 => 'Lily',
			76 => 'Lillian',
			77 => 'Natalie',
			78 => 'Hannah',
			79 => 'Aria',
			80 => 'Layla',
			81 => 'Brooklyn',
			82 => 'Alexa',
			83 => 'Zoe',
			84 => 'Penelope',
			85 => 'Riley',
			86 => 'Leah',
			87 => 'Audrey',
			88 => 'Savannah',
			89 => 'Allison',
			90 => 'Samantha',
			91 => 'Nora',
			92 => 'Skylar',
			93 => 'Camila',
			94 => 'Anna',
			95 => 'Paisley',
			96 => 'Ariana',
			97 => 'Ellie',
			98 => 'Aaliyah',
			99 => 'Claire',
			100 => 'Violet',
			101 => 'Jordan',
			102 => 'Brayden',
			103 => 'Nicholas',
			104 => 'Robert',
			105 => 'Angel',
			106 => 'Hudson',
			107 => 'Lincoln',
			108 => 'Evan',
			109 => 'Dominic',
			110 => 'Austin',
			111 => 'Gavin',
			112 => 'Nolan',
			113 => 'Parker',
			114 => 'Adam',
			115 => 'Chase',
			116 => 'Jace',
			117 => 'Ian',
			118 => 'Cooper',
			119 => 'Easton',
			120 => 'Kevin',
			121 => 'Jose',
			122 => 'Tyler',
			123 => 'Brandon',
			124 => 'Asher',
			125 => 'Jaxson',
			126 => 'Mateo',
			127 => 'Jason',
			128 => 'Ayden',
			129 => 'Zachary',
			130 => 'Carson',
			131 => 'Xavier',
			132 => 'Leo',
			133 => 'Ezra',
			134 => 'Bentley',
			135 => 'Sawyer',
			136 => 'Kayden',
			137 => 'Blake',
			138 => 'Nathaniel'
		);
		
		$last_names = array(
			0 => 'Ahart',
			1 => 'Smith',
			2 => 'Johnson',
			3 => 'Williams',
			4 => 'Jones',
			5 => 'Brown',
			6 => 'Davis',
			7 => 'Miller',
			8 => 'Wilson',
			9 => 'Moore',
			10 => 'Taylor',
			11 => 'Anderson',
			12 => 'Thomas',
			13 => 'Jackson',
			14 => 'White',
			15 => 'Harris',
			16 => 'Martin',
			17 => 'Thompson',
			18 => 'Garcia',
			19 => 'Martinez',
			20 => 'Robinson',
			21 => 'Clark',
			22 => 'Rodriguez',
			23 => 'Lewis',
			24 => 'Lee',
			25 => 'Walker',
			26 => 'Hall',
			27 => 'Allen',
			28 => 'Young',
			29 => 'Hernandez',
			30 => 'King',
			31 => 'Wright',
			32 => 'Lopez',
			33 => 'Hill',
			34 => 'Scott',
			35 => 'Green',
			36 => 'Adams',
			37 => 'Baker',
			38 => 'Gonzalez',
			39 => 'Nelson',
			40 => 'Carter',
			41 => 'Mitchell',
			42 => 'Perez',
			43 => 'Roberts',
			44 => 'Turner',
			45 => 'Phillips',
			46 => 'Campbell',
			47 => 'Parker',
			48 => 'Evans',
			49 => 'Edwards',
			50 => 'Collins',
			51 => 'Stewart',
			52 => 'Sanchez',
			53 => 'Morris',
			54 => 'Rogers',
			55 => 'Reed',
			56 => 'Cook',
			57 => 'Morgan',
			58 => 'Bell',
			59 => 'Murphy',
			60 => 'Bailey',
			61 => 'Rivera',
			62 => 'Cooper',
			63 => 'Richardson',
			64 => 'Cox',
			65 => 'Howard',
			66 => 'Ward',
			67 => 'Torres',
			68 => 'Peterson',
			69 => 'Gray',
			70 => 'Ramirez',
			71 => 'James',
			72 => 'Watson',
			73 => 'Brooks',
			74 => 'Kelly',
			75 => 'Sanders',
			76 => 'Price',
			77 => 'Bennett',
			78 => 'Wood',
			79 => 'Barnes',
			80 => 'Ross',
			81 => 'Henderson',
			82 => 'Coleman',
			83 => 'Jenkins',
			84 => 'Perry',
			85 => 'Powell',
			86 => 'Long',
			87 => 'Patterson',
			88 => 'Hughes',
			89 => 'Flores',
			90 => 'Washington',
			91 => 'Butler',
			92 => 'Simmons',
			93 => 'Foster',
			94 => 'Gonzales',
			95 => 'Bryant',
			96 => 'Alexander',
			97 => 'Russell',
			98 => 'Griffin',
			99 => 'Diaz',
			100 => 'Hayes',
		);
		
		foreach ($users as $i => $data) {
			if ($user_id_to_set < $data['Z_PK']) {
				$user_id_to_set = $data['Z_PK'];
			}
		}
		
		$user_id_to_set += 1;
		
		$rand2 = rand(0, 100);
		$rand1 = rand(0, 138);
		
		//$id, $Z_PK, $ZFIRSTNAME, $ZLASTNAME, $ZEMAIL, $ZPASSWORD
		$status = $this->zuser->writeData(
			$conn,
			0,
			$user_id_to_set,
			ucfirst($names[$rand1]),
			ucfirst($last_names[$rand2]),
			strtolower(substr($names[$rand1], 0, 1)) . strtolower($last_names[$rand2]) . 'ABTECH@gmail.com',
			'test'
		);
		
		if ($status != 1) {
			print '<pre>';
			print_r('Failed at User');
			print '</pre>';
			exit;
		}
		
		//****************NUTRIENT GOALS*************************
		
		
		$new_index = 0;
		
		foreach ($goals as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$goal_array = array(
			0 => array(
				'ZCOMMON' => 'Carbohydrate',
				'ZNAME' => 'Carbohydrate',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(250, 350)
			),
			1 => array(
				'ZCOMMON' => 'Protein',
				'ZNAME' => 'Protein',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(50, 115)
			),
			2 => array(
				'ZCOMMON' => 'Calories',
				'ZNAME' => 'Energy',
				'ZUNIT' => 'Cals',
				'ZVALUE' => rand(1500, 3200)
			),
			3 => array(
				'ZCOMMON' => 'Fiber',
				'ZNAME' => 'Fiber, total dietary',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(15, 32)
			),
			4 => array(
				'ZCOMMON' => 'Sugar',
				'ZNAME' => 'Sugars, total',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(5, 42)
			),
			5 => array(
				'ZCOMMON' => 'Sodium',
				'ZNAME' => 'Sodium, Na',
				'ZUNIT' => 'mg',
				'ZVALUE' => rand(1150, 1400)
			),
			6 => array(
				'ZCOMMON' => 'Fat',
				'ZNAME' => 'Total lipid (fat)',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(48, 89)
			),
			7 => array(
				'ZCOMMON' => 'Cholesterol',
				'ZNAME' => 'Cholesterol',
				'ZUNIT' => 'mg',
				'ZVALUE' => rand(220, 470)
			)
		);
		
		for ($j = 0; $j < 8; $j++) {
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZVALUE, $ZCOMMON, $ZNAME, $ZUNIT, $ZUSERID
			$status = $this->znutrientgoals->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				$goal_array[$j]['ZVALUE'],
				$goal_array[$j]['ZCOMMON'],
				$goal_array[$j]['ZNAME'],
				$goal_array[$j]['ZUNIT'],
				$user_id_to_set
			);
			
			$new_index += 1;
		}
		
		if ($status != 1) {
			print '<pre>';
			print_r('Failed at Nutrient Goals');
			print '</pre>';
			exit;
		}
		
		//****************FOOD DATA*************************
		
		$food_aux = $this->znutrientdatanixid->getRecords($conn);
		$new_index = 0;

		
		//****************FOOD DATA*************************
		
		$food_aux = $this->znixfooddata->getRecords($conn);
		$new_index = 0;

		
		//****************MED TRACKING*************************
		
		$med_tracks = $this->zmedtracking->getRecords($conn);
		$new_index = 0;
		
		
		//****************MEDICATION DATA*************************
		
		
		$new_index = 0;
		
		foreach ($meds as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$test_array = array(
			0 => 'Acetaminophen',
			1 => 'Adderall',
			2 => 'Alprazolam',
			3 => 'Amitriptyline',
			4 => 'Amlodipine',
			5 => 'Amoxicillin',
			6 => 'Ativan',
			7 => 'Atorvastatin',
			8 => 'Azithromycin',
			9 => 'Ciprofloxacin',
			10 => 'Citalopram',
			11 => 'Clindamycin',
			12 => 'Clonazepam',
			13 => 'Codeine',
			14 => 'Cyclobenzaprine',
			15 => 'Cymbalta',
			16 => 'Doxycycline',
			17 => 'Gabapentin',
			18 => 'Hydrochlorothiazide',
			19 => 'Ibuprofen',
			20 => 'Lexapro',
			21 => 'Lisinopril',
			22 => 'Loratadine',
			23 => 'Lorazepam',
			24 => 'Losartan',
			25 => 'Lyrica',
			26 => 'Meloxicam',
			27 => 'Metformin',
			28 => 'Metoprolol',
			29 => 'Naproxen',
			30 => 'Omeprazole',
			31 => 'Oxycodone',
			32 => 'Pantoprazole',
			33 => 'Prednisone',
			34 => 'Tramadol',
			35 => 'Trazodone',
			36 => 'Viagra',
			37 => 'Wellbutrin',
			38 => 'Xanax',
			39 => 'Zoloft'
		);
		
		$comment_array = array(
			0 => 'From tracking',
			1 => '',
			2 => 'Test comment'
		);
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 6000);
			$rand2 = rand(0, 39);
			$rand3 = round((rand(25, 150)+5/2)/5)*5;
			$rand4 = rand(0, 3);
			$rand5 = rand(0, 2);
			
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZVAL1, $ZBRAND, $ZCOMMENT, $ZDOSAGEINFO, $ZFDAID, $ZGENERIC, $ZMANUFACTURER, $ZSTR1, $ZUNITS, $ZUSERID
			$status = $this->zmedicationdata->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				
				//Amount
				$rand3,
				$rand4,
				0,
				'',
				$comment_array[$rand5],
				$rand3 . ' MG Tablet',
				'',
				$test_array[$rand2],
				'',
				'',
				'mg', 
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Medication Data');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}
		
		
	
		
		//****************LAB DATA*************************
		
		$new_index = 0;
		
		foreach ($lab_data as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$test_array = array(
			0 => 'Abdominoplasty (Tummy Tuck ( Abdominoplasty))',
			1 => 'Ablation Therapy for Arrhythmias',
			2 => 'Ablation, Endometrial (Endometrial Ablation)',
			3 => 'Ablation, Uterus (Endometrial Ablation)',
			4 => 'Abnormal Liver Enzymes (Liver Blood Tests)',
			5 => 'Absorbent Products Incontinence (Urinary Incontinence Products for Men)',
			6 => 'Abstinence Method of Birth Control (Natural Methods of Birth Control)',
			7 => 'Acupuncture',
			8 => 'Adenoidectomy Surgical Instructions',
			9 => 'Adenosine (Exercise Stress Test)',
			10 => 'Adenosine Stress Test For Heart Disease (Coronary Artery Disease Screening Tests (CAD))',
			11 => 'AFP Blood Test (Alpha-fetoprotein Blood Test)',
			12 => 'Alexander Technique for Childbirth (Childbirth Class Options)',
			13 => 'ALK (Keratoplasty Eye Surgery (ALK))',
			14 => 'Allergy Shots',
			15 => 'Allergy, Skin Test (Skin Test For Allergy)',
			16 => 'Alpha-fetoprotein Blood Test',
			17 => 'ALT Test (Liver Blood Tests)',
			18 => 'AMA (Antimitochondrial Antibodies)',
			19 => 'Amino Acid, Homocysteine (Homocysteine)',
			20 => 'Amniocentesis',
			21 => 'Amniotic Fluid (Amniocentesis)',
			22 => 'ANA (Antinuclear Antibody)',
			23 => 'Analysis of Urine (Urinalysis)',
			24 => 'Angiogram Of Heart (Coronary Angiogram)',
			25 => 'Angioplasty (Coronary Angioplasty)',
			26 => 'Annulus Support (Heart Valve Disease Treatment)',
			27 => 'Anti-CCP (Citrulline Antibody)',
			28 => 'Anti-citrulline Antibody (Citrulline Antibody)',
			29 => 'Anti-cyclic Citrullinated Peptide Antibody (Citrulline Antibody)',
			30 => 'Anti-Reflux Surgery (Fundoplication)',
			31 => 'Antimicrosomal Antibody Test (Thyroid Peroxidase Test)',
			32 => 'Antimitochondrial Antibodies',
			33 => 'Antinuclear Antibody',
			34 => 'Antithyroid Microsomal Antibody Test (Thyroid Peroxidase Test)',
			35 => 'Antro-duodenal Motility Study',
			36 => 'Aortic Heart Valve Replacement (Heart Valve Disease Treatment)',
			37 => 'Apgar Score',
			38 => 'Appendectomy',
			39 => 'Arrhythmia Treatment (Ablation Therapy for Arrhythmias)',
			40 => 'Arthritis Physical and Occupational Therapy',
			41 => 'Arthrocentesis (Joint Aspiration)',
			42 => 'Arthroplasty (Joint Replacement Surgery Of The Hand)',
			43 => 'Arthroscopy',
			44 => 'Artificial Kidney (Hemodialysis)',
			45 => 'Aspiration, Joint (Joint Aspiration)',
			46 => 'AST Test (Liver Blood Tests)',
			47 => 'Astigmatic Keratotomy Eye Surgery',
			48 => 'Auditory Brainstem Response (Newborn Infant Hearing Screening)',
			49 => 'Augmentation, Lip (Lip Augmentation)',
			50 => 'Autism Screening and Diagnosis',
			51 => 'Autopsy'
		);
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 6000);
			$rand2 = rand(0, 51);
			
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
			$status = $this->zlabdata->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				'This is a comment',
				$test_array[$rand2],
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Lab Data');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}

		//****************HEART RATE*************************
		
		$new_index = 0;
		
		foreach ($heartrates as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 172);
			
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZRATE, $ZCOMMENT, $ZUSERID
			$status = $this->zheartrate->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				'This is a comment',
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Heart Rate');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}

		//****************BLOOD PRESSURE*************************
		
		$new_index = 0;

		foreach ($blood as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$num = rand(4, 16);

		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(65, 95);
			$rand2 = rand(105, 135);
			
			//$id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZDIASTOLIC, $ZSYSTOLIC, $ZUSERID
			$status = $this->zbloodpressure->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				$rand2,
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Blood Pressure');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}
		
		}
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function location()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/location';
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function elements()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/elements';
	
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function saleTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/sale-test';
		
		 require 'lib/paytrace/vendor/autoload.php';
		
		$client = new GuzzleHttp\Client();
		
		// DRY
		$auth_server = 'https://api.paytrace.com';
		
		// send a request to the authentication server
		// note: normally, you'd store the username/password in a more secure fashion!
		$res = $client->post($auth_server . "/oauth/token", [
			'body' => [
				'grant_type' => 'password', 
				'username' => 'JoeAhart', 
				'password' => 'Tranzq123!'
			]
		]);
		
		// grab the results (as JSON)
		$json = $res->json();
		
		// get the access token
		$token = $json['access_token'];
		
		// create a fake sale transaction
		$sale_data = array(
			"amount" => 19.99, 
			"credit_card" => array(
				"number" => "4111111111111111",
				"expiration_month" => "12",
				"expiration_year" => "2020"
			),
			"billing_address" => array(
				"name" => "Steve Smith", 
				"street_address" => "8320 E. West St.",
				"city" => "Spokane",
				"state" => "WA", 
				"zip" => "85284"
			)
		);
		
		try {
			// post the transaction to hermes
			$res = $client->post($auth_server . "/v1/transactions/sale/keyed", [
				'headers' => [  'Authorization' => "Bearer " . $token],
				'json' => $sale_data
			]);
		} catch (Exception $e) {
			//@TODO
				print_r('!!! - ' . $e->getMessage());
				exit;
		}
		
		$response = $res->json();
		
		// dump the json results
		$result = var_export($res->json());
			
		//@TODO
			print '<pre>';
			print_r($result);
			print '</pre>';
			exit;
	}
	
	public function writeStepAction() {
		$this->load->model('_preloader');
		$init = $this->_preloader->load();
		
		$this->load->model('zsteps');
		$new_index = $this->zsteps->getNextId();
		
		$time = strtotime($_POST['date'] . ' -31 years');
		
		//$id, $Z_PK, $date, $user_id, $steps
		$status = $this->zsteps->writeData(
			0,
			$new_index,
			$_POST['date'],
			$_SESSION['user_id'],
			$_POST['steps']
		);
		
		print $status;
		exit;
	}

}