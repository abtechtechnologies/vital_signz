<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestController extends CI_Controller {
	
	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function fillEmptyInventorySlots()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$users = $this->user->getAvailableHolders(true);
		
		print '<pre>';
		print_r($users);
		print '</pre>';
		exit;
		
		$this->load->model('product');
		$this->load->model('inventory');

		$status = 1;
		
		foreach ($users as $user_id => $record) {

				$products = $this->product->getRecords();

				foreach ($products as $product_id => $data) {
					$inventory_record = $this->inventory->getMatch($user_id, $product_id);
					
	
					if (count($inventory_record) == 0) {

						$status = $this->inventory->writeData(0, 0, $product_id, $user_id);
					}
				}
				
		}
		
		print '<pre>';
		print_r($status);
		print '</pre>';
		exit;
		
	}

	public function removeDuplicateInventoryHolders()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$_SESSION['user_id'] = 2;
		
		$set_user_id = 4;
		
		
		$this->load->model('user');
		$users = $this->user->getRecords();
		
		$this->load->model('product');
		$this->load->model('inventory');
		$pass = array();
		$duplicates = array();
		
		$quantities = array();
		
		
		$status = 1;
		
		foreach ($users as $user_id => $record) {
			$inventory = $this->inventory->getRecords($user_id);
			
			$duplicates[$user_id] = array();
			$pass[$user_id] = array();
			$quantities[$user_id] = array();
			
			foreach ($inventory as $id => $data) {
				if ($data['user_id'] == $user_id) {
					if (isset($pass[$user_id][$data['product_id']])) {
						$duplicates[$user_id][$data['product_id']] = $data;
					} else {
						$pass[$user_id][$data['product_id']] = $data;
					}
					
					if ($data['quantity'] < 0) {
						$quantity = 0;
					} else {
						$quantity = $data['quantity'];
					}
					
					if (isset($quantities[$user_id][$data['product_id']])) {
						$quantities[$user_id][$data['product_id']] += $quantity;
					} else {
						$quantities[$user_id][$data['product_id']] = $quantity;
					}
				}
				
				
			}
		}
		
		
		
		
		
		
		
		print '<pre>';
		print_r($duplicates);
		print '</pre>';
		exit;
		
		foreach ($duplicates as $product_id => $data) {
			
			$matches = $this->inventory->getMatch($set_user_id, $product_id);
			
			$unit_name = '';
			
			foreach ($matches as $inventory_id => $data2) {
				if ($data2['unit_name'] != '') {
					$unit_name = $data2['unit_name'];
				}
				
				$status = $this->inventory->delete($inventory_id);
			}
			
			if ($status == 1) {
				$status = $this->inventory->writeData(0, $quantities[$product_id], $product_id, $set_user_id, $unit_name);
			}
			
		}
		
		print '<pre>';
		print_r($status);
		print '</pre>';
		exit;
	}
	
	public function generateCode()
	{
		$this->load->model('user');
		$users = $this->user->getRecordsAdmin();
		$all_codes = $this->user->getCodes();
		$status = 1;
		
		foreach ($users as $id => $data) {
			if ($status == 1) {
				if ($data['referral_code'] == 0 || $data['referral_code'] == '') {
					do {
						$code = '';
						
						$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
						
						shuffle($seed); // probably optional since array_is randomized; this may be redundant
						$rand = '';
						foreach (array_rand($seed, 4) as $k) $rand .= $seed[$k];
					} while (isset($all_codes[$rand]));
					
					$status = $this->user->writeCode($id, $rand);
				}
			}
		}
		
		print '<pre>';
		print_r($status);
		print '</pre>';
		exit;
	}

	public function resize()
	{
		$this->load->model('test');
		$this->load->model('product');
		
		$products = $this->product->getRecords();
		
		$document_root = $_SERVER['DOCUMENT_ROOT'];
		
		foreach ($products as $id => $data) {
			if ($data['attachment_file_name'] != '') {
				$settings = array('w'=>1920, 'output-filename' => $document_root . '/uploads/products/large/' . $data['attachment_file_name']);
				
				$status = $this->test->resize('/uploads/' . $data['attachment_file_name'], $settings, $document_root);

				//$file = substr($status, 16);
				
				//$end = $this->product->updateCache($id, $file);
			}
		}
		
		print '<pre>';
		print_r($status);
		print '</pre>';
		exit;
	}
	
	public function email()
	{
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$this->load->model('transaction');
		$scheduled = $this->transaction->getScheduled();
		
		$this->load->model('product');
		$this->load->model('user');
		$this->load->model('productsize');
		
		foreach ($scheduled as $transaction_id => $data) {
			$product_sizes = $this->product->getProductSizesByTransactionId($transaction_id);
			
			foreach ($product_sizes as $product_size_id => $data2) {
				$product_size = $this->productsize->getRecord($product_size_id);
				
				$product_sizes[$product_size_id] = $product_size;
				$product_sizes[$product_size_id]['quantity'] = $data2['quantity'];
				$product_sizes[$product_size_id]['product'] = $this->product->getRecord($product_size['product_id']);
			}
			
			$scheduled[$transaction_id]['product_sizes'] = $product_sizes;
		}
		
		//Notify drivers
		$drivers = $this->user->getDrivers();
		
		foreach ($scheduled as $transaction_id => $data) {
			
			$customer = $this->user->getRecord($data['user_id']);
			
			//foreach ($drivers as $id => $data) {
			//if ($status == 1) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('info@wwweed.org', 'WWWeed Admin');
			
			//if ($page_data['company_info']['dev_mode'] == 1) {
			$this->email->to('ahartjoe@gmail.com');
			//$this->email->to($data['email']);
			
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');
			
			$this->email->subject('THIS IS A TEST!');
			
			//Piece together string for telling what was ordered
			$products_string = '';
			$count = 1;
			$total = 0;
			
			foreach ($data['product_sizes'] as $product_size_id => $data3) {
				
				if ($count == count($data['product_sizes'])) {
					$products_string .= $data3['product']['name'] . ' (' . $data3['name'] . ' x ' . $data3['quantity'] . ')';
				} else {
					$products_string .= $data3['product']['name'] . ' (' . $data3['name'] . ' x ' . $data3['quantity'] . ') / ';
				}
				
				$count++;
				
				$total += $data3['price'] * $data3['quantity'];
			}
			
			$data = array(
							'name' => 'Joey D',
							'transaction_id' => $transaction_id,
							'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
							'delivery_address1' => $customer['address'],
							'delivery_address2' => 	$customer['city'] . ', ' . $customer['zip'],
							'site' => $company_info['site'],
							'phone' => $customer['phone'],
							'notes' => $data['notes'],
							'total' => $total,
							'product_string' => $products_string,
							'products' => $data['product_sizes']
			);
			
			$body = $this->load->view('email/invoice.phtml', $data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
			
			//$this->email->clear(true);
			//}
			//}
		}
	}
}
